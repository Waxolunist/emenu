function (key, values) {
	var max = 0;
	for(var val in values) {
		max = Math.max(max,val);
	}
	return max;
}