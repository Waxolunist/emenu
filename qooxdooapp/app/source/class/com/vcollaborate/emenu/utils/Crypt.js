/* ************************************************************************
 #ignore(sjcl)
 ************************************************************************ */
qx.Class.define("com.vcollaborate.emenu.utils.Crypt", {
	extend: qx.core.Object,
    type: "singleton",

	/*
    *****************************************************************************
    CONSTRUCTOR
    *****************************************************************************
    */
	/**
     * Creates a new instance of Part.
     */
    construct: function(){
		//load other scripts
    	if(!this.__sjclLoaded) {
        	var sl = new com.vcollaborate.emenu.utils.ScriptLoader();
        	sl.load("js/sjcl.min.js", this.__initCrypto, this);
        	sl.addListenerOnce("scriptLoaded", function(e) {
        		this.fireDataEvent('scriptLoaded', e);
        	}, this);
    	}
    },
    
    members: {
    	__cryptoinit: null,
    	__sjclLoaded: false,
    	__sjcl: null,
    	__0x438a: ["\x6F\x70\x61\x6C\x75\x33\x69\x70\x6F\x70\x35\x34"], 
    	
    	__initCrypto: function() {
    		this.debug("__initCrypto");
    		this.__sjcl = sjcl;
    		this.__sjcl.random.startCollectors();
	  		this.__cryptoinit = {
				iv: this.randomize(4,0),
				key: [],
				rp: {},
				p: {adata:"",
				    iter:1000,
				    mode:"ocb2",
				    ts:128,
				    ks:256}
			}
			
			this.__cryptoinit.p.salt = "7047C8B4 91EA099E";
			
			this.__sjclLoaded = true;
    	},
    	
    	decrypt: function (ct, password) {
    		this.debug("decrypt");
    		if(password) {
    			this.__cryptoinit.password = password;
    		} else {
    			this.__cryptoinit.password = qx.util.Base64.encode(this.__0x438a[0]);
    		}
    		
			var result;
			try {
				
				var ctjson = qx.lang.Json.parse(qx.util.Base64.decode(ct));
				ctjson.ks = this.__cryptoinit.p.ks;
				ctjson.ts = this.__cryptoinit.p.ts;
				ctjson.mode = this.__cryptoinit.p.mode;
				ctjson.salt = this.__cryptoinit.p.salt;
			

			  	result = this.__sjcl.decrypt(qx.util.Base64.decode(this.__cryptoinit.password) 
			  							|| this.__cryptoinit.key, qx.lang.Json.stringify(ctjson), {}, this.__cryptoinit.rp);
			} catch(e) {
			  return e;
			}
			return result;
		},

		encrypt: function (text, password) {		
			this.debug("encrypt");
			if(password) {
    			this.__cryptoinit.password = password;
    		} else {
    			this.__cryptoinit.password = qx.util.Base64.encode(this.__0x438a[0]);
    		}
			
			var ct = qx.lang.Json.parse(this.__sjcl.encrypt(qx.util.Base64.decode(this.__cryptoinit.password) 
										|| this.__cryptoinit.key, text, this.__cryptoinit.p, this.__cryptoinit.rp));
			var ctresult = { iv:ct.iv,ct:ct.ct }; 
			return qx.util.Base64.encode(qx.lang.Json.stringify(ctresult));
		},

		randomize: function (words, paranoia) {
			return this.__sjcl.random.randomWords(words, paranoia);
		}
    },
    
    events : {
 		/**
 		* fires when a script is loaded
		*/
	 	scriptLoaded: 'qx.event.type.Event'
	}
});