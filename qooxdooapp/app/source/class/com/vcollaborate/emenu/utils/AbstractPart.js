/**
 * This abstract class defines the methods for all derived parts.
 * AbstractParts are loaded from the application in specific order,
 * thus as little manipulation as possible to common files and other parts have to be
 * done.
 * If some methods are not overridden, this class ensures, that the parts are still working
 * and no errors are thrown.
 */
qx.Class.define("com.vcollaborate.emenu.utils.AbstractPart", {
    type: "abstract",
    extend: qx.core.Object,
    include: [qx.locale.MTranslation],

    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {

        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Override for using.
         *
         * @param controller {com.vcollaborate.emenu.Application} The main application class
         * @return {void}
         */
        init: function(controller){
            this.debug("init");
        },

	    /**
	     * Override for using.
	     *
	     * @param commands {var} the global commands object
         * @return {void}
         */
        initCommands: function(commands){
            this.debug("initCommands");
        },

	    /**
	     * Override for using.
	     *
	     * @see #initCommands
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @param part {qx.ui.toolbar.Part} the part to add the buttons to
         * @return {void}
         */
        initButtons: function(commands, buttons, part){
            this.debug("initButtons");
        },

	    /**
	     * Override for using.
	     *
         * @return {void}
         */
        initListeners: function(){
            this.debug("initListeners");
        },

	    /**
	     * Override for using.
	     *
	     * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @return {void}
         */
        setToolTips: function(commands, buttons){
            this.debug("setToolTips");
        },

	    /**
	     * Override for using.
	     *
         * @return {var} null
         */
		getLeftSidePane: function() {
			this.debug("getLeftSidePane");
			return null;
		},

	    /**
	     * Override for using.
	     *
         * @return {var} null
         */
		getRightSidePane: function() {
			this.debug("getRightSidePane");
			return null;
		},
		
		postInit: function() {
			this.debug("postInit");
		}
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
    }
});
