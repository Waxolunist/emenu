/**
 * ViewUtils
 */
qx.Class.define("com.vcollaborate.emenu.utils.ViewUtils",
{
  statics :
  {

    setOnTop : function(widget)
    {
		var root = qx.core.Init.getApplication().getRoot();
		var maxWindowZIndex = 1E5;
		var windows = root.getWindows();
		for (var i = 0; i < windows.length; i++) {
			var zIndex = windows[i].getZIndex();
			maxWindowZIndex = Math.max(maxWindowZIndex, zIndex);
		}
		widget.setZIndex( maxWindowZIndex +1 );
    }
  }
});
