/**
 * Loads scripts
 */
qx.Class.define("com.vcollaborate.emenu.utils.ScriptLoader", {	
	extend: qx.core.Object,

	members : {
		load: function(script, callback, context) {
			this.loadScriptArrayAsync([script], callback, context);
		},
		
		loadScriptArrayAsync: function(arr, callback, context) {
			for(var i = 0, l = arr.length, cnt = 0; i < l; i++) {
				this.debug("load script " + arr[i]);
				var script = arr[i];
				var src = qx.util.ResourceManager.getInstance().toUri(script);
				var sl = new qx.io.ScriptLoader();
			 	sl.load(src, function(status){
				 	if (status == 'success'){
						this.debug("script loaded: " + script);
						if(++cnt == l) {
							if(callback) {
								this.fireDataEvent('scriptLoaded',arr);
								callback.call(context);
							}
							this.fireDataEvent('_scriptLoaded',arr);
						}
				 	} else {
				 		this.debug("scripts " + arr + " could not be loaded");
				 	}
			 	},this);	
			}
		},
		
		loadScriptArray: function(arr, callback, context) {
        	var l = arr.length;
        	for(var i = 0; i < l; i++) {
				if(arr[i] !== undefined) {
        			this.load(arr[i], null, this);					
					delete arr[i];
					break;
				}
				if(i == (l-1)) {
					//all scripts loaded
					if(callback) {
						callback.call(context);
						return;
					}
				}
			}
			this.addListenerOnce('_scriptLoaded', function(){
				this.loadScriptArray(arr, callback, context);
			}, this);
		}
	},
	
	/*
    *****************************************************************************
    STATICS
	*****************************************************************************
	*/
  	statics :
  	{
  		load : function(script, callback, context) {
	  		var sl = new com.vcollaborate.emenu.utils.ScriptLoader();
	  		sl.load(script, callback, context);
	  	},
	  	
	  	loadScriptArrayAsync : function(arr, callback, context) {
	  		var sl = new com.vcollaborate.emenu.utils.ScriptLoader();
	  		sl.loadScriptArray(arr, callback, context);
	  	},
	  	
	  	loadScriptArray : function(arr, callback, context) {
	  		var sl = new com.vcollaborate.emenu.utils.ScriptLoader();
	  		sl.loadScriptArray(arr, callback, context);
	  	}
		
	},
	
	events : {
 		/**
 		* fires when a script is loaded
		*/
	 	scriptLoaded: 'qx.event.type.Event',
	 	_scriptLoaded: 'qx.event.type.Event'
	}
});