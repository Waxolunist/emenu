qx.Class.define("com.vcollaborate.emenu.utils.String", {
    type: "static",
    statics: {
        EMPTY: "",
        
        isBlank: function(pString) {
        	if (!pString || pString.length == 0) {
        		return true;
    		}
		    // checks for a non-white space character 
		    // which I think [citation needed] is faster 
		    // than removing all the whitespace and checking 
		    // against an empty string
		    return !/[^\s]+/.test(pString);
        }
    }
});
