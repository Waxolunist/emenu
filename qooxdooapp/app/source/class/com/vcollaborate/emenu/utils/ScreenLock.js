qx.Class.define("com.vcollaborate.emenu.utils.ScreenLock", {
    extend: qx.core.Object,
    type: "singleton",
    
    construct: function(){
    	
    },
    
    members: {

    	__timerId: null,
    	__oldBlockerColor: null,
    	__oldBlockerOpacity: null,
    	__disabledCommands: null,
    	
    	initScreenLock: function(idleTimeout, unlockWindow, blockerColor, blockerOpacity) {
    		this.debug("initScreenLock");
    		this.set({
    			idleTimeout: idleTimeout || 0,
    			unlockWindow: unlockWindow,
    			blockerColor: blockerColor,
    			blockerOpacity: blockerOpacity
    		});
        	qx.core.Init.getApplication().getRoot().addListener("mousemove", this.__startStopIdleTimer, this);
        	qx.core.Init.getApplication().getRoot().addListener("keydown", this.__startStopIdleTimer, this);
        	
        	unlockWindow.addListener("close", function(e) {
        		if(this.__oldBlockerColor) {
        			qx.core.Init.getApplication().getRoot().setBlockerColor(this.__oldBlockerColor);
        		}
        		if(this.__oldBlockerOpacity) {
        			qx.core.Init.getApplication().getRoot().setBlockerOpacity(this.__oldBlockerOpacity);
        		}
        		
        		this.__disabledCommands.forEach(function(item,idx,arr){
        			item.setEnabled(true);
	        	},this);
        		
        		this.__disabledCommands = null;
        		
        		qx.core.Init.getApplication().getRoot().addListener("mousemove", this.__startStopIdleTimer, this);
        		qx.core.Init.getApplication().getRoot().addListener("keydown", this.__startStopIdleTimer, this);
        		
        		this.__startStopIdleTimer();
        	}, this);
        },
        
        __startStopIdleTimer: function() {
        	var timer = qx.util.TimerManager.getInstance();   
        	timer.stop(this.__timerId);
	        this.__timerId = timer.start(function(userData, timerid) {
	        	this.__lockScreen();
	        }, 0, this, null, this.getIdleTimeout()*1000);
        },
        
        __applyIdleTimeout: function(value, old, name) {
    		this.debug("__applyIdleTimeout");
    		this.__startStopIdleTimer();
    	},
    	
    	__lockScreen: function() {
    	    this.debug("__lockScreen");
    	    
    	     if(this.getBlockerColor()) {
	        	this.__oldBlockerColor = qx.core.Init.getApplication().getRoot().getBlockerColor(); 
	        	qx.core.Init.getApplication().getRoot().setBlockerColor(this.getBlockerColor());
	        }
	        
	        if(this.getBlockerOpacity()) {
	        	this.__oldBlockerOpacity = qx.core.Init.getApplication().getRoot().getBlockerOpacity();
	        	qx.core.Init.getApplication().getRoot().setBlockerOpacity(this.getBlockerOpacity());
	        }
    	    this.getUnlockWindow().center();
	        this.getUnlockWindow().open();
	        
	        var commands = qx.core.Init.getApplication().getShortcuts();
	        this.__disabledCommands = new qx.data.Array();
	        for(var c in commands) {
	        	if(commands[c].getEnabled()) {
	        		commands[c].setEnabled(false);
	        		this.__disabledCommands.push(commands[c]);
	        	}
	        }

	        qx.core.Init.getApplication().getRoot().removeListener("mousemove", this.__startStopIdleTimer, this);
        	qx.core.Init.getApplication().getRoot().removeListener("keydown", this.__startStopIdleTimer, this);
    	}
    },
    
    properties: {
        idleTimeout: {
            apply: "__applyIdleTimeout",
            init: 0,
            nullable: false,
            check: "Integer",
            validate: qx.util.Validate.range(0,10000), 
            event: "changeIdleTimeout"
        },
        
        unlockWindow: {
            init: null,
            nullable: true,
            check: "qx.ui.window.Window",
            event: "changeUnlockWindow"
        }, 
        
   		blockerColor: {
   			init: null,
   			nullable: true,
   			check: "Color",
   			event: "changeBlockerColor"
   		},
   		
		blockerOpacity: {
			init: null,
			nullable: true,
			check: "Number",
			validate: qx.util.Validate.range(0,1), 
            event: "changeBlockerOpacity"
		}
    },
    
    statics: {
    }
});
