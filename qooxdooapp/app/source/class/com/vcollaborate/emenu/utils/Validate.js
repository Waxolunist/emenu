qx.Class.define("com.vcollaborate.emenu.utils.Validate", {
    type: "static",
    statics: {
        equal: function(compareProperty, errorMessage){
            return function(value){
				var value2 = this["get" + qx.lang.String.firstUp(compareProperty)]();
                errorMessage = errorMessage ||
                qx.locale.Manager.tr("%1 equals not %2.", value, value2);
                
				this.debug("value: " + value + ", value2: " + value2);
				
                if (value != value2) {
                    throw new qx.core.ValidationError("Validation Error", errorMessage);
                }
            }
        },
		
		urlOrEmpty: function(errorMessage){
			return function(value){
				if(com.vcollaborate.emenu.utils.Validate.notEmpty(value)) {
					qx.util.Validate.checkUrl(value, null, errorMessage);
				}
			}
		},
		
		empty: function(value) {
			return !com.vcollaborate.emenu.utils.Validate.notEmpty(value);
		},
		
		notEmpty: function(value) {
			return value != null && value.length > 0;
		},
		
		username: qx.util.Validate.regExp(/^[0-9a-zA-Z]{3,16}$/, qx.locale.Manager.tr("Only letters and numbers are allowed. Minimum 3 characters.")),
		
		password: qx.util.Validate.regExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])\w{6,}$/, qx.locale.Manager.tr("The password does not fulfil the password policy."))
    }
});
