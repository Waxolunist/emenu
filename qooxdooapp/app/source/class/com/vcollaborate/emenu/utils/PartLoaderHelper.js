/**
 * Loads the parts according to {@link #activeParts}
 */
qx.Class.define("com.vcollaborate.emenu.utils.PartLoaderHelper",
{
	type : "static",

	/*
    *****************************************************************************
    STATICS
	*****************************************************************************
	*/
  	statics :
  	{
		/**
		 * The activated parts. In the order here defined, the toolbar will render the buttons.
		 */
    	/*__activeParts : [
			"com.vcollaborate.emenu.categories"
			//,"com.vcollaborate.emenu.articles"
			,"com.vcollaborate.emenu.passwords"
			//,"com.vcollaborate.emenu.time"
			,"com.vcollaborate.emenu.pref"
			,"com.vcollaborate.emenu.test"
		],*/

		isPartActive : function(partName) {
			return qx.lang.Array.contains(com.vcollaborate.emenu.utils.PartLoaderHelper.getActiveParts(), partName);
		},

	    /**
	     * Loads one or more parts asynchronously. The callback is called after all parts and their dependencies are fully loaded. If the parts are already loaded the callback is called immediately.
	     *
	     * @param partNames {var} List of parts names to load as defined in the config file at compile time.
	     * @param callback {var} Function to execute on completion
	     * @param self {var} Context to execute the given function in
	     * @return {void}
	     */
	    require : function(partNames, callback, self)
	    {
	      	if (qx.Bootstrap.isString(partNames)) {
	        	partNames = [ partNames ];
	      	}
	
	      	var partActivated = true;
	
	      	for (var i=0; i<partNames.length; i++)
	      	{
	        	if (!qx.lang.Array.contains(com.vcollaborate.emenu.utils.PartLoaderHelper.getActiveParts(), partNames[i])) {
	          		partActivated = false;
	        	}
	      	}
	
	      	if (partActivated)
	      	{
	        	self.debug("loading: " + partNames);
	        	qx.io.PartLoader.require(partNames, callback, self);
	    	}
		},
		
		getActiveParts : function() {
			var appObj = com.vcollaborate.emenu.model.Application.getApplicationObject();
			return appObj.getLicenseObject().getParts();
			//var licensekey = appObj.getLicensekey();
			//var license = com.vcollaborate.emenu.model.License.getLicenseObject(licensekey);
			//return license.getParts();
			//return com.vcollaborate.emenu.utils.PartLoaderHelper.__activeParts;
		}
	}
});