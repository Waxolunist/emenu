qx.Class.define("com.vcollaborate.emenu.utils.Dialogs", {
    type: "static",
    statics: {
        installWizard: function(callback, ctx, adminparty){
	      /*
	       * wizard widget
	       */
	      var pageData = 
	      [
	       {
	         "message" : "<p style='font-weight:bold'>" + qx.locale.Manager.tr("Welcome to the installation wizard of EMenu") + "</p>",
	         "formData" : {
	           "licenseLabel" : {
	             "type" : "label",
	             "label" : qx.locale.Manager.tr("Please enter your licensekey or enter 'test' for a 30-day trial.")
	           },         
	           "license" : {
	             "type" : "textarea",
	             "label": qx.locale.Manager.tr("Licensekey"),
	             "validation" : {
	               "required" : true
	             },
	             "lines": 10
	           }
	         }
	       },
	       {
	         "messageFn" : function() {
	         				if(adminparty) {
	         					return "<p style='font-weight:bold'>" + qx.locale.Manager.tr("Administration account") + "</p><p>" 
	         			 			+ qx.locale.Manager.tr("Admin Party: Create an administration account 'admin'") + "</p>";
	         				} else {
	         					return "<p style='font-weight:bold'>" + qx.locale.Manager.tr("Administration account") + "</p><p>" 
	         			 			+ qx.locale.Manager.tr("Enter your 'admin' password.") + "</p>";
	         				}
	         			 },
	         "formData" : {
	           "label1" : {
	             "type" : "label",
	             "label" : qx.locale.Manager.tr("Please enter your email address.")
	           },
	           "email" : {
	             "type" : "textfield",
	             "label": qx.locale.Manager.tr("Email"),
	             "validation" : {
	               "required" : true,
	               "validator" : qx.util.Validate.email()
	             }
	           },
	           "label2" : {
	             "type" : "label",
	             "label" : qx.locale.Manager.tr("Please enter a password.")
	           },
	           "password" : {
	             "type" : "passwordfield",
	             "label": qx.locale.Manager.tr("Password"),
	             "validation" : {
	               "required" : true,
	               "validator" : com.vcollaborate.emenu.utils.Validate.password
	             }
	           },
	           "passwordRepeat" : {
	             "type" : "passwordfield",
	             "label": qx.locale.Manager.tr("Password Repeat"),
	             "validation" : {
	               "required" : true,
	               "validator" : function(value) {
	               		if(this.getModel().getPassword() != value) {
	               			throw new qx.core.ValidationError("Validation Error", qx.locale.Manager.tr("The password and password Repeat are not the same."));
	               		}
	               }
	             }
	           }
	         }
	       }
	      ];
	      var wizard = new dialog.Wizard({
	        width       : 500,
	        maxWidth    : 500,
	        pageData    : pageData, 
	        allowCancel : true,
	        callback    : callback,
	        context     : ctx
	      });
	      wizard.start();
        }
    }
});
