/**
 * This widgets displays an {@link com.vcollaborate.emenu.model.Article}.
 */
qx.Class.define("com.vcollaborate.emenu.articles.ArticleView", {
    extend: qx.ui.container.Composite,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
	/**
     * Creates a new instance of ArticleView.
     */
    construct: function(){
        this.base(arguments);
        
        this.__initLayout();
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * The current {@link com.vcollaborate.emenu.model.Article}.
         */
        article: {
            apply: "__applyArticle",
            nullable: true,
            check: "Object"
        }
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __htmlArea: null,
        
		/*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */	
        /**
         * Applies the new {@link com.vcollaborate.emenu.model.Article} to this view.
         * If value is old, the content of the htmlArea is set to an empty string.
         *
         * @param value {com.vcollaborate.emenu.model.Article} the new {@link com.vcollaborate.emenu.model.Article}
         * @param old {com.vcollaborate.emenu.model.Article} the old {@link com.vcollaborate.emenu.model.Article}
         * @return {void}
         */
        __applyArticle: function(value, old){
            this.debug("__applyArticle");
            
            if (value) {
                this.updateArticleView();
            }
            else {
                this.__htmlArea.setHtml("");
            }
            
            this.__htmlArea.getContentElement().scrollToY(0);
        },
        
		/**
		 * Initialises the layout of this view.
		 * The layout consists of a horizontal {@link qx.ui.splitpane.Pane}. On the 
		 * left side the __htmlArea is shown, on the left side an empty widget at 
		 * the moment. Later some additonal properties can be shown there.
		 * 
		 * @return {void}
		 */
        __initLayout: function(){
			this.debug("__initLayout");
			
            var dock = new qx.ui.layout.Dock();
            
            this.setLayout(dock);
            
            var horizontalSplitPane = new qx.ui.splitpane.Pane("horizontal");
            
            this.add(horizontalSplitPane, {
                edge: "center"
            });
            
            this.__htmlArea = new qx.ui.embed.Html().set({
                backgroundColor: "white"
            });
            //this.__htmlArea.setOverflow("auto", "auto");
            this.__htmlArea.setSelectable(true);
            
            var htmlContainer = new qx.ui.core.scroll.ScrollPane();
            htmlContainer.add(this.__htmlArea);
            
            horizontalSplitPane.add(htmlContainer.set({
                decorator: "input-html",
                textColor: "text-active",
                minWidth: 200
            }), 7);
            
            horizontalSplitPane.add(new qx.ui.core.Widget().set({
                decorator: null,
                backgroundColor: "white",
                maxWidth: 600,
                minWidth: 200
            }), 3);
        },
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * This method updates the __htmlArea to the current content of 
         * the current {@link #article}. Additionaly it sets the target of
         * all html links to *_blank*.
         *
         * @return {void}
         */
        updateArticleView: function(){
			this.debug("updateArticleView");
			
            var html = this.getArticle().getContent();
            this.__htmlArea.setHtml(html);
            
            // flush all elements (needed to access the content of the dom element)
            qx.html.Element.flush();
            
            // get the dom element containing the html of the article
            var element = this.__htmlArea.getContentElement().getDomElement();
            
            // get all links
            var links = element.getElementsByTagName("a");
            
            // set the targets of all links to _blank
            for (var i = 0; i < links.length; i++) {
                links[i].target = "_blank";
            }
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
		this.debug("destruct");
		
        this._disposeObjects("__htmlArea");
    }
});