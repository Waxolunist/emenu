/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/document-new.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-apply.png)
 ************************************************************************ */
/**
 * This widgets displays an {@link com.vcollaborate.emenu.model.Article} for editing and adding.
 */
qx.Class.define("com.vcollaborate.emenu.articles.AddArticleWindow", {
    extend: qx.ui.window.Window,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of AddArticleWindow.
     */
    construct: function(){
        this.base(arguments, this.tr("Add article"), "icon/22/actions/document-new.png");
        
        // set the properties of the window
        this.set({
            modal: true,
            showMinimize: false,
            showMaximize: false,
            allowMaximize: false
        });
        
        // Create the content with a helper
        this.__addContent();
        
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __titleTextfield: null,
        __categoryLabel: null,
        __form: null,
        __htmlArea: null,
        __categorySelection: null,
        //TODO make it a property
		__article: null,
        __addButton: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Adds the content of the window.
         *
         * @return {void}
         */
        __addContent: function(){
            this.debug("__addContent");
            
            this.setLayout(new qx.ui.layout.VBox(0));
            
            var groupBox = new qx.ui.groupbox.GroupBox(this.tr("Article Information"));
            groupBox.setWidth(900);
            groupBox.setLayout(new qx.ui.layout.VBox(10));
            
			if (com.vcollaborate.emenu.categories.Part != undefined) {
				this.__initCategories();
				groupBox.add(this.__categoryLabel);
			}
			
            this.__titleTextfield = new qx.ui.form.TextField().set({
                required: true
            });
            this.__titleTextfield.setPlaceholder(this.tr("Title"));
            groupBox.add(this.__titleTextfield);
            
			this.__htmlArea = new com.vcollaborate.emenu.view.HtmlAreaBindingWidget(this.tr("Description"));
			groupBox.add(this.__htmlArea, {
                flex: 1,
                height: "100%"
            });
            
            this.__addButton = new qx.ui.form.Button(this.tr("Add"), "icon/22/actions/dialog-apply.png");
            
            this.__addButton.set({
                alignX: "right",
                allowGrowX: false
            });
            
            this.__addButton.addListener("execute", this.__addArticle, this);
            groupBox.add(this.__addButton);
            
            this.add(groupBox, {
                flex: 1,
                height: "100%"
            });
            
            // create a form
            this.__form = new qx.ui.form.Form();
            
            // set the headline of the form
            this.__form.addGroupHeader(this.tr("Article Information"));
        },
        
        
        /**
         * Inits the listeners for key strokes.
         */
        __initListeners: function(){
            this.debug("__initListeners");
            
            this.addListener("resize", this.center, this);
			if (com.vcollaborate.emenu.categories.Part != undefined) {
				this.addListener("appear", this.__initCategories, this);
			}
            
            this.addListener("keypress", function(e){
                var key = e.getKeyIdentifier();
                
                if (key == "Escape") {
                    this.close();
                }
            }, this);
        },
        
        /**
         * Inits the category if {@link com.vcollaborate.emenu.categories.Part} is loaded.
         *
         * @return {void}
         */
        __initCategories: function(){
            this.debug("__initCategories");
            
            this.__categorySelection = com.vcollaborate.emenu.categories.Part.getInstance().getSelectedCategory();
            
            this.__setCategoryLabel();
        },
        
        /**
         * Sets the category label.
         *
         * @return {void}
         */
        __setCategoryLabel: function(){
            this.debug("__setCategoryLabel");
            
            if (!this.__categoryLabel) {
                this.__categoryLabel = new qx.ui.basic.Label("").set({
                    rich: true
                });
            }
            
            if (this.__categorySelection) {
                this.__categoryLabel.setValue("<b>" + this.tr("Category") + ":</b>&nbsp;" + this.__categorySelection.getTitle());
            }
        },
        
        /**
         * Handles button clicks on 'Add' button or/and
         * pressing enter key on textfields
         *
         * @param e {qx.event.type.Event} Execute event
         * @return {void}
         */
        __addArticle: function(e){
            this.debug("__addArticle");
            
            if (this.__form.validate()) {
                var title = this.__titleTextfield.getValue();
                
                if (!title) {
                    title = "";
                }
                
                var content = this.__htmlArea.getContent();
				
                if (this.__article) {
                    this.__article.setTitle(title);
                    this.__article.setContent(content);
                    var articleView = com.vcollaborate.emenu.articles.Part.getInstance().getArticleView();
                    articleView.setArticle(this.__article);
                    articleView.updateArticleView();
                }
                else {
                    this.__article = new com.vcollaborate.emenu.model.Article(null, title, this.__categorySelection, content, 0);
                }
                
                this.__article.save();
                
                // clear the content of the window
                this.__titleTextfield.setValue("");
                this.__htmlArea.emptyContent();
                
                this.close();
            }
        },
		
		/**
         * This is just a helper method for a listener.
         *
         * @return {void}
         */
        __setArticleAfterReady: function(){
			this.debug("__setArticleAfterReady");
			
            //this.setArticle(this.__article);
        },
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Sets the article.
         *
         * @param article {com.vcollaborate.emenu.model.Article} the article to display for editing
         * @return {void}
         */
        setArticle: function(article){
            this.debug("setArticle");
            
			this.__article = article;
            var title = this.getChildControl("title");
            
            if (this.__article) {
				//TODO test if it works together with the couchdb
                //if (this.__htmlArea.isReady()) {
                    title.setValue(this.tr("Edit article"));
                    
					if (com.vcollaborate.emenu.categories.Part != undefined) {
						this.__categorySelection = this.__article.getCategory();
						this.__setCategoryLabel();
					}
                    
					this.__titleTextfield.setValue(this.__article.getTitle());
                    
                    this.__htmlArea.setContent(this.__article.getContent());
                    
                    this.__addButton.setLabel(this.tr("Save"));
                //}
                //else {
                    //to set the content of htmlarea, the iframe needs to be loaded first, thus I set the content again after ready
                //    this.__htmlArea.addListenerOnce("ready", this.__setArticleAfterReady, this);
                //}
            }
            else {
                title.setValue(this.tr("Add article"));
                this.__addButton.setLabel(this.tr("Add"));
                this.__htmlArea.emptyContent();
                this.__titleTextfield.setValue(null);
            }
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
   		this.debug("destruct");

        this.__commands = null;
        this._disposeObjects("__titleTextfield", "__form", "__htmlArea", "__categoryLabel", "__categorySelection", "__article", "__addButton");
    }
});