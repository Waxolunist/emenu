/**
 * This package contains the com.vcollaborate.emenu.articles's UI parts and logic
 * for binding, displaying, editing, adding and removing objects of the type
 * {@link com.vcollaborate.emenu.model.Article}.
 */