/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/list-add.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/list-remove.png)
 #asset(com/vcollaborate/emenu/images/edit.png)
 ************************************************************************ */
/**
 * This part adds the possibility to add, edit and remove {@link com.vcollaborate.emenu.mode.Article} to
 * {@link com.vcollaborate.emenu.mode.Category}.
 * It provides only a rightsidepane.
 *
 * @see com.vcollaborate.emenu.categories.Part
 */
qx.Class.define("com.vcollaborate.emenu.articles.Part", {
    extend: com.vcollaborate.emenu.utils.AbstractPart,
    type: "singleton",

	/*
    *****************************************************************************
    CONSTRUCTOR
    *****************************************************************************
    */
	/**
     * Creates a new instance of Part.
     */
    construct: function(){
        this.base(arguments);

        if (!this.__addArticleWindow) {
            this.__addArticleWindow = new com.vcollaborate.emenu.articles.AddArticleWindow();
        }

        if (!this.__listView) {
            this.__listView = new com.vcollaborate.emenu.articles.List();
            this.__listView.setHeight(200);
            this.__listView.setDecorator("main");
        }

        if (!this.__articleView) {
            this.__articleView = new com.vcollaborate.emenu.articles.ArticleView();
            this.__articleView.setDecorator("main");
        }

        if (!this.__listController) {
            // create the controller which binds ths list
            // 1. Parameter: The model (null because is bound in the next line)
            // 2. Parameter: The view (the list widget)
            // 3. Parameter: name of the model property to show as label in the list
            this.__listController = new qx.data.controller.List(null, this.__listView.getList(), "title");

            // bind the article
            // bind the first selection of the list to the article view
            this.__listController.bind("selection[0]", this.__articleView, "article");
        }
    },

	/*
    *****************************************************************************
    MEMBERS
    *****************************************************************************
    */
    members: {

        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __addArticleWindow: null,
        __listView: null,
        __articleView: null,
        __listController: null,
        __addArticleCmd: null,
        __editArticleCmd: null,
        __removeArticleCmd: null,

        /*
         *****************************************************************************
         DERIVED
         *****************************************************************************
         */
        /**
         * Adds the window for adding and editing articles ({@link com.vcollaborate.emenu.articles.AddArticleWindow})
         * to the root of the application.
         *
         * @param controller {com.vcollaborate.emenu.Application} The main application class
         * @return {void}
         */
        init: function(controller){
            this.base(arguments);
            controller.getRoot().add(this.__addArticleWindow);
        },

        /**
         * Inits following three commands:
         *
         * <ul>
         * <li>{@link #getAddArticleCmd}</li>
         * <li>{@link #getEditArticleCmd}</li>
         * <li>{@link #getRemoveArticleCmd}</li>
         * </ul>
         *
         * @param commands {var} the global commands object
         * @return {void}
         */
        initCommands: function(commands){
            this.base(arguments);
            commands.addArticle = new qx.ui.core.Command("Control+Shift+A");
            commands.addArticle.addListener("execute", this.__showAddArticle, this);
			commands.addArticle.setEnabled(false);
            this.__addArticleCmd = commands.addArticle;

            commands.editArticle = new qx.ui.core.Command("Control+Shift+S");
            commands.editArticle.addListener("execute", this.__showEditArticle, this);
			commands.editArticle.setEnabled(false);
            this.__editArticleCmd = commands.editArticle;

            commands.removeArticle = new qx.ui.core.Command("Control+Shift+D");
            commands.removeArticle.addListener("execute", this.__removeArticle, this);
			commands.removeArticle.setEnabled(false);
            this.__removeArticleCmd = commands.removeArticle;
        },

        /**
         * Inits the buttons for the toolbar, one for each command.
         *
         * @see #initCommands
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @param part {qx.ui.toolbar.Part} the part to add the buttons to
         * @return {void}
         */
        initButtons: function(commands, buttons, part){
            this.base(arguments);
            buttons.addArticle = new qx.ui.toolbar.Button(this.tr("Add article"), "icon/22/actions/list-add.png");
            buttons.addArticle.setCommand(commands.addArticle);
            part.addAt(buttons.addArticle, 0);

            buttons.editArticle = new qx.ui.toolbar.Button(this.tr("Edit article"), "com/vcollaborate/emenu/images/edit.png");
            buttons.editArticle.setCommand(commands.editArticle);
            part.addAt(buttons.editArticle, 1);

            buttons.removeArticle = new qx.ui.toolbar.Button(this.tr("Remove article"), "icon/22/actions/list-remove.png");
            buttons.removeArticle.setCommand(commands.removeArticle);
            part.addAt(buttons.removeArticle, 2);
			
			part.show();
        },

        /**
         * Inits the listeners to handle the disable- and enable-actions of the toolbar buttons.
         *
         * @return {void}
         */
        initListeners: function(){
            this.base(arguments);
            this.__listView.getList().addListener("changeSelection", function(e){
                if (e.getData().length > 0) {
                    this.getRemoveArticleCmd().setEnabled(true);
                    this.getEditArticleCmd().setEnabled(true);
                }
                else {
                    this.getRemoveArticleCmd().setEnabled(false);
                    this.getEditArticleCmd().setEnabled(false);
                }
            }, this);
        },

        /**
         * Sets the tooltips for each button of this part.
         *
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @return {void}
         */
        setToolTips: function(commands, buttons){
            this.base(arguments);
            buttons.addArticle.setToolTipText(commands.addArticle.toString());
            buttons.editArticle.setToolTipText(commands.editArticle.toString());
            buttons.removeArticle.setToolTipText(commands.removeArticle.toString());
        },

        /**
         * Returns a vertical {@link qx.ui.splitpane.Pane} with a {@link com.vcollaborate.emenu.articles.List}
         * at the head and a {@link com.vcollaborate.emenu.articles.ArticleView} at the bottom.
         *
         * @return {qx.ui.splitpane.Pane} {@link qx.ui.splitpane.Pane}
         */
        getRightSidePane: function(){
            this.base(arguments);
            var verticalSplitPane = new qx.ui.splitpane.Pane("vertical");
            verticalSplitPane.setDecorator(null);

            verticalSplitPane.add(this.__listView, 0);
            verticalSplitPane.add(this.__articleView, 1);

            return verticalSplitPane;
        },

        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Opens the {@link com.vcollaborate.emenu.article.AddArticleWindow} to add an
         * {@link com.vcollaborate.emenu.model.Article}.
         *
         * @return {void}
         */
        __showAddArticle: function(){
            this.debug("__showAddArticle");
            this.__addArticleWindow.setArticle(null);
            this.__addArticleWindow.center();
            this.__addArticleWindow.open();
        },

        /**
         * Opens a {@link com.vcollaborate.emenu.article.AddArticleWindow} to edit
         * the selected {@link com.vcollaborate.emenu.model.Article}.
         *
         * @return {void}
         */
        __showEditArticle: function(){
            this.debug("__showEditArticle");
            var articleSelection = this.getSelectedArticle();

            if (articleSelection) {
                this.__addArticleWindow.setArticle(articleSelection);
                this.__addArticleWindow.center();
                this.__addArticleWindow.open();
            }
        },

        /**
         * Removes the selected {@link com.vcollaborate.emenu.model.Article}.
         *
         * @return {void}
         */
        __removeArticle: function(){
            this.debug("__removeArticle");
            var articleSelection = this.getSelectedArticle();

            if (articleSelection) {
                articleSelection.getCategory().getArticles().remove(articleSelection);
                articleSelection.remove();
            }
        },

        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Returns the {@link qx.data.controller.List} bound to {@link com.vcollaborate.emenu.articles.List}.
         *
         * @return {qx.data.controller.List} {@link qx.data.controller.List}
         */
        getListController: function(){
            return this.__listController;
        },

        /**
         * Returns the {@link com.vcollaborate.emenu.articles.ArticleView}.
         *
         * @return {com.vcollaborate.emenu.articles.ArticleView} {@link com.vcollaborate.emenu.articles.ArticleView}
         */
        getArticleView: function(){
            return this.__articleView;
        },

        /**
         * Returns the current selected {@link com.vcollaborate.emenu.model.Article}
         *
         * @return {com.vcollaborate.emenu.model.Article} {@link com.vcollaborate.emenu.model.Article}
         */
        getSelectedArticle: function(){
            return this.__listController.getSelection().getItem(0);
        },

        /**
         * Returns the command __addArticle__.
         * The command has the shortcut *Control+Shift+A* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getAddArticleCmd: function(){
            return this.__addArticleCmd;
        },

        /**
         * Returns the command __editArticle__.
         * The command has the shortcut *Control+Shift+S* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getEditArticleCmd: function(){
            return this.__editArticleCmd;
        },

        /**
         * Returns the command __removeArticle__.
         * The command has the shortcut *Control+Shift+D* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getRemoveArticleCmd: function(){
            return this.__removeArticleCmd;
        }
    },

	/*
    *****************************************************************************
    DESTRUCTOR
    *****************************************************************************
    */
	destruct: function(){
		this.debug("destruct");
		
        this._disposeObjects("__addArticleWindow", "__listView", "__articleView", "__listController", "__addArticleCmd", "__editArticleCmd", "__removeArticleCmd");
    }
});
