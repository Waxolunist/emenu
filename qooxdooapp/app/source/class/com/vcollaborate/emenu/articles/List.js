/**
 * A list to display all {@link com.vcollaborate.emenu.model.Article} of
 * a {@link com.vcollaborate.emenu.model.Category}.
 */
qx.Class.define("com.vcollaborate.emenu.articles.List", {
    extend: qx.ui.core.Widget,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of List.
     */
    construct: function(){
        this.base(arguments);
        
        this.__initLayout();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __stack: null,
        __list: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Initialises the layout of this view.
         * The layout consists of a {@link qx.ui.basic.Label}. Beneath it adds a
         * {@link qx.ui.container.Stack} and adds a {@link qx.ui.form.List} to it.
         *
         * @return {void}
         */
        __initLayout: function(){
            this.debug("__initLayout");
            
            var layout = new qx.ui.layout.VBox();
            layout.setSeparator("separator-vertical");
            this._setLayout(layout);
            
            // Create the header of the list
            //TODO set appearance
            var listHeader = new qx.ui.basic.Label(this.tr("Articles"));
            listHeader.setBackgroundColor("light-background");
            listHeader.setPadding(5);
            listHeader.setAllowGrowX(true);
            listHeader.setFont("bold");
            this._add(listHeader);
            
            // Create the stack for the list
            this.__stack = new qx.ui.container.Stack();
            this._add(this.__stack, {
                flex: 1
            });
            
            // create list view
            this.__list = new qx.ui.form.List();
            this.__list.setSelectionMode("single");
            
            this.__stack.add(this.__list);
        },
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Returns the list widget used in the list view.
         *
         * @return {qx.ui.form.List}
         */
        getList: function(){
            return this.__list;
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
		this.debug("destruct");
		
        this._disposeObjects("__list", "__stack");
    }
});
