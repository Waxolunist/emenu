/**
 * The Font of this application. 
 */
qx.Theme.define("com.vcollaborate.emenu.theme.Font",
{
  //extend : darktheme.theme.Font,
  extend : qx.theme.simple.Font,
  fonts  : {
  	
  	"notification" :
    {
      size : 20,
      family : ["arial", "sans-serif"],
      bold: true
    }

  }
});