/* ************************************************************************



************************************************************************ */

/**
 * The Decoration of this application. 
 */
qx.Theme.define("com.vcollaborate.emenu.theme.Decoration",
{
  //extend      : darktheme.theme.Decoration,
  extend      : qx.theme.simple.Decoration,
  decorations : {
  	
  	"input-html" :
    {
      decorator : qx.ui.decoration.Beveled,

      style :
      {
        outerColor : "border-light",
        innerColor : "black",
        //backgroundImage : "decoration/form/input.png",
        //backgroundRepeat : "repeat-x",
        backgroundColor : "white"
      }
    },
    
    "popupwindow" :
    {
    	include : "window",
    	
      	style :
      	{
        	width : 1,
        	shadowLength : 5,
        	shadowBlurRadius : 5,
        	innerWidth : 1,
        	innerColor: "button-border"
      	}
  	},
  	
  	"notificationbarmessage" :
  	{
  		decorator : [
        	qx.ui.decoration.MBorderRadius,
        	qx.ui.decoration.MBoxShadow,
        	qx.ui.decoration.MSingleBorder,
        	qx.ui.decoration.MBackgroundColor
      	],

	    style :
	    {
	      	width: 1,
	        shadowBlurRadius : 1,
	        shadowColor : "black",
	        radius: 7,
	        color: "white-box-border"
	    }
  	},
  	
  	"shadow-popup" :
    {
    	decorator : qx.ui.decoration.Uniform,

      	style : {
      		width : 0,
        	color : "white-box-border"
      	}
    }
  }
});