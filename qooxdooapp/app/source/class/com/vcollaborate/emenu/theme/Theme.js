/**
 * The Theme of this application. 
 */
qx.Theme.define("com.vcollaborate.emenu.theme.Theme",
{
  meta :
  {
    color      : com.vcollaborate.emenu.theme.Color,
    decoration : com.vcollaborate.emenu.theme.Decoration,
    font       : com.vcollaborate.emenu.theme.Font,
    icon       : qx.theme.icon.Oxygen,
    appearance : com.vcollaborate.emenu.theme.Appearance
  }
});