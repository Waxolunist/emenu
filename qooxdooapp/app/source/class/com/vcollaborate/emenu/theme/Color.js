/**
 * The Color of this application. 
 */
qx.Theme.define("com.vcollaborate.emenu.theme.Color",
{
  //extend : darktheme.theme.Color,
  extend: qx.theme.simple.Color,

  colors :
  {
    "progressive-progressbar-background"         : "transparent",
    "progressive-progressbar-indicator-done"     : "transparent",
    "progressive-progressbar-indicator-undone"   : "transparent",
    "progressive-progressbar-percent-background" : "transparent",
    "progressive-progressbar-percent-text"       : "#000000",
    "text-active"								 : "black"
  }
});