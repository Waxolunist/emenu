/**
 * The Appearance of this application. 
 */
qx.Theme.define("com.vcollaborate.emenu.theme.Appearance",
{
  //extend      : darktheme.theme.Appearance,
  extend      : qx.theme.simple.Appearance,
  appearances : {
  	
    "popupwindow" :
    {
    	
      include : "window",
      style : function(states)
      {
        return {
          decorator : "popupwindow"
        };
      }
    },
    
    "notificationbar":
    {
    	style : function(states)
      	{
        	return {
        		backgroundColor : "transparent",
          		padding : [6, 6],
          		margin: [5, 5, 5, 5],
          		minWidth: 250
        	};
      	}
    },
    
    "notificationbarmessage":
    {
    	style : function(states)
      	{
        	return {
        		backgroundColor : "shadow",
          		decorator : "notificationbarmessage",
          		minWidth: 250,
          		font: "notification"
        	};
      	}
    },
    
    "notificationbarmessage/atom": {
    	style : function(states)
      	{
        	return {
        		iconPosition : "left",
        		padding : 5,
        		allowGrowY: false,
        		opacity: 1
        	};
      	}
    },
    
    "notificationbarmessage/atom/label": {
    	style : function(states)
      	{
        	return {
        		textColor: "white"
        	};
      	}
    }
  	
  }
});