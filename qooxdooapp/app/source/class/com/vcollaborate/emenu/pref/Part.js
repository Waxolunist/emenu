/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/apps/preferences-theme.png)
 ************************************************************************ */
/**
 * This part adds the possibility to change the language.
 */
qx.Class.define("com.vcollaborate.emenu.pref.Part", {
    extend: com.vcollaborate.emenu.utils.AbstractPart,
    type: "singleton",
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of part.
     */
    construct: function(){
        this.debug("construct");
        
        this.base(arguments);
        
        if (!this.__prefWindow) {
            this.__prefWindow = new com.vcollaborate.emenu.pref.PreferenceWindow();
        }
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __prefWindow: null,
        
        /*
         *****************************************************************************
         DERIVED
         *****************************************************************************
         */
        /**
         * Adds the window for changing the language.
         *
         * @param controller {com.vcollaborate.emenu.Application} The main application class
         * @return {void}
         */
        init: function(controller){
            this.base(arguments);
            
            controller.getRoot().add(this.__prefWindow);
        },
        
        /**
         * Inits the preferences command.
         *
         * @param commands {var} the global commands object
         * @return {void}
         */
        initCommands: function(commands){
            this.base(arguments);
            
            commands.preferences = new qx.ui.core.Command("Control+P");
			commands.preferences.addListener("execute", this.__showPreferences, this);
        },
        
        /**
         * Inits the buttons for the toolbar, one for each command.
         *
         * @see #initCommands
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @param part {qx.ui.toolbar.Part} the part to add the buttons to
         * @return {void}
         */
        initButtons: function(commands, buttons, part){
            this.base(arguments);
            
            buttons.preferences = new qx.ui.toolbar.Button(this.tr("Preferences"), "icon/22/apps/preferences-theme.png");
            buttons.preferences.setCommand(commands.preferences);
            part.addAt(buttons.preferences, 0);
			
			part.show();
        },
        
        /**
         * Sets the tooltips for each button of this part.
         *
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @return {void}
         */
        setToolTips: function(commands, buttons){
            this.base(arguments);
            
            buttons.preferences.setToolTipText(commands.preferences.toString());
        },
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Opens the preferences window
         *
         * @return {void}
         */
        __showPreferences: function(){
            this.debug("__showPreferences");
            
            this.__prefWindow.center();
            this.__prefWindow.open();
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this._disposeObjects("__prefWindow");
    }
});
