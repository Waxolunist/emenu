/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-ok.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-cancel.png)
 #asset(qx/icon/${qx.icontheme}/22/apps/preferences-theme.png)
 #asset(qx/icon/${qx.icontheme}/22/apps/preferences-locale.png)
 #asset(qx/icon/${qx.icontheme}/22/status/dialog-password.png)
 ************************************************************************ */
/**
 * The preference window
 */
qx.Class.define("com.vcollaborate.emenu.pref.PreferenceWindow", {
    extend: qx.ui.window.Window,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
	/**
	 * Creates a new instance of PreferenceWindow.
	 */
    construct: function(){
        this.base(arguments, this.tr("Preferences"), "icon/22/apps/preferences-theme.png");
        
        // set the properties of the window
        this.set({
            modal: true,
            showMinimize: false,
            showMaximize: false,
            allowMaximize: false
        });
        
        this.setResizableBottom(false);
        this.setResizableTop(false);
        
        // Create the content with a helper
        this._addContent();
        
        this.addListener("keypress", function(e){
            var key = e.getKeyIdentifier();
            
            if (key == "Escape") {
                this.close();
            }
        }, this);
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /**
         * Adds the content of the window.
         *
         * @return {void}
         */
        _addContent: function(){
            // Set the layout of the window
            var windowLayout = new qx.ui.layout.VBox(10);
            this.setLayout(windowLayout);
            this.setMinWidth(350);
            
            // Create and add groupboxes
            var groupBoxLang = new qx.ui.groupbox.GroupBox(this.tr("Language"), "icon/22/apps/preferences-locale.png");
            groupBoxLang.setMinWidth(150);
            groupBoxLang.setLayout(new qx.ui.layout.VBox());
            this.add(groupBoxLang);
            
            /*
            var groupBoxSecret = new qx.ui.groupbox.GroupBox(this.tr("Secret"), "icon/22/status/dialog-password.png");
            groupBoxSecret.setMinWidth(150);
            groupBoxSecret.setLayout(new qx.ui.layout.VBox());
            this.add(groupBoxSecret);
           	*/
           	
            // Create radio manager
            var radioManager = new qx.ui.form.RadioGroup();
            
            // Create the radio buttons for the languages
            var languages = {
                "en": "English",
                "de": "Deutsch"
            };
            
            var localeManager = qx.locale.Manager.getInstance();
            
            // Check for a mismatch of available and used locales
            if (qx.core.Environment.get("qx.debug")) {
                var availableLocales = localeManager.getAvailableLocales().sort().join(", ");
                var usedLocales = qx.lang.Object.getKeys(languages).sort().join(", ");
                
                if (availableLocales !== usedLocales) {
                    this.warn("Mismatch of locales: \navailable: " + availableLocales + "\nused     : " + usedLocales);
                }
            }
            
            var radioButton;
            
            var first = 0;
            
            for (var lang in languages) {
                radioButton = new qx.ui.form.RadioButton(languages[lang]);
                radioButton.setUserData("language", lang);
                
                // add to radioManager and groupBox
                radioManager.add(radioButton);
                groupBoxLang.add(radioButton);
                
                // Select entry containing current language
                if (localeManager.getLanguage() == lang) {
                    radioManager.setSelection([radioButton]);
                    radioButton.focus();
                }
            }
            
            // add the button bar
            var buttonBarLayout = new qx.ui.layout.HBox(10, "right");
            var buttonBar = new qx.ui.container.Composite(buttonBarLayout);
            
            var cancelButton = new qx.ui.form.Button(this.tr("Cancel"), "icon/22/actions/dialog-cancel.png");
            cancelButton.addListener("execute", this.close, this);
            
            var okButton = new qx.ui.form.Button(this.tr("OK"), "icon/22/actions/dialog-ok.png");
            
            okButton.addListener("execute", function(e){
                var selectedLanguage = radioManager.getSelection()[0].getUserData("language");
                
                qx.io.PartLoader.require([selectedLanguage], function(){
                    qx.locale.Manager.getInstance().setLocale(selectedLanguage);
                }, this);
                
                this.close();
            }, this);
            
            buttonBar.add(cancelButton);
            buttonBar.add(okButton);
            
            this.add(buttonBar);
        }
    }
});