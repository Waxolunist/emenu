/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-apply.png)
 ************************************************************************ */
/**
 * Choose an icon
 */
qx.Class.define("com.vcollaborate.emenu.view.RandomKeyGeneratorButton", {

    extend: com.vcollaborate.emenu.view.BindingButton,
    
    construct: function(label){
        this.base(arguments, label);
        this.addListener("execute", this.__execute, this);
    },
    
    members: {
        __randomKeyGenerator: null,
        
        __execute: function(e){
            this.debug("__execute");
            
            if (!this.__randomKeyGenerator) {
                this.__randomKeyGenerator = new com.vcollaborate.emenu.view.RandomKeyGenerator();
                this.__randomKeyGenerator.addListener("randomKeyGenerated", this.__randomKeyGenerated, this);
                qx.core.Init.getApplication().getRoot().add(this.__randomKeyGenerator);
            }
            
            this.__randomKeyGenerator.center();
            this.__randomKeyGenerator.open();
        },
        
        __randomKeyGenerated: function(e) {
        	this.debug("__randomKeyGenerated");
        	
        	this.setValue(e.getData());
        },
        
        _applyValue: function(value){
            this.debug("_applyValue");
            
            if(value) {
				this.setIcon("icon/22/actions/dialog-apply.png");
				this.setLabel(this.tr("Key Generated"));	
			} else {
				this.setIcon(null);
				this.setLabel(this.tr("Generate Key"));
			}
        }
        

    }

});
