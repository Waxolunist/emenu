/* ************************************************************************
 ************************************************************************ */
/**
 * Choose an icon
 */
qx.Class.define("com.vcollaborate.emenu.view.PopupWindow", {
    extend: qx.ui.window.Window,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of IconChooser.
     */
    construct: function(){
        this.base(arguments);
        
        this.set({
            modal: true,
            showMinimize: false,
            showMaximize: false,
            allowMaximize: false,
            allowGrowX: true,
            allowGrowY: true
        });
        
        this.setResizableBottom(false);
        this.setResizableTop(false);
        this.setResizableRight(false);
        this.setResizableLeft(false);
        this._excludeChildControl("captionbar");
        this.setAppearance("popupwindow");
    }
});
