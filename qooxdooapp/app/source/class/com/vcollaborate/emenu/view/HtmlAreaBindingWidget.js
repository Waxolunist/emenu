/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/format-text-bold.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/format-text-italic.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/format-text-underline.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/format-text-strikethrough.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/format-justify-left.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/format-justify-center.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/format-justify-right.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/format-justify-fill.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/format-indent-more.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/format-indent-less.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/edit-undo.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/edit-redo.png)
 #asset(com/vcollaborate/emenu/images/format-list-ordered.png)
 #asset(com/vcollaborate/emenu/images/format-list-unordered.png)
 #asset(com/vcollaborate/emenu/blank.html)
 ************************************************************************ */
/**
 * Rich text editor widget which encapsulates the low-level {@link qx.bom.htmlarea.HtmlArea}
 * component to offer editing features.
 *
 * Optimized for the use at a RIA.
 */
qx.Class.define("com.vcollaborate.emenu.view.HtmlAreaBindingWidget", {

    extend: qx.ui.groupbox.GroupBox,
    implement: [qx.ui.form.IStringForm, qx.ui.form.IForm],
    include: [qx.ui.form.MForm],
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Constructor
     *
     * @param value {String} Initial content
     * @param styleInformation {String | Map | null} {@link qx.ui.embed.HtmlArea}
     * @param source {String} source of the iframe
     */
    construct: function(legend, icon){
        this.base(arguments, legend, icon);
        
        this.setWidth(900);
        this.setLayout(new qx.ui.layout.VBox(0));
        
        this.getChildControl("toolbar");
        
        this.getChildControl("htmlArea").addListener("focused", this.__readContent, this);
        this.getChildControl("htmlArea").addListener("focusOut", this.__readContent, this);
        
        this.getChildControl("htmlArea").addListenerOnce("ready", this.emptyContent, this);
    },
    
    members: {
        // overridden
        _createChildControlImpl: function(id, hash){
            this.debug("_createChildControlImpl: " + id);
            
            var control;
            
            switch (id) {
                case "toolbar":
                    control = this.__setupToolbar();
                    this.addAt(control, 0);
                    break;
                    
                case "htmlArea":
                    var htmlDecorator = new qx.ui.decoration.Single(1, "solid", "border-main");
                    htmlDecorator.setWidthTop(0);
                    control = new qx.ui.embed.HtmlArea("", null, "resource/com/vcollaborate/emenu/blank.html");
                    control.set({
                        minHeight: 200
						//,
                        //defaultFontFamily: "Verdana,Geneva,Arial,Helvetica,sans-serif",
                        //defaultFontSize: 3
                    });
                    control.setDecorator(htmlDecorator);
                    this.addAt(control, 1, {
                        flex: 1,
                        height: "100%"
                    });
                    
                    break;
            }
            
            return control || this.base(arguments, id);
        },
        
        /**
         * Creates the toolbar entries
         *
         * @return {qx.ui.toolbarToolBar} toolbar widget
         */
        __setupToolbar: function(){
            this.debug("__setupToolbar");
            
            var toolbar = new qx.ui.toolbar.ToolBar().set({
                decorator: new qx.ui.decoration.Single().set({
                    top: [1, "solid", "border-separator"],
                    left: [1, "solid", "border-separator"],
                    right: [1, "solid", "border-separator"]
                })
            });
            
            // Put together toolbar entries
            var button;
            var toolbarEntries = this.__getToolbarEntries();
            
            for (var i = 0, j = toolbarEntries.length; i < j; i++) {
                var part = new qx.ui.toolbar.Part;
                toolbar.add(part);
                
                for (var entry in toolbarEntries[i]) {
                    var infos = toolbarEntries[i][entry];
                    
                    if (infos.custom) {
                        button = infos.custom.call(this);
                    }
                    else {
                        button = new qx.ui.toolbar.Button(null, infos.image);
                        
                        button.set({
                            focusable: false,
                            keepFocus: true,
                            center: true,
                            toolTipText: infos.text ? infos.text : ""
                        });
                        
                        button.addListener("execute", infos.action, this.getChildControl("htmlArea"));
                    }
                    
                    part.add(button);
                }
            }
            
            return toolbar;
        },
        
        __getToolbarEntries: function(){
            this.debug("__getToolbarEntries");
            var htmlArea = this.getChildControl("htmlArea");
            return [{
                bold: {
                    text: this.tr("Format Bold"),
                    image: "icon/22/actions/format-text-bold.png",
                    action: htmlArea.setBold
                },
                italic: {
                    text: this.tr("Format Italic"),
                    image: "icon/22/actions/format-text-italic.png",
                    action: htmlArea.setItalic
                },
                underline: {
                    text: this.tr("Format Underline"),
                    image: "icon/22/actions/format-text-underline.png",
                    action: htmlArea.setUnderline
                },
                strikethrough: {
                    text: this.tr("Format Strikethrough"),
                    image: "icon/22/actions/format-text-strikethrough.png",
                    action: htmlArea.setStrikeThrough
                }
                //                ,removeFormat: {
                //                    text: "Remove Format",
                //                    image: "icon/22/actions/edit-clear.png",
                //                    action: this.__htmlArea.removeFormat
                //                }
            }, {
                alignLeft: {
                    text: this.tr("Align Left"),
                    image: "icon/22/actions/format-justify-left.png",
                    action: htmlArea.setJustifyLeft
                },
                alignCenter: {
                    text: this.tr("Align Center"),
                    image: "icon/22/actions/format-justify-center.png",
                    action: htmlArea.setJustifyCenter
                },
                alignRight: {
                    text: this.tr("Align Right"),
                    image: "icon/22/actions/format-justify-right.png",
                    action: htmlArea.setJustifyRight
                },
                alignJustify: {
                    text: this.tr("Align Justify"),
                    image: "icon/22/actions/format-justify-fill.png",
                    action: htmlArea.setJustifyFull
                }
            }, {
                indent: {
                    text: this.tr("Indent More"),
                    image: "icon/22/actions/format-indent-more.png",
                    action: htmlArea.insertIndent
                },
                outdent: {
                    text: this.tr("Indent Less"),
                    image: "icon/22/actions/format-indent-less.png",
                    action: htmlArea.insertOutdent
                }
            }, {
                ol: {
                    text: this.tr("Insert Ordered List"),
                    image: "com/vcollaborate/emenu/images/format-list-ordered.png",
                    action: htmlArea.insertOrderedList
                },
                ul: {
                    text: this.tr("Inserted Unordered List"),
                    image: "com/vcollaborate/emenu/images/format-list-unordered.png",
                    action: htmlArea.insertUnorderedList
                }
            }, {
                undo: {
                    text: this.tr("Undo Last Change"),
                    image: "icon/22/actions/edit-undo.png",
                    action: htmlArea.undo
                },
                redo: {
                    text: this.tr("Redo Last Undo Step"),
                    image: "icon/22/actions/edit-redo.png",
                    action: htmlArea.redo
                }
            }];
        },
        
        __readContent: function(e){
            this.debug("__readContent");
			
			if(this.getChildControl("htmlArea").getContentBody().innerHTML == "<p>&nbsp;</p>") {
				this.getChildControl("htmlArea").getContentBody().innerHTML = "<p></p>";
			}
			
            this.setContent(this.getChildControl("htmlArea").getComputedValue(true));
        },
        
        __applyContent: function(value){
            this.debug("__applyContent: " + value);
            
            if (this.getChildControl("htmlArea").getContentBody()) {
                this.getChildControl("htmlArea").getContentBody().innerHTML = value;
            }
        },
        
        __transformContent: function(value){
            if (qx.lang.String.startsWith(value, "<p>")) {
                return value;
            } 
			else if (qx.lang.String.startsWith(value, "<p>&nbsp;")) {
				return "<p>" + value + "</p>";
			}
			else {
				return "<p>" + value + "</p>";
			}
        },
        
        
        emptyContent: function(){
            this.debug("emptyContent");
            
            this.getChildControl("htmlArea").setValue("");
            this.setContent("");
            this.getChildControl("htmlArea").resetHtml();
//			this.getChildControl("htmlArea").setValue("");
        },
        
        getValue: function(){
            this.debug("getValue");
            
            return this.getContent();
        },
        
        resetValue: function(){
            this.debug("resetValue");
            
            this.emptyContent();
        },
        
        setValue: function(value){
            this.debug("setValue");
            
            if (value) {
                this.setContent(value);
            }
            else {
                this.emptyContent();
            }
        },
        
        setPlaceholder: function(placeHolder){
            this.debug("setPlaceholder");
            
            this.setLegend(placeHolder);
        }
    },
    
    properties: {
        content: {
            init: null,
            nullable: true,
            check: "String",
            event: "changeValue",
            apply: "__applyContent",
            transform: "__transformContent"
        }
    }
});
