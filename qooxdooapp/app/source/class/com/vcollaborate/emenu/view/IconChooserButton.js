/**
 * Choose an icon
 */
qx.Class.define("com.vcollaborate.emenu.view.IconChooserButton", {

    extend: com.vcollaborate.emenu.view.BindingButton,
    
    construct: function(){
        this.base(arguments, "...");
        this.addListener("execute", this.__execute, this);
    },
    
    members: {
        __iconChooserWindow: null,
        
        __execute: function(e){
            this.debug("__execute");
            
            if (!this.__iconChooserWindow) {
                this.__iconChooserWindow = new com.vcollaborate.emenu.view.IconChooser();
                this.__iconChooserWindow.addListener("iconSelected", this.__iconSelected, this);
                qx.core.Init.getApplication().getRoot().add(this.__iconChooserWindow);
            }
            
            this.__iconChooserWindow.center();
            this.__iconChooserWindow.open();
        },
        
        __iconSelected: function(e){
            this.debug("__iconSelected");
            
            this.setValue(e.getData());
            
        },
        
        _applyValue: function(value){
            this.debug("_applyValue");
            
			if(value) {
				this.setIcon(value);
				this.setLabel(null);	
			} else {
				this.setIcon(null);
				this.setLabel("...");
			}
        }
    }

});
