/* ************************************************************************
 ************************************************************************ */
/**
 * Choose an icon
 */
qx.Class.define("com.vcollaborate.emenu.view.RegisterUserDialog", {
    extend: com.vcollaborate.emenu.view.PopupWindow,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of IconChooser.
     */
    construct: function(self){
        this.base(arguments);
        
        this.set({
            minWidth: 200,
            height: 100
        });
        
        this.__self = self;
        
        this.__addContent();
        this.__addValidator();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __container: null,
        __okButton: null,
        __cancelButton : null,
        __usernameField: null,
        __passwordField: null,
        __passwordRepeatField: null,
        __emailField: null,
        __keyField: null,
        __self: null,
        __validationmanager: null,
        
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Adds the content of the window.
         *
         * @return {void}
         */
        __addContent: function(){
            this.debug("__addContent");            
            
            this.setLayout(new qx.ui.layout.VBox(10));
            
            this.__container = new qx.ui.container.Composite();
            var layout = new qx.ui.layout.Grid();
            layout.setSpacing(6);
    		layout.setColumnFlex(0, 1);
    		layout.setColumnAlign(0, "right", "middle");
        
        	this.__container.setLayout(layout);
            
            this.__usernameField = new qx.ui.form.TextField().set({required:true});
			this.__emailField = new qx.ui.form.TextField().set({required:true});;
      		this.__passwordField = new qx.ui.form.PasswordField().set({required:true});;
      		this.__passwordRepeatField = new qx.ui.form.PasswordField().set({required:true});;
      		this.__keyField = new com.vcollaborate.emenu.view.RandomKeyGeneratorButton(this.tr("Generate Key")).set({required:true});;
            
			var labels = [this.tr("Name"), this.tr("Email"), this.tr("Password"), this.tr("Password Repeat"), this.tr("Key")];
			var fields = [this.__usernameField, this.__emailField, this.__passwordField, this.__passwordRepeatField, this.__keyField];
			
			for (var i=0; i<labels.length; i++) {
		    	this.__container.add(new qx.ui.basic.Label(labels[i]).set({
		        	allowShrinkX: false
		        }), {row: i, column : 0});
		        
		        this.__container.add(fields[i].set({
        			allowShrinkX: false
      			}), {row: i, column : 1});
		    }
      		
	      	this.__okButton =  new qx.ui.form.Button(this.tr("Register")).set({
	      		allowStretchX: false,
	      		width: 100
	      	});
      		this.__cancelButton =  new qx.ui.form.Button(this.tr("Cancel")).set({
	      		allowStretchX: false,
	      		width: 100
	      	});

			var buttonContainer = new qx.ui.container.Composite(new qx.ui.layout.HBox(5)).set({
        		allowGrowX: true
      		});

			buttonContainer.add(this.__cancelButton);
			buttonContainer.add(this.__okButton);
			
      		this.__container.add(
        		buttonContainer,
        		{
          			row : 5,
          			column : 1
        		}
      		);
      		this.add(this.__container);
        },
        
        /**
         * Inits the validator
         *
         * @return {void}
         */
        __addValidator: function(){
        	this.debug("__addValidator");
        	
        	this.__validationmanager = new qx.ui.form.validation.Manager();
        	this.__validationmanager.add(this.__usernameField, com.vcollaborate.emenu.utils.Validate.username);
        	this.__validationmanager.add(this.__emailField, qx.util.Validate.email());
      		this.__validationmanager.add(this.__passwordField, com.vcollaborate.emenu.utils.Validate.password);
      		this.__validationmanager.add(this.__passwordRepeatField);
      		this.__validationmanager.add(this.__keyField);
      		
      		
      		var oThis = this;
      		this.__validationmanager.setValidator(function(items) {
        		var valid = oThis.__passwordField.getValue() == oThis.__passwordRepeatField.getValue();
        		if (!valid) {
          			var message = qx.locale.Manager.tr("Passwords must be equal.");
          			oThis.__passwordField.setInvalidMessage(message);
          			oThis.__passwordRepeatField.setInvalidMessage(message);
          			oThis.__passwordField.setValid(false);
          			oThis.__passwordRepeatField.setValid(false);
        		}
        		return valid;
      		});
        },
        
        /**
         * Inits the listeners
         *
         * @return {void}
         */
        __initListeners: function(){
            this.debug("__initListeners");
            
            this.addListener("keypress", function(e){
                var key = e.getKeyIdentifier();
                
                if(key == "Enter") {
					this.__checkInput(e);
				}
            }, this);
            
            /* Check input on click */
      		this.__okButton.addListener("execute", this.__checkInput, this);
      		this.__cancelButton.addListener("execute", this.close, this);
      		this.addListener("close", this.__clear, this);
      		
      		this.__keyField.addListener("changeValue", this.__keyGenerated, this);
        },
        
        __checkInput: function() {
        	this.debug("__checkInput");
        	this.__validationmanager.validate();
        	if(this.__validationmanager.getValid()) {
        		this.fireDataEvent("registered", {
        			username: this.__usernameField.getValue(),
        			email: this.__emailField.getValue(),
        			password: this.__passwordField.getValue(),
        			key: this.__keyField.getValue()
        		});
        		this.close();	
        	}
        },
        
        __clear: function() {
        	this.debug("__clear");
			this.__usernameField.resetValue();
			this.__emailField.resetValue();
			this.__passwordField.resetValue();
			this.__passwordRepeatField.resetValue();
			this.__keyField.resetValue();
			
			this.__usernameField.setValid(true);
			this.__emailField.setValid(true);
			this.__passwordField.setValid(true);
			this.__passwordRepeatField.setValid(true);
			this.__keyField.setValid(true);
        },
        
        __keyGenerated: function() {
        	this.debug("__keyGenerated");
			this.__keyField.setValid(true);
        }
    },
	
    /*
     *****************************************************************************
     EVENTS
     *****************************************************************************
     */
    events: {
        /**
         * Fired when the registration is valid was successful.
         */
        "registered": "qx.event.type.Data"
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this._disposeObjects("__container", "__okButton", "__usernameField",
        		"__passwordField", "__passwordRepeatField", "__validationmanager", "__cancelButton");
    }
            
        
});
