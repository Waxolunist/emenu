/* ************************************************************************
 ************************************************************************ */
/**
 * Choose an icon
 */
qx.Class.define("com.vcollaborate.emenu.view.LoginDialog", {
    extend: com.vcollaborate.emenu.view.PopupWindow,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of IconChooser.
     */
    construct: function(loginFunction, successFunction, registerFunction, self, labels){
        this.base(arguments);
        
        this.set({
        	alwaysOnTop: true,
            minWidth: 200,
            height: 100
        });
        
        this.__loginFunction = loginFunction;
        this.__successFunction = successFunction;
        this.__registerFunction = registerFunction; 
        this.__self = self;
        this.__labels = labels || null;
        if(!this.__labels) {
        	this.__labels = {};
        	this.__labels.title = this.tr("<b>Login to EMenu</b>");
        	this.__labels.name = this.tr("Name");
        	this.__labels.password = this.tr("Password");
        	this.__labels.login = this.tr("Login");
        	this.__labels.register = this.tr("Register");
        }
        
        this.__addContent();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __container: null,
        __okButton: null,
        __registerButton: null,
        __usernameField: null,
        __passwordField: null,
        __loginFunction: null,
        __successFunction: null,
        __registerFunction: null,
        __effect: null,
        __self: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Adds the content of the window.
         *
         * @return {void}
         */
        __addContent: function(){
            this.debug("__addContent");
            
            this.setLayout(new qx.ui.layout.VBox(10));
            
            this.__container = new qx.ui.container.Composite();
            var layout = new qx.ui.layout.Grid();
            layout.setSpacing(6);
            layout.setColumnFlex(0, 1);
      		layout.setColumnAlign(0, "right", "middle");
        
        	this.__container.setLayout(layout);
            
        	this.__container.add(new qx.ui.basic.Label(this.__labels.title).set({
		       	allowShrinkX: false,
		      	rich: true,
		      	alignX: "center"
		    }), {row: 0, column : 0, colSpan: 2});
        	
			var labels = [this.__labels.name, this.__labels.password];
			for (var i=0; i<labels.length; i++) {
		    	this.__container.add(new qx.ui.basic.Label(labels[i]).set({
		        	allowShrinkX: false
		        }), {row: i+1, column : 0});
		    }
		    
      		this.__usernameField = new qx.ui.form.TextField();
      		this.__passwordField = new qx.ui.form.PasswordField();

      		this.__container.add(this.__usernameField.set({
        		allowShrinkX: false
      		}), {row: 1, column : 1});

      		this.__container.add(this.__passwordField.set({
        		allowShrinkX: false
      		}), {row: 2, column : 1});
      		
      		// Splitbutton
      		var menu = new qx.ui.menu.Menu;
      		this.__registerButton = new qx.ui.menu.Button(this.__labels.register);
      		menu.setMinWidth(120);
      		menu.add(this.__registerButton);

      		// Create opener button
      		if(this.__registerFunction) {
      			this.__okButton = new qx.ui.form.SplitButton(this.__labels.login, null, menu);
      		} else {
	      		this.__okButton =  new qx.ui.form.Button(this.__labels.login);
      		}
      		this.__okButton.setAllowStretchX(false);

      		this.__container.add(
        		this.__okButton,
        		{
          			row : 3,
          			column : 1
        		}
      		);
      		this.add(this.__container);
        },
        
        /**
         * Inits the listeners
         *
         * @return {void}
         */
        __initListeners: function(){
            this.debug("__initListeners");
            
            this.addListener("keypress", function(e){
                var key = e.getKeyIdentifier();
                
                if(key == "Enter") {
					this.__checkInput(e);
				}
            }, this);
            
            /* Check input on click */
      		this.__okButton.addListener("execute", this.__checkInput, this);
      		if(this.__registerFunction) {
				this.__registerButton.addListener("execute", this.__callRegisterFunction, this);
			}
      		/* Prepare effect as soon as the container is ready */
      		this.addListener("appear", this.__prepareEffect, this);
      		
      		this.addListener("close", this.__clear, this);
        },
        
        __callRegisterFunction: function() {
        	this.__registerFunction.call(this.__self);
        },
        
        __checkInput: function() {
        	this.debug("__checkInput");
        	var loginSuccessful = this.__loginFunction.call(this.__self, this.__usernameField.getValue(), this.__passwordField.getValue());
        	
        	if(loginSuccessful) {
        		this.__successFunction.call(this.__self, this);
				this.fireDataEvent("loginSuccessful", null);        		
        	} else {
        		this.debug("Start effect.");
        		this.__effect.start();
        		this.fireDataEvent("loginError", null);
        	}
        },
        
        __prepareEffect: function() {
        	this.__effect = new qx.fx.effect.combination.Shake(this.getContainerElement().getDomElement());
        },
        
        __clear: function() {
			this.__usernameField.resetValue();
			this.__passwordField.resetValue();
        }
    },
	
    /*
     *****************************************************************************
     EVENTS
     *****************************************************************************
     */
    events: {
        /**
         * Fired when the login was successful.
         */
        "loginSuccessful": "qx.event.type.Data",
        
        /**
         * Fired when the login was not successful.
         */
        "loginError": "qx.event.type.Data"
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this._disposeObjects("__container", "__okButton", "__usernameField",
        		"__passwordField", "__loginFunction", "__successFunction", "__registerFunction",
        		"__effect", "__registerButton");
    }
            
        
});
