qx.Class.define("com.vcollaborate.emenu.view.UrlCellRenderer", {
    extend: qx.ui.table.cellrenderer.Html,
    
    construct: function(align, color, style, weight){
        this.base(arguments, align, color, style, weight);
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        // overridden
        _getContentHtml: function(cellInfo){
			if (com.vcollaborate.emenu.utils.Validate.notEmpty(cellInfo.value)) {
				return '<a href="' + cellInfo.value + '" target="_blank">' + cellInfo.value + '</a>';
			}
			return com.vcollaborate.emenu.utils.String.EMPTY;
        }
        
    }
});
