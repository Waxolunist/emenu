qx.Class.define("com.vcollaborate.emenu.view.PasswordFieldHelper", {
    type: "static",
    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
        toggleType: function(e){
            this.debug("toggleType");
            var element = this.getContentElement();
            var type = element.getAttribute("type");
            if (type == "text") {
                element.setAttribute("type", "password");
            }
            else {
                element.setAttribute("type", "text");
            }
        },
        
        resetType: function(e){
			this.debug("resetType");
            var element = this.getContentElement();
            element.setAttribute("type", "password");
        }
        
    }
});
