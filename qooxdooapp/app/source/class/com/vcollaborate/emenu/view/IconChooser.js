/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/insert-image.png)
 #asset(com/vcollaborate/emenu/images/nuvola/*)
 ************************************************************************ */
/**
 * Choose an icon
 */
qx.Class.define("com.vcollaborate.emenu.view.IconChooser", {
    extend: qx.ui.window.Window,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of IconChooser.
     */
    construct: function(numberOfColumns){
        this.base(arguments, this.tr("Select an icon"), "icon/22/actions/insert-image.png");
        
        // set the properties of the window
        this.set({
            modal: true,
            showMinimize: false,
            showMaximize: false,
            allowMaximize: false,
            allowGrowX: true,
            allowGrowY: true,
            minWidth: 200,
            height: 200
        });
        
        this.__numberOfColumns = numberOfColumns || 5;
        
        this.__initImageList();
        this.__addContent();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __currentSelectedImage: null,
        __gridLayout: null,
        __scrollContainer: null,
        __numberOfColumns: null,
        __imageList: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Adds the content of the window.
         *
         * @return {void}
         */
        __addContent: function(){
            this.debug("__addContent");
            
			this.setLayout(new qx.ui.layout.Dock(10,10));
            
            this.__gridLayout = new qx.ui.layout.Grid(4, 4);
            this.__contentPane = new qx.ui.container.Composite(this.__gridLayout);
            this.__scrollContainer = new qx.ui.container.Scroll(this.__contentPane).set({
                allowGrowX: true,
                allowGrowY: true
            });
            
            this.__addImages(this.__numberOfColumns, this.__contentPane);
   			
			var okButton = new qx.ui.form.Button(this.tr("Select"));
			okButton.addListener("execute", this.__selectIcon, this);
			
            this.add(this.__scrollContainer, {edge:"center"});
			this.add(okButton, {edge:"south"});
        },
        
        /**
         * Inits the listeners
         *
         * @return {void}
         */
        __initListeners: function(){
            this.debug("__initListeners");
            
            this.addListener("resize", this.__onPaneResize, this);
            
            this.addListener("keypress", function(e){
                var key = e.getKeyIdentifier();
                
                if (key == "Escape") {
                    this.close();
                }
                else if (key == "Right" || key == "Left" || key == "Up" || key == "Down") {
                    this.__moveSelection(e);
                } else if(key == "Enter") {
					this.__selectIcon(e);
				}
            }, this);
			
			this.addListener("close", this.__resetWindow, this);
        },
        
        /**
         * Choose the image
         */
        __styleSelectedImage: function(image){
            this.debug("__styleSelectedImage");
            
            //TODO setAppearance instead
            if (this.__currentSelectedImage) {
                this.__currentSelectedImage.setBackgroundColor("white");
            }
            
            if (image) {
                image.setBackgroundColor("background-selected");
            }
            this.__currentSelectedImage = image;
        },
        
		/**
		 * Moves the selection with keys
		 * 
		 * @param e {Event} e
		 */
        __moveSelection: function(e){
            this.debug("__moveSelection");
            
            var key = e.getKeyIdentifier();
            
            if (this.__currentSelectedImage) {
                var layoutproperties = this.__currentSelectedImage.getLayoutProperties();
                
                var row = layoutproperties.row;
                var column = layoutproperties.column;
                var rowCount = this.__gridLayout.getRowCount();
                var columnCount = this.__gridLayout.getColumnCount();
                
                var newRow = row;
                var newColumn = column;
                
                if (key == "Right") {
                    if (column < (columnCount - 1)) 
                        newColumn++;
                }
                else if (key == "Left") {
                    if (column > 0) 
                        newColumn--;
                }
                else if (key == "Up") {
                    if (row > 0) 
                        newRow--;
                }
                else if (key == "Down") {
                    if (row < (rowCount - 1)) 
                        newRow++;
                }
                else {
                    return;
                }
                
                var image = this.__gridLayout.getCellWidget(newRow, newColumn);
                if (image) {
                    this.__scrollContainer.scrollChildIntoView(image);
                    this.__styleSelectedImage(image);
                }
            }
        },
        
        /**
         * The actions on resize
         */
        __onPaneResize: function(e){
            this.debug("__onPaneResize");
            if (!this.isSeeable()) {
                this.center();
                this.__styleSelectedImage(null);
            }
            else if (this.getWidth()) {
                this.__contentPane.removeAll();
				//TODO better calculation because of appearance
                this.__addImages(Math.floor((this.getWidth() - 25) / 30), this.__contentPane);
                this.__styleSelectedImage(this.__currentSelectedImage);
            }
        },
        
		__resetWindow: function(e) {
			this.center();
            this.__styleSelectedImage(null);
			this.setWidth(180);
			this.resetHeight();
			this.__scrollContainer.scrollToY(0);
		},
		
        /**
         *
         *
         * @param numberOfItemsPerLine {Integer} number of items per line to show
         * @param contentPane {qx.ui.core.Widget} content
         */
        __addImages: function(numberOfItemsPerLine, contentPane){
            this.debug("__addImages");
            
            this.__imageList.forEach(function(image, index){
                contentPane.add(image, {
                    row: Math.floor(index / numberOfItemsPerLine),
                    column: index % numberOfItemsPerLine
                });
            }, this);
        },
        
        /**
         * Inits the list of images
         */
        __initImageList: function(){
            this.debug("__initImageList");
            
            if (!this.__imageList) {
                this.__imageList = [];
            }
            
            var imageArray = ["C00_Password.png", "C01_Package_Network.png", "C02_MessageBox_Warning.png", "C03_Server.png", "C04_Klipper.png", "C05_Edu_Languages.png", "C06_KCMDF.png", "C07_Kate.png", "C08_Socket.png", "C09_Identity.png", "C10_Kontact.png", "C11_Camera.png", "C12_IRKickFlash.png", "C13_KGPG_Key3.png", "C14_Laptop_Power.png", "C15_Scanner.png", "C16_Mozilla_Firebird.png", "C17_CDROM_Unmount.png", "C18_Display.png", "C19_Mail_Generic.png", "C20_Misc.png", "C21_KOrganizer.png", "C22_ASCII.png", "C23_Icons.png", "C24_Connect_Established.png", "C25_Folder_Mail.png", "C26_FileSave.png", "C27_NFS_Unmount.png", "C28_QuickTime.png", "C29_KGPG_Term.png", "C30_Konsole.png", "C31_FilePrint.png", "C32_FSView.png", "C33_Run.png", "C34_Configure.png", "C35_KRFB.png", "C36_Ark.png", "C37_KPercentage.png", "C38_Samba_Unmount.png", "C39_History.png", "C40_Mail_Find.png", "C41_VectorGfx.png", "C42_KCMMemory.png", "C43_EditTrash.png", "C44_KNotes.png", "C45_Cancel.png", "C46_Help.png", "C47_KPackage.png", "C48_Folder.png", "C49_Folder_Blue_Open.png", "C50_Folder_Tar.png", "C51_Decrypted.png", "C52_Encrypted.png", "C53_Apply.png", "C54_Signature.png", "C55_Thumbnail.png", "C56_KAddressBook.png", "C57_View_Text.png", "C58_KGPG.png", "C59_Package_Development.png", "C60_KFM_Home.png", "C61_Services.png", "C62_Tux.png", "C63_Feather.png", "C64_Apple.png", "C65_Apple.png", "C66_Money.png", "C67_Certificate.png", "C68_BlackBerry.png"];
            
            imageArray.forEach(function(imagename, index){
                //TODO appearance
                var image = new qx.ui.basic.Image("com/vcollaborate/emenu/images/nuvola/" + imagename).set({
                    decorator: "main",
                    padding: 4,
                    backgroundColor: "white",
                    width: com.vcollaborate.emenu.view.IconChooser.imageWidth,
                    height: com.vcollaborate.emenu.view.IconChooser.imageHeight
                });
                
                image.addListener("click", function(e){
                    this.__styleSelectedImage(e.getTarget());
                }, this);
				
				image.addListener("dblclick", function(e){
                    this.__styleSelectedImage(e.getTarget());
					this.__selectIcon(e);
                }, this);
                
                this.__imageList.push(image);
            }, this);
        },
        
		/**
		 * Selects the icon, closes the window and fires an event.
		 * 
		 * @param e {Event} e
		 */
		__selectIcon: function(e) {
			 this.debug("__selectIcon");
			 
			 if (this.__currentSelectedImage) {
			 	this.fireDataEvent("iconSelected", this.__currentSelectedImage.getSource());
			 	this.close();
			 }
		}
    },
	
    /*
     *****************************************************************************
     EVENTS
     *****************************************************************************
     */
    events: {
        /**
         * Fired when the choosing is finished because of a selection.
         */
        "iconSelected": "qx.event.type.Data"
    },
	    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
        /**
         * Width of images
         */
        imageWidth: 26,
        
        /**
         * Height of images
         */
        imageHeight: 26
    }
});
