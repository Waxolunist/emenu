/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/status/dialog-password.png)
 ************************************************************************ */
/**
 * Choose an icon
 */
qx.Class.define("com.vcollaborate.emenu.view.RandomKeyGenerator", {
    extend: qx.ui.window.Window,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of IconChooser.
     */
    construct: function(){
        this.base(arguments, this.tr("Moove randomly around and press ok"), "icon/22/status/dialog-password.png");
        
        // set the properties of the window
        this.set({
            modal: true,
            showMinimize: false,
            showMaximize: false,
            allowMaximize: false,
            allowGrowX: false,
            allowGrowY: false,
            width: 330,
            resizableBottom: false,
            resizableTop: false,
            resizableRight: false,
            resizableLeft: false
        });
        
        this.__initWidgetList();
        this.__addContent();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __widgetList: null,
        __contentPane: null,
        __randomKeyPane: null,
        __randomCounter: 0,
        __okButton: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Adds the content of the window.
         *
         * @return {void}
         */
        __addContent: function(){
            this.debug("__addContent");
            
            var dock = new qx.ui.layout.Dock(5,5);
			this.setLayout(dock);
            
            this.__contentPane = new qx.ui.container.Composite(new qx.ui.layout.Grid(0,0)).set({
                allowGrowX: false,
                allowGrowY: false
            });
            
            this.__randomKeyPane = new qx.ui.container.Composite(new qx.ui.layout.Grid(0,0)).set({
                allowGrowX: true,
                allowGrowY: true,
                height: 50,
                backgroundColor: "light-background"
            });
            
            this.__addWidgets(this.__contentPane);
   			
			this.__okButton = new qx.ui.form.Button(this.tr("OK"));
			//TODO: For Productionuse set to false
			this.__okButton.setEnabled(false);
			this.__okButton.addListener("execute", this.__keyGenerated, this);
			
            this.add(this.__contentPane, {edge:"center"});
            this.add(this.__okButton, {edge:"south"});
            this.add(this.__randomKeyPane, {edge:"south"});
            this.add(new qx.ui.core.Widget().set({
            	height: 1,
            	backgroundColor: "black"
            }), {edge:"south"});
        },
        
        /**
         * Inits the listeners
         *
         * @return {void}
         */
        __initListeners: function(){
            this.debug("__initListeners");
            
            this.addListener("resize", this.__onPaneResize, this);
            
            this.addListener("keypress", function(e){
                var key = e.getKeyIdentifier();
                
                if (key == "Escape") {
                    this.close();
                }
                else if (key == "Right" || key == "Left" || key == "Up" || key == "Down") {
                    this.__moveSelection(e);
                } else if(key == "Enter") {
					this.__selectIcon(e);
				}
            }, this);
			
			this.addListener("close", this.__resetWindow, this);
        },
        
        /**
         * The actions on resize
         */
        __onPaneResize: function(e){
            this.debug("__onPaneResize");
            if (!this.isSeeable()) {
                this.center();
            }
            else if (this.getWidth()) {
                this.__contentPane.removeAll();
                this.__addWidgets(this.__contentPane);
            }
        },
        
		__resetWindow: function(e) {
			this.center();
			this.setWidth(330);
			this.resetHeight();
			this.__randomKeyPane.removeAll();
            this.__randomCounter = 0;
		},
		
        /**
         *
         *
         * @param contentPane {qx.ui.core.Widget} content
         */
        __addWidgets: function(contentPane){
            this.debug("__addWidgets");
            
            this.__widgetList.forEach(function(widget, index){
                contentPane.add(widget, {
                    row: Math.floor(index / com.vcollaborate.emenu.view.RandomKeyGenerator.WIDTH),
                    column: index % com.vcollaborate.emenu.view.RandomKeyGenerator.WIDTH
                });
            }, this);
        },
        
        /**
         * Inits the list of images
         */
        __initWidgetList: function(){
            this.debug("__initWidgetList");
            
            if (!this.__widgetList) {
                this.__widgetList = [];
            }
            
            var n = com.vcollaborate.emenu.view.RandomKeyGenerator.WIDTH*com.vcollaborate.emenu.view.RandomKeyGenerator.WIDTH;
            for(var i = 0; i < n; i++) {
            	var widget = new qx.ui.core.Widget().set({
            		width: 10,
            		height: 10,
        			backgroundColor: qx.util.ColorUtil.randomColor()
      			});
      			
      			widget.addListener("mouseover", function(e)
      			{
      				if(this.__randomCounter < com.vcollaborate.emenu.view.RandomKeyGenerator.MAXRANDOM) {
	      				var color = e.getTarget().getBackgroundColor();
						this.__randomKeyPane.add(new qx.ui.core.Widget().set({
	            			height: 10,
	            			width: 10,
	            			backgroundColor: color
	            		}), {
	                    	row: Math.floor(this.__randomCounter / com.vcollaborate.emenu.view.RandomKeyGenerator.WIDTH),
	                    	column: this.__randomCounter % com.vcollaborate.emenu.view.RandomKeyGenerator.WIDTH
	                	});
	                	this.__randomCounter++;
	                }
	                if(this.__randomCounter == com.vcollaborate.emenu.view.RandomKeyGenerator.MAXRANDOM) {
	                	this.__okButton.setEnabled(true);
	                }
	        	}, this);
        		
        		this.__widgetList.push(widget);
            }
        },
        
		/**
		 * Selects the icon, closes the window and fires an event.
		 * 
		 * @param e {Event} e
		 */
		__keyGenerated: function(e) {
			this.debug("__keyGenerated");
			 
			var tmpRandomString = "";
			this.__randomKeyPane.getChildren().forEach(function(widget, index){
				var color = widget.getBackgroundColor();
				var hexstring = qx.util.ColorUtil.rgbToHexString(qx.util.ColorUtil.stringToRgb(color));
				tmpRandomString = tmpRandomString + hexstring;
			}, this);
			var base16 = com.vcollaborate.emenu.utils.Base16.decode(tmpRandomString);
			var base64 = qx.util.Base64.encode(base16);
			this.setRandomString(base64);
			
			this.fireDataEvent("randomKeyGenerated", base64);
			this.debug("Randomstring set to: " + base64);
			this.close();
		}
    },
	
	/*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        randomString: {
            nullable: true,
            check: "String",
            init: "",
            event: "changeRandomString"
        }
	},
	
    /*
     *****************************************************************************
     EVENTS
     *****************************************************************************
     */
    events: {
        /**
         * Fired when the generation is finished.
         */
        "randomKeyGenerated": "qx.event.type.Data"
    },
    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
        WIDTH: 30,
        MAXRANDOM: 150
    },
    
     /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this.__commands = null;
        this._disposeObjects("__widgetList", "__randomCounter", "__contentPane", "__randomKeyPane", "__okButton");
    }
});
