/**
 * Status Dialog
 */
qx.Class.define("com.vcollaborate.emenu.view.NotificationBar", {
    extend: qx.ui.container.Composite,
    type: "singleton",
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of StatusDialog.
     *
     * @param message
     */
    construct: function(){
        this.base(arguments);
        
        this.setAppearance("notificationbar");
        
        this.__initLayout();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __counter: 0,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Inits the layout of this widget.
         *
         * @return {void}
         */
        __initLayout: function(){
            this.debug("__initLayout");
            var layout = new qx.ui.layout.VBox(5);
            layout.setReversed(true);
            this.setLayout(layout);
            //com.vcollaborate.emenu.utils.ViewUtils.setOnTop(this);
            this.setZIndex(-1);
        },
        
        /**
         * Inits the listeners of this widget.
         *
         * @return {void}
         */
        __initListeners: function(){
            this.debug("__initListeners");
        },
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        showMessage: function(message, icon, duration){
        	this.debug("showMessage");
        	var notification = new com.vcollaborate.emenu.view.NotificationBarMessage(message, icon, duration);
        	notification.addListenerOnce("hidden", function(e) {
        		if(--this.__counter == 0) {
        			this.setZIndex(-1);
        		}
        	}, this);
        	this.add(notification);
        	this.__counter++;
        	com.vcollaborate.emenu.utils.ViewUtils.setOnTop(this);
        }
    },
	
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
		this.debug("destruct");
		
        this._disposeObjects("__counter");
    }
});