/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/help-about.png)
 ************************************************************************ */
/**
 * The main tool bar widget
 */
qx.Class.define("com.vcollaborate.emenu.view.ToolBar", {
    extend: qx.ui.toolbar.ToolBar,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of toolbar.
     *
     * @param commands {var} the global commands object
     */
    construct: function(commands){
        this.base(arguments);
        
        this.__commands = commands;
        
        this.__init();
        
        qx.locale.Manager.getInstance().addListener("changeLocale", this.__setToolTips, this);
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __buttons: {},
		__parts: {},
        __commands: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Inits this toolbar
         *
         * @return {void}
         */
        __init: function(){
			this.debug("__init");
			
            this.__initLayout();
            this.__setToolTips();
        },
        
        /**
         * Inits the layout of this widget
         *
         * @return {void}
         */
        __initLayout: function(){
            this.debug("__initLayout");
			
			com.vcollaborate.emenu.utils.PartLoaderHelper.getActiveParts().forEach(function(item, index){
				this.__parts[item] = new qx.ui.toolbar.Part;
				this.addAt(this.__parts[item], index);
				this.__parts[item].hide();
			}, this);
			
            com.vcollaborate.emenu.utils.PartLoaderHelper.getActiveParts().forEach(function(item){
                com.vcollaborate.emenu.utils.PartLoaderHelper.require([item], function(){
                    var part = qx.Class.getByName(item + ".Part");
                    part.getInstance().initButtons(this.__commands, this.__buttons, this.__parts[item]);
                }, this);
            }, this);
            
            // Add a spacer
            this.addSpacer();
            
            // Info part
            var infoPart = new qx.ui.toolbar.Part;
            this.add(infoPart);
            
            this.__buttons.about = new qx.ui.toolbar.Button(this.tr("Help"), "icon/22/actions/help-about.png");
            this.__buttons.about.setCommand(this.__commands["about"]);
            infoPart.addAt(this.__buttons.about, 0);
        },
        
        /**
         * sets the tooltips of this toolbar
         *
         * @return {void}
         */
        __setToolTips: function(){
            this.debug("__setToolTips");
			
            this.__buttons.about.setToolTipText(this.__commands.about.toString());
            
            com.vcollaborate.emenu.utils.PartLoaderHelper.getActiveParts().forEach(function(item){
                com.vcollaborate.emenu.utils.PartLoaderHelper.require([item], function(){
                    var part = qx.Class.getByName(item + ".Part");
                    part.getInstance().setToolTips(this.__commands, this.__buttons);
                }, this);
            }, this);
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.__commands = null;
        this.__buttons = null;
		this.__parts = null;
        this._disposeObjects("__buttons", "__commands", "__parts");
    }
});
