/**
 * The Application's header
 */
qx.Class.define("com.vcollaborate.emenu.view.Header", {
    extend: qx.ui.container.Composite,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
	/**
	 * Creates a new instance of header.
	 */
    construct: function(){
        this.base(arguments);
        
        this.setLayout(new qx.ui.layout.HBox);
        this.setAppearance("app-header");
        
        var title = new qx.ui.basic.Label(this.tr("EMenu Administration"));
        var version = new qx.ui.basic.Label(this.tr("Version 1.0"));
        
        this.add(title);
        this.add(new qx.ui.core.Spacer, {
            flex: 1
        });
        this.add(version);
    }
});
