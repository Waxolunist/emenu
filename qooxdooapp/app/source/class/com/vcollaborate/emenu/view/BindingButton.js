/**
 * Choose an icon
 */
qx.Class.define("com.vcollaborate.emenu.view.BindingButton", {

    extend: qx.ui.form.Button,
    implement: [qx.ui.form.IStringForm, qx.ui.form.IForm],
    include: [qx.ui.form.MForm],
    
    construct: function(label){
        this.base(arguments, label, null, null);
    },
    
    members: {
        _applyValue: function(value){
            this.debug("_applyValue");
        },
        
        setPlaceholder: function(placeHolder){
            this.debug("setPlaceholder");
            
            this.setLabel(placeHolder);
        }
    },
    
    properties: {
        value: {
            init: null,
            nullable: true,
            check: "String",
            event: "changeValue",
            apply: "_applyValue"
        }
    }

});
