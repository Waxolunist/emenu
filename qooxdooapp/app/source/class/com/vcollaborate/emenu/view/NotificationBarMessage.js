/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/64/status/dialog-information.png)
 ************************************************************************ */
/**
 * Status Dialog
 */
qx.Class.define("com.vcollaborate.emenu.view.NotificationBarMessage", {
    extend: qx.ui.core.Widget,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of StatusDialog.
     *
     * @param message
     */
    construct: function(message, icon, duration){
        this.base(arguments);
        
        this.setAppearance("notificationbarmessage");
        
        this.set({
        	message: message,
        	duration: duration || 5000,
        	opacity: 0
        });
        
        switch(icon)
	    {
	    	case "info":
	    		this.setIcon("icon/64/status/dialog-information.png");
	    	break;
	    	default:
	    		this.setIcon(icon);
	    	break;
	    }
        
        this.__initLayout();
        this.__initListeners();
        this.addListenerOnce("appear", this.__prepareEffects, this);
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    	
    	__showEffect: null,
    	__hideEffect: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        
        __prepareEffects: function(){
        	this.debug("__prepareEffects");

  			var el = this.getContainerElement().getDomElement();
  			this.__showEffect = new qx.fx.effect.core.Fade(el).set({from: 0, to: 0.9, duration: 1.5 });
			this.__hideEffect = new qx.fx.effect.combination.Fold(this.getContainerElement().getDomElement()).set({
				mode: "in",
				duration: 2
			});

			this.__showEffect.addListener('finish',function(){
			    this.setOpacity(0.9);
			}, this);        	
        	this.__showEffect.start();
        	
        	this.__hideEffect.addListener('finish',function(){
			    this.fireDataEvent("hidden", {});
			}, this);        	
        },
        
        /**
         * Inits the layout of this widget.
         *
         * @return {void}
         */
        __initLayout: function(){
            this.debug("__initLayout");
            var layout = new qx.ui.layout.Dock();
            this._setLayout(layout);
            this._showChildControl("atom");
            if(this.getDuration() > -1) {
	            qx.event.Timer.once(function(){
					this.hideMessage();
	            }, this, this.getDuration());
            } else {
            	this.debug("Notification is permanent");
            }
        },
        
        /**
         * Inits the listeners of this widget.
         *
         * @return {void}
         */
        __initListeners: function(){
            this.debug("__initListeners");
        },
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */        
        hideMessage: function(duration){
        	this.debug("hideMessage");
			this.__hideEffect.start();
        },
        
        // overridden
	    _createChildControlImpl : function(id, hash)
	    {
	      var control;
	
	      switch(id)
	      {
	        case "atom":
	        	control = new qx.ui.basic.Atom(this.getMessage(), this.getIcon()).set({
	        		rich: true,
	        		opacity: 1
	        	});
      			this._add(control, {edge: "center"});
	          break;

	      }
	
	      return control || this.base(arguments, id);
	    }
    },
	
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        message: {
            init: null,
            nullable: true,
            check: "String",
            event: "changeMessage"
        },
        
        icon: {
        	init: null,
            nullable: true,
            check: "String",
            event: "changeMessage"
            
        },
        
        duration: {
        	init: null,
            nullable: false,
            check: "Integer",
            event: "changeDuration"
        }
    },
	
	/*
     *****************************************************************************
     EVENTS
     *****************************************************************************
     */
    events: {
        /**
         * Fired when the message is hidden.
         */
        "hidden": "qx.event.type.Data"
    },
	
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
		this.debug("destruct");
		
        this._disposeObjects("__showEffect", "__hideEffect");
    }
});