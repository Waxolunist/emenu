/**
 * A form to add entries of type {@link com.vcollaborate.emenu.time.ProjectTimeFormItem}
 */
qx.Class.define("com.vcollaborate.emenu.time.ProjectTimeForm", {
    extend: qx.ui.container.Scroll,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of part.
     */
    construct: function(){
        this.base(arguments);
        
        this.__initLayout();
        this.addTimeEntry();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __container: null,
        __titlebar: null,
        __timeEntries: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Inits the layout of this widget
         *
         * @return {void}
         */
        __initLayout: function(){
			//TODO set appearance
			this.debug("__initLayout");
			
            this.setBackgroundColor("light-background");
            
            this.__timeEntries = new Array();
            
            this.__container = new qx.ui.container.Composite(new qx.ui.layout.VBox(5, null, "separator-vertical")).set({
                allowGrowY: false
            });
            
            this.__titlebar = new qx.ui.container.Composite(new qx.ui.layout.Dock(5, 5));
            
            this.__titlebar.add(new qx.ui.basic.Label(this.tr("Costs")).set({
                allowGrowX: true,
                font: "bold"
            }), {
                edge: "west",
                width: "10%"
            });
            
            this.__titlebar.add(new qx.ui.basic.Label(this.tr("Project")).set({
                allowGrowX: true,
                font: "bold"
            }), {
                edge: "center",
                width: "40%"
            });
            
            this.__titlebar.add(new qx.ui.basic.Label(this.tr("Comment")).set({
                allowGrowX: true,
                font: "bold"
            }), {
                edge: "east",
                width: "50%"
            });
            
            this.__container.add(this.__titlebar);
            
            this.add(this.__container);
        },
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Adds a {@link com.vcollaborate.emenu.time.ProjectTimeFormItem} to this form.
         *
         * @param e {Event} event
         * @return {void}
         */
        addTimeEntry: function(e){
			this.debug("addTimeEntry");
			
            var projecttimeformitem = new com.vcollaborate.emenu.time.ProjectTimeFormItem(this);
            this.__timeEntries.push(projecttimeformitem);
            this.__container.add(projecttimeformitem);
        },
        
        /**
         * Removes the given {@link com.vcollaborate.emenu.time.ProjectTimeFormItem} from this form
         *
         * @param timeEntry {com.vcollaborate.emenu.time.ProjectTimeFormItem} the time entry to remove
         * @return {void}
         */
        removeTimeEntry: function(timeEntry){
			this.debug("removeTimeEntry");
			
            this.debug("removeTimeEntry");
            qx.lang.Array.remove(this.__timeEntries, timeEntry);
            this.__container.remove(timeEntry);
            
            com.vcollaborate.emenu.time.Part.getInstance().getAddTimeEntryCmd().setEnabled(true);
        },
        
        /**
         * the number of timeentries
         *
         * @return {Integer} the number of {@link com.vcollaborate.emenu.time.ProjectTimeFormItem}
         */
        getNumberOfTimeEntries: function(){
			this.debug("getNumberOfTimeEntries");
			
            return this.__timeEntries.length;
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
		
        this._disposeObjects("__container", "__titlebar", "__timeEntries");
    }
});
