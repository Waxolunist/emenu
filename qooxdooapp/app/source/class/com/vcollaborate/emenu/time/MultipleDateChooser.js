/**
 * A multiple date chooser showing as many datepickers as chosen.
 */
qx.Class.define("com.vcollaborate.emenu.time.MultipleDateChooser", {
    extend: qx.ui.container.Scroll,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of ProgressiveTestData.
     *
     * @param numberOfDateChoosers {Integer} the number of {@link qx.ui.control.DateChooser}
     */
    construct: function(numberOfDateChoosers){
        this.base(arguments);
        
        this.set({
            minWidth: 200,
            maxWidth: 300
        });
        
        this.__dateChoosers = new Array(numberOfDateChoosers || 3);
        
        this.__initLayout();
        this.__setShownMonth();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __parentWidget: null,
        __dateChoosers: null,
        __changeShownMonthFired: null,
        __setShownMonthExecute: true,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Inits the layout of this widget
         *
         * @return {void}
         */
        __initLayout: function(){
            this.debug("__initLayout");
            
            this.__parentWidget = new qx.ui.core.Widget();
            this.__parentWidget._setLayout(new qx.ui.layout.VBox());
            
            for (var i = 0; i < this.__dateChoosers.length; i++) {
                this.__dateChoosers[i] = new qx.ui.control.DateChooser();
                this.__parentWidget._add(this.__dateChoosers[i]);
                
                if (i > 0) {
                    this.__dateChoosers[i].getChildControl("last-month-button").hide();
                    this.__dateChoosers[i].getChildControl("last-year-button").hide();
                    this.__dateChoosers[i].getChildControl("next-month-button").hide();
                    this.__dateChoosers[i].getChildControl("next-year-button").hide();
                }
            }
            
            this.add(this.__parentWidget);
        },
        
        /**
         * Inits the listeners
         *
         * @return {void}
         */
        __initListeners: function(){
            this.debug("__initListeners");
            
            for (var i = 0; i < this.__dateChoosers.length; i++) {
                this.__dateChoosers[i].addListener("changeValue", this.__changeValue, this);
            }
            
            this.__dateChoosers[0].addListener("changeShownMonth", this.__changeShownMonth, this);
            this.__dateChoosers[0].addListener("changeShownYear", this.__changeShownMonth, this);
        },
        
        /**
         * Sets the shown month of each datepicker
         *
         * @param arrayPosition {Integer} the position of the {@link qx.ui.control.DateChooser}
         * @return {void}
         */
        __setShownMonth: function(arrayPosition){
            this.debug("__setShownMonth");
            
            var index = arrayPosition || 0;
            var shownMonth = this.__dateChoosers[index].getShownMonth() || new Date().getMonth();
            var shownYear = this.__dateChoosers[index].getShownYear() || new Date().getFullYear();
            
            //increment
            var helperDate = new Date(shownYear, shownMonth, 1);
            
            for (var i = index + 1; i < this.__dateChoosers.length; i++) {
                helperDate = com.vcollaborate.emenu.time.MultipleDateChooser.setNextMonth(helperDate);
                this.__dateChoosers[i].showMonth(helperDate.getMonth(), helperDate.getFullYear());
            }
            
            //decrement
            helperDate = new Date(shownYear, shownMonth, 1);
            
            for (var i = index - 1; i >= 0; i--) {
                helperDate = com.vcollaborate.emenu.time.MultipleDateChooser.setPreviousMonth(helperDate);
                this.__dateChoosers[i].showMonth(helperDate.getMonth(), helperDate.getFullYear());
            }
        },
        
        /**
         * Changes the shown month
         *
         * @param e {Event} Event
         * @return {void}
         */
        __changeShownMonth: function(e){
            this.debug("__changeShownMonth");
            
            var arrayPosition = this.__getArrayPosition(e.getTarget());
            
            if (this.__setShownMonthExecute) {
                this.__setShownMonthExecute = false;
                this.__setShownMonth(arrayPosition);
            }
            
            this.__setShownMonthExecute = true;
        },
        
        /**
         * Changes the value
         *
         * @param e {Event} Event
         * @return {void}
         */
        __changeValue: function(e){
            this.debug("__changeValue");
            
            if (e.getOldData() != e.getData()) {
                this.setValue(e.getData());
                
                var arrayPosition = this.__getArrayPosition(e.getTarget());
                
                //set all others null
                for (var i = 0; i < this.__dateChoosers.length; i++) {
                    if (i != arrayPosition) {
                        this.__dateChoosers[i].setValue(null);
                    }
                }
                
                this.__dateChoosers[arrayPosition].setValue(e.getData());
                
                if (e.getOldData() != null && e.getData() != null) {
                    if (e.getOldData().getMonth() != e.getData().getMonth() || e.getOldData().getFullYear() != e.getData().getFullYear()) {
                        if ((arrayPosition == 0 && e.getOldData() < e.getData()) || (arrayPosition > 0 && arrayPosition < this.__dateChoosers.length - 1) || (arrayPosition == this.__dateChoosers.length - 1 && e.getOldData() > e.getData())) {
                            this.__dateChoosers[arrayPosition].setValue(null);
                            this.__dateChoosers[arrayPosition].showMonth(e.getOldData().getMonth(), e.getOldData().getFullYear());
                            
                            if (e.getOldData() < e.getData()) {
                                this.__dateChoosers[arrayPosition + 1].setValue(e.getData());
                                this.__dateChoosers[arrayPosition + 1].activate();
                            }
                            else {
                                this.__dateChoosers[arrayPosition - 1].setValue(e.getData());
                                this.__dateChoosers[arrayPosition - 1].activate();
                            }
                        }
                        else 
                            if (arrayPosition == this.__dateChoosers.length - 1 && e.getOldData() < e.getData()) {
                                this.__setShownMonth(arrayPosition);
                            }
                    }
                }
            }
        },
        
        /**
         * Gets the index of the datechooser
         *
         * @param dateChooser {qx.ui.control.DateChooser} the datechooser to get the position
         * @return {Integer | null} the position of the datechooser
         */
        __getArrayPosition: function(dateChooser){
            this.debug("__getArrayPosition");
            
            for (var i = 0; i < this.__dateChoosers.length; i++) {
                if (this.__dateChoosers[i] == dateChooser) {
                    return i;
                }
            }
            
            return null;
        }
    },
    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
        /**
         * Use the date object created on first load of the page and increase the month number part by one. Modify year as well if month number becomes more than 11 (december).
         *
         * @param date {Date} the date to increment by one
         * @return {Date} the date plus one month
         */
        setNextMonth: function(date){
            var _date = date;
            var nextMonth = _date.getMonth() + 1;
            
            if (nextMonth > 11) {
                _date.setFullYear(_date.getFullYear() + 1, 0, _date.getDate());
            }
            else {
                _date.setFullYear(_date.getFullYear(), nextMonth, _date.getDate());
            }
            
            return _date;
        },
        
        /**
         * Use the date object created on first load of the page and decrease the month number part by one. Modify year as well if month number becomes less than 1 (january).
         *
         * @param date {Date} the date to decrement by one
         * @return {Date} the date minus one month
         */
        setPreviousMonth: function(date){
            var _date = date;
            var nextMonth = _date.getMonth() - 1;
            
            if (nextMonth < 0) {
                _date.setFullYear(_date.getFullYear() - 1, 11, _date.getDate());
            }
            else {
                _date.setFullYear(_date.getFullYear(), nextMonth, _date.getDate());
            }
            
            return _date;
        }
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * The current selected value or null if nothing is selected.
         */
        value: {
            check: "Date",
            event: "changeValue",
            init: null,
            nullable: true
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this.__dateChoosers = null;
        this.__changeShownMonthFired = null;
        this._disposeObjects("__parentWidget", "__changeShownMonthFired", "__dateChoosers", "__setShownMonthExecute");
    }
});
