/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/16/actions/list-remove.png)
 ************************************************************************ */
/**
 *  A widget to add a time entry
 */
qx.Class.define("com.vcollaborate.emenu.time.ProjectTimeFormItem", {
    extend: qx.ui.container.Composite,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of project time form item.
     * 
     * @param controllerForm {com.vcollaborate.emenu.time.ProjectTimeForm} the parent of this widget
     */
    construct: function(controllerForm){
        this.base(arguments);
        
        this.__controllerForm = controllerForm;
        
        this.__initLayout();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __timeSpinner: null,
        __projectSelectBox: null,
        __commentTextfield: null,
        __removeBtn: null,
        __controllerForm: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Inits the layout of this widget
         *
         * @return {void}
         */
        __initLayout: function(){
			this.debug("__initLayout");
			
            this._setLayout(new qx.ui.layout.Dock(5, 5));
            
            this.__timeSpinner = new qx.ui.form.Spinner(0, 0, 1).set({
                height: 28
            });
            this.__timeSpinner.setSingleStep(0.1);
            
            var nf = new qx.util.format.NumberFormat();
            nf.setMinimumFractionDigits(1);
            nf.setMaximumFractionDigits(1);
            this.__timeSpinner.setNumberFormat(nf);
            
            this.__projectSelectBox = new qx.ui.form.SelectBox().set({
                height: 28
            });
            
            this.__commentTextfield = new qx.ui.form.TextField().set({
                height: 28
            });
            
            this.__removeBtn = new qx.ui.form.Button(null, "icon/16/actions/list-remove.png").set({
                allowGrowY: false,
                height: 28
            });
            
            this.add(this.__timeSpinner, {
                edge: "west",
                width: "10%"
            });
            
            this.add(this.__projectSelectBox, {
                edge: "center",
                width: "40%"
            });
            
            this.add(this.__removeBtn, {
                edge: "east",
                width: "3%"
            });
            
            this.add(this.__commentTextfield, {
                edge: "east",
                width: "47%"
            });
        },
        
        /**
         * Inits the listeners of this widget
         *
         * @return {void}
         */
        __initListeners: function(){
			this.debug("__initListeners");
			
            this.__removeBtn.addListener("execute", this.removeTimeEntry, this);
        },
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * removes itself from its parent
         *
         * @param e {Event} Event
         * @return {void}
         */
        removeTimeEntry: function(e){
			this.debug("removeTimeEntry");
			
            this.__controllerForm.removeTimeEntry(this);
        }
    },
    
	/*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
		
        this._disposeObjects("__timeSpinner", "__projectSelectBox", "__commentTextfield", "__removeBtn", "__controllerForm");
    }
});
