/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/document-save.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/list-add.png)
 #asset(com/vcollaborate/emenu/images/xls.png)
 ************************************************************************ */
/**
 * This part adds the possibility to create time data.
 */
qx.Class.define("com.vcollaborate.emenu.time.Part", {
    extend: com.vcollaborate.emenu.utils.AbstractPart,
    type: "singleton",
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of part.
     */
    construct: function(){
        this.debug("construct");
        
        this.base(arguments);
        
        if (!this.__projectTimeForm) {
            this.__projectTimeForm = new com.vcollaborate.emenu.time.ProjectTimeForm();
        }
        
        if (!this.__dateChooser) {
            this.__dateChooser = new com.vcollaborate.emenu.time.MultipleDateChooser();
        }
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __projectTimeForm: null,
        __dateChooser: null,
        __addTimeEntryCmd: null,
        
        /*
         *****************************************************************************
         DERIVED
         *****************************************************************************
         */
        /**
         * Inits following three commands:
         *
         * <ul>
         * <li>{@link #getAddTimeEntryCmd}</li>
         * <li>saveTimeEntryCmd</li>
         * <li>exportTimeXLSCmd</li>
         * </ul>
         *
         * @param commands {var} the global commands object
         * @return {void}
         */
        initCommands: function(commands){
            this.base(arguments);
            commands.addTimeEntry = new qx.ui.core.Command("Control+Enter");
            commands.addTimeEntry.addListener("execute", this.addTimeEntry, this);
            this.__addTimeEntryCmd = commands.addTimeEntry;
            
            commands.saveTimeEntry = new qx.ui.core.Command("Control+S");
            commands.saveTimeEntry.addListener("execute", this.saveTimeEntry, this);
			commands.saveTimeEntry.setEnabled(false);
            
            commands.exportTimeXLS = new qx.ui.core.Command("Control+E");
            commands.exportTimeXLS.addListener("execute", this.exportTimeXLS, this);
        },
        
        /**
         * Inits the buttons for the toolbar, one for each command.
         *
         * @see #initCommands
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @param part {qx.ui.toolbar.Part} the part to add the buttons to
         * @return {void}
         */
        initButtons: function(commands, buttons, part){
            this.base(arguments);
            buttons.saveTimeEntry = new qx.ui.toolbar.Button(this.tr("Save Timesheet"), "icon/22/actions/document-save.png");
            buttons.saveTimeEntry.setCommand(commands.saveTimeEntry);
            part.addAt(buttons.saveTimeEntry, 0);
            
            buttons.addTimeEntry = new qx.ui.toolbar.Button(this.tr("Add Timeentry"), "icon/22/actions/list-add.png");
            buttons.addTimeEntry.setCommand(commands.addTimeEntry);
            part.addAt(buttons.addTimeEntry, 1);
            
            buttons.exportTimeXLS = new qx.ui.toolbar.Button(this.tr("Export to XLS"), "com/vcollaborate/emenu/images/xls.png");
            buttons.exportTimeXLS.setCommand(commands.exportTimeXLS);
            part.addAt(buttons.exportTimeXLS, 2);
			
			part.show();
        },
        
        /**
         * Sets the tooltips for each button of this part.
         *
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @return {void}
         */
        setToolTips: function(commands, buttons){
            this.base(arguments);
			
            buttons.saveTimeEntry.setToolTipText(commands.saveTimeEntry.toString());
            buttons.addTimeEntry.setToolTipText(commands.addTimeEntry.toString());
            buttons.exportTimeXLS.setToolTipText(commands.exportTimeXLS.toString());
        },
		
		/**
         * Returns a multiple date chooser
         *
         * @return {com.vcollaborate.emenu.time.MultipleDateChooser} multiple date chooser
         */
        getLeftSidePane: function(){
            this.base(arguments);
            
			return this.__dateChooser;
        },
        
		/**
         * Returns a entry field to enter time entries.
         *
         * @return {com.vcollaborate.emenu.time.ProjectTimeForm} a project time form
         */
        getRightSidePane: function(){
            this.base(arguments);
			
            return this.__projectTimeForm;
        },
		
		/*
        *****************************************************************************
        PUBLIC
        *****************************************************************************
        */
        /**
         * Adds a time entry to the project time form.
         *
         * @param e {Event} Event
         * @return {void}
         */
        addTimeEntry: function(e){
            this.debug("addTimeEntry");
			
            this.__projectTimeForm.addTimeEntry();
            
            if (this.__projectTimeForm.getNumberOfTimeEntries() == 10) {
                e.getTarget().setEnabled(false);
            }
        },
        
        /**
         * Not implemented now.
         *
         * @return {void}
         */
        saveTimeEntry: function(){
            this.debug("saveTimeEntry");
			//TODO
        },
        
        /**
         * Not implemented now.
         *
         * @return {void}
         */
        exportTimeXLS: function(){
            this.debug("exportTimeXLS");
            //TODO
        },
        
        /**
         * Returns the command __addTimeEntry__.
         * The command has the shortcut *Control+Enter* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getAddTimeEntryCmd: function(){
            return this.__addTimeEntryCmd;
        }
    },
	
	/*
    *****************************************************************************
    DESTRUCTOR
    *****************************************************************************
    */
    destruct: function(){
        this.debug("destruct");
        
        this._disposeObjects("__projectTimeForm", "__dateChooser", "__addTimeEntryCmd");
    }
});
