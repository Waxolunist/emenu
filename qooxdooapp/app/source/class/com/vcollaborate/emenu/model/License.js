/**
 * Data model for a category
 */
qx.Class.define("com.vcollaborate.emenu.model.License", {
    extend: qx.core.Object,
    include: [com.vcollaborate.emenu.io.CouchDBODM],
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of category.
     *
     * @param _id {String} _id
     * @param title {String} title
     * @param parentcategory {com.vcollaborate.emenu.model.Category} parentcategory
     * @param order {Integer} order
     * @param icon {String?null} iconPath
     */
    construct: function(parts, number, expirationDate, email){
        this.base(arguments);
        
        this.set_id(com.vcollaborate.emenu.model.License.LICENSEID);
        
        this.set({
            parts: parts || [],
            number: number || "",
            expirationDate: expirationDate || null,
            email: email || null
        });
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        __applyExpirationDate : function (value, old) {
        	this.debug("__applyExpirationDate: " + value);
        }
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
    },
    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
    	LICENSEID: "license",
    	
    	createTestLicense: function(email) {
    		var expiryDate = new Date();
			expiryDate.setDate(expiryDate.getDate()+30);
    		var license = new com.vcollaborate.emenu.model.License(
    			[
					"com.vcollaborate.emenu.categories"
					//,"com.vcollaborate.emenu.articles"
					,"com.vcollaborate.emenu.passwords"
					//,"com.vcollaborate.emenu.time"
					,"com.vcollaborate.emenu.pref"
					,"com.vcollaborate.emenu.test"
				],
				"0", expiryDate, email || "test@v-collaborate.com"
    		);
    		
    		return license;
    	},
    	
    	calculateLicenseKey: function(license) {
    		if (license instanceof com.vcollaborate.emenu.model.License) {
    			return com.vcollaborate.emenu.utils.Crypt.getInstance().encrypt(qx.util.Base64.encode(license.toJson()));
    		}
    		throw new com.vcollaborate.emenu.model.LicenseError(qx.locale.Manager.tr("Not a valid license key!"));
    	},
    	
    	getLicenseObject: function(licensekey, email) {
    		if(licensekey == "test" || !licensekey) {
    			return com.vcollaborate.emenu.model.License.createTestLicense(email);
    		}
    		var license = com.vcollaborate.emenu.io.CouchDBODM.jsonToModel(qx.util.Base64.decode(com.vcollaborate.emenu.utils.Crypt.getInstance().decrypt(licensekey)), 
    																com.vcollaborate.emenu.model.License.delegate);
    		return license;
    		//var json = new qx.data.marshal.Json(com.vcollaborate.emenu.model.License.delegate); 
    		//return json.toModel(qx.lang.Json.parse(qx.util.Base64.decode(licensekey), com.vcollaborate.emenu.io.CouchDBODM.toModelReviver));
    	},
    	
    	delegate: {
			getModelClass: function(properties) {
         		return com.vcollaborate.emenu.model.License;
        	}
      	}
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * Email
         */
        parts: {
            check: "Array",
            init: null,
            event: "changeParts",
            nullable: false
        },
        
        /**
         * Key
         */
        number: {
            check: "String",
            init: null,
            event: "changeNumber",
            nullable: false
        },
        
        /**
         * ExpirationDate
         * null means never
         */
        expirationDate: {
            check: "Date",
            init: null,
            event: "changeExpirationDate",
            nullable: true,
            apply: "__applyExpirationDate"
        },
        
        email: {
            check: "String",
            init: null,
            event: "changeEmail",
            nullable: true
        },
        
        /**
         * The type. Always *article*.
         */
        type: {
            check: "String",
            init: "license"
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
    }
    
});
