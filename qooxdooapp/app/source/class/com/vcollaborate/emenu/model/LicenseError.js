/**
 * A license Error 
 */
qx.Class.define("com.vcollaborate.emenu.model.LicenseError",
{
    extend : qx.type.BaseError
});