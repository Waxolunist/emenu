/**
 * Data model for a category
 */
qx.Class.define("com.vcollaborate.emenu.model.User", {
    extend: qx.core.Object,
    include: [com.vcollaborate.emenu.io.CouchDBODM],
    implement: [com.vcollaborate.emenu.io.IUser],
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of category.
     *
     * @param _id {String} _id
     * @param title {String} title
     * @param parentcategory {com.vcollaborate.emenu.model.Category} parentcategory
     * @param order {Integer} order
     * @param icon {String?null} iconPath
     */
    construct: function(_id, name, password, email, key, admin){
        this.base(arguments);
        
        this.set_id(_id);
        
        this.set({
            name: name,
            password: password,
            email: email,
            key: key,
            admin: admin
        });

        this.addListenerOnce("changeID", this.__onChangeID, this);
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    	
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
    	isAdmin : function() {
    		return this.getAdmin();
    	}
    },
    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * name
         */
        name: {
            check: "String",
            init: null,
            event: "changeName",
            nullable: false,
            persist: true
        },
        
        password: {
            check: "String",
            init: null,
            event: "changePassword",
            nullable: false,
            persist: true
        },
        
        /**
         * Email
         */
        email: {
            check: "String",
            init: null,
            event: "changeEmail",
            nullable: false,
            persist: true,
            validate: qx.util.Validate.email() 
        },
        
        /**
         * Key
         */
        key: {
            check: "String",
            init: null,
            event: "changeKey",
            nullable: false,
            persist: true
        },
                
        /**
         * The type. Always *user*.
         */
        type: {
            check: "String",
            init: "user"
        },
        
        admin: {
        	check: "Boolean",
        	init: false,
            event: "changeAdmin",
            nullable: false,
            persist: true
        }
    },
    
    events : {
 		/**
 		* fires when a script is loaded
		*/
	 	admincreated: 'qx.event.type.Event'
	},
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
    }
    
});
