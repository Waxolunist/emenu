/**
 * This is the model of the article used by the package {@link com.vcollaborate.emenu.articles}.
 */
qx.Class.define("com.vcollaborate.emenu.model.Article", {
    extend: qx.core.Object,
    include: [com.vcollaborate.emenu.io.CouchDBODM, com.vcollaborate.emenu.io.TableData],
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of article.
     * 
     * @param _id {String?null} _id
     * @param title {String?null} title
     * @param category {com.vcollaborate.emenu.model.Category?null} category
     * @param content {String?""} content
     * @param order {Integer?0} order
     */
    construct: function(_id, title, category, content, order){
        this.base(arguments);
        
        this.set_id(_id || null);
        
        this.set({
            title: title || null,
            order: order || 0,
            category: category || null,
            content: content || "",
            categoryID: category ? category.get_id() : null
        });
        
        this.addListenerOnce("changeID", this.__onChangeID, this);
    },
	
	
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * The parentcategory where it belongs to. It does not persist because of cyclic redundancy.
         */
        category: {
            nullable: true,
            check: "com.vcollaborate.emenu.model.Category",
            init: null,
            event: "changeCategory",
			apply: "__applyCategory",
            persist: false
        },
        
        /**
         * The ID of the parentcategory. This property is just set to avoid endless loops while serializing.
         */
        categoryID: {
            nullable: true,
            check: "String",
            init: null,
            event: "changeCategoryID"
        },
        
        /**
         * The title of this article.
         */
        title: {
            init: null,
            nullable: true,
            check: "String",
            event: "changeTitle"
        },
        
        /**
         * The content of this article.
         */
        content: {
            init: "",
            nullable: false,
            check: "String",
            event: "changeContent"
        },
        
        /**
         * The order number of this article.
         */
        order: {
            check: "Integer",
            init: 0,
            event: "changeOrder",
            nullable: true
        },
        
        /**
         * The type. Always *article*.
         */
        type: {
            check: "String",
            init: "article"
        }
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Called after saving.
         *
         * @param e {Event} The event object
         * @return {void}
         */
        __onChangeID: function(e){
            this.debug("__onChangeID");
            
            e.stopPropagation();
            
            if (e.getOldData() != e.getData()) {
            }
        },
		
		/**
		 * Apply the category.
		 * 
		 * @param value {com.vcollaborate.emenu.model.Category} value
		 */
		__applyCategory: function(value, old) {
			this.debug("__applyCategory");
			
			if (old) {
				old.getArticles().remove(this);
				this.setCategoryID(null);
			}
			
			if (value) {
				value.getArticles().push(this);
				this.setCategoryID(value.get_id());
			}
		}
    },
    
	statics: {
		delegate: {
			getModelClass: function(properties) {
         		return com.vcollaborate.emenu.model.Article;
        	}
      	}
	},
	
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
    }
});
