/**
 * Data model for a category
 */
qx.Class.define("com.vcollaborate.emenu.model.Category", {
    extend: qx.core.Object,
    include: [com.vcollaborate.emenu.io.CouchDBODM],
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of category.
     *
     * @param _id {String} _id
     * @param title {String} title
     * @param parentcategory {com.vcollaborate.emenu.model.Category} parentcategory
     * @param order {Integer} order
     * @param icon {String?null} iconPath
     */
    construct: function(_id, title, parentcategory, order, icon){
        this.base(arguments);
        
        this.setPath(new qx.data.Array());
        
        var tmpParentcategory = parentcategory;
        
        while (tmpParentcategory != null) {
            this.getPath().insertAt(0, tmpParentcategory.get_id());
            tmpParentcategory = tmpParentcategory.getParentcategory();
        }
        
        this.getPath().push(_id);
        this.set_id(_id);
        
        this.set({
            title: title,
            order: order,
            parentcategory: parentcategory
        });
        
		if (icon != null) {
      		this.setIcon(icon);
    	}
		
        this.setCategories(new qx.data.Array());
        this.setArticles(new qx.data.Array());
        
        if (parentcategory) {
            parentcategory.getCategories().push(this);
        }
        
        this.addListenerOnce("changeID", this.__onChangeID, this);
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Removes all child categories and it self. If rootcategory is null, it takes this as parameter.
         *
         * @param rootcategory {com.vcollaborate.emenu.model.Category} the category to go from to delete all childs
         * @return {void}
         */
        __removeRecursive: function(rootcategory){
            this.debug("__removeRecursive");
            
            if (rootcategory) {
                this.debug("removeRecursive " + rootcategory.getTitle());
                
                rootcategory.getCategories().forEach(function(category){
                    category.__removeRecursive(category);
                });
                
                rootcategory.remove();
                
                //TODO as long as there is only a one to one relationship between category and article
                rootcategory.getArticles().forEach(function(article){
                    article.remove();
                });
            }
            else {
                this.__removeRecursive(this);
            }
        },
        
        /**
         * Called after saving. It sets the order and the path new.
         *
         * @param e {Event} The event object
         */
        __onChangeID: function(e){
            this.debug("__onChangeID");
            e.stopPropagation();
            
            if (e.getOldData() != e.getData()) {
                //this only happens after save
                this.getPath().setItem(this.getPath().getLength() - 1, this.get_id());
                
                if (this.getOrder() == undefined || this.getOrder() == null) {
                    var oThis = this;
                    
                    this.couchdb().queryView("app/maxorder", {
                        key: oThis.getParentcategory().get_id(),
                        group: true
                    }, function(resp){
                        var order = resp.rows[0].value + 1;
                        oThis.setOrder(order);
                        oThis.save();
                    });
                }
                else {
                    //if order is already set
                    this.save();
                }
            }
        },
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Removes this from the parentcategory and all child categories.
         *
         * @return {void}
         */
        removeRecursive: function(){
            this.debug("removeRecursive");
            
            this.getParentcategory().getCategories().remove(this);
            this.__removeRecursive();
        },
        
        /**
         * Loads all sub categories and articles of this category.
         */
        loadRecursive: function(){
            this.debug("loadRecursive");
            
            var oThis = this;
            
            this.couchdb().queryView("app/descendants", {
                startkey: [oThis.get_id()],
                endkey: [oThis.get_id(), {}]
            }, function(resp){
                for (var i in resp.rows) {
                    var doc = resp.rows[i].value;
                    
                    var category = new com.vcollaborate.emenu.model.Category(doc._id, doc.title, oThis, doc.order, doc.icon);
                    category.set_rev(doc._rev);
                    category.loadArticles();
                    category.loadRecursive();
                }
            }, function(){
                oThis.debug("Error while querying view descendants.");
            });
        },
        
        /**
         * Loads all articles of this category.
         */
        loadArticles: function(){
            var oThis = this;
            
			var query = "app/articles";
			var json = new qx.data.marshal.Json(com.vcollaborate.emenu.model.Article.delegate); 
			
			if (com.vcollaborate.emenu.utils.PartLoaderHelper.isPartActive("com.vcollaborate.emenu.passwords")) {
				query = "app/passwords";
				json = new qx.data.marshal.Json(com.vcollaborate.emenu.model.Password.delegate);
			}
			
            this.couchdb().queryView(query, {
                key: oThis.get_id()
            }, function(resp){
                for (var i in resp.rows) {
                    var doc = resp.rows[i].value;
					var object = json.toModel(doc);
					object.setCategory(oThis);
                }
            }, function(){
                oThis.debug("Error while querying view articles.");
            });
        }
    },
    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
        /**
         * The root of all categories.
         */
        root: null,
        
        /**
         * Returns the root of all categories.
         *
         * @return {com.vcollaborate.emenu.model.Category} the root of all categories.
         */
        getRoot: function(){
            if (!com.vcollaborate.emenu.model.Category.root) {
                var couchdb = com.vcollaborate.emenu.io.CouchDB.getInstance();
                
                couchdb.openDocument("root", function(resp){
                    //onsuccess
                    com.vcollaborate.emenu.model.Category.root = new com.vcollaborate.emenu.model.Category(resp._id, resp.title, null, resp.order);
                    com.vcollaborate.emenu.model.Category.root.set_rev(resp._rev);
                }, function(){
                    //onerror
                    com.vcollaborate.emenu.model.Category.root = new com.vcollaborate.emenu.model.Category("root", "", null, 0);
                    com.vcollaborate.emenu.model.Category.root.save();
                }, {
                    async: false
                });
                
                if (!com.vcollaborate.emenu.io.CouchDB.hasConnection()) {
                    com.vcollaborate.emenu.model.Category.root = new com.vcollaborate.emenu.model.Category("root", "", null, 0);
                }
            }
            
            return com.vcollaborate.emenu.model.Category.root;
        }
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * All subcategories.
         */
        categories: {
            check: "qx.data.Array",
            event: "changeCategories",
            persist: false
        },
        
        /**
         * All articles of this.
         */
        articles: {
            check: "qx.data.Array",
            event: "changeArticles",
            persist: false
        },
        
        /**
         * The parentcategory or null if no parent exists.
         */
        parentcategory: {
            check: "com.vcollaborate.emenu.model.Category",
            init: null,
            event: "changeParentCategory",
            nullable: true,
            persist: false
        },
        
        /**
         * The name of this category.
         */
        title: {
            check: "String",
            event: "changeTitle"
        },
        
        /**
         * The path to this category.
         */
        path: {
            check: "qx.data.Array",
            event: "changePath"
        },
		
		/**
		 * The path of the icon
		 */
		icon: {
			check: "String",
            event: "changeIcon",
			persist: true,
			nullable: false,
			init: "com/vcollaborate/emenu/images/nuvola/C48_Folder.png"
		},
        
        /**
         * The current order in the current level of this parent.
         */
        order: {
            check: "Integer",
            init: 0,
            event: "changeOrder",
            nullable: true
        },
        
        /**
         * The type. Always category.
         */
        type: {
            check: "String",
            init: "category"
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
    }
    
});
