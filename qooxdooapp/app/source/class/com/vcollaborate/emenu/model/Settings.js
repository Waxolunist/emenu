/**
 * This is the model of the settings object.
 */
qx.Class.define("com.vcollaborate.emenu.model.Settings", {
    extend: qx.core.Object,
    include: [com.vcollaborate.emenu.io.CouchDBODM],
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of settings.
     * 
     * @param _id {String?null} _id
     */
    construct: function(_id){
        this.base(arguments);
        
        this.set_id(_id || null);
        
        this.addListenerOnce("changeID", this.__onChangeID, this);
    },
	
	
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        
        language: {
        	nullable: true,
        	check: "String",
        	init: null,
        	event: "changeLanguage"
        }
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Called after saving.
         *
         * @param e {Event} The event object
         * @return {void}
         */
        __onChangeID: function(e){
            this.debug("__onChangeID");
            
            e.stopPropagation();
            
            if (e.getOldData() != e.getData()) {
            }
        }
    },
    
	statics: {
		delegate: {
			getModelClass: function(properties) {
         		return com.vcollaborate.emenu.model.Settings;
        	}
      	}
	},
	
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
    }
});
