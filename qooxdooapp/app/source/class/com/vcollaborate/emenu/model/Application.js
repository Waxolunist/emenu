/**
 * Data model for a category
 */
qx.Class.define("com.vcollaborate.emenu.model.Application", {
    extend: qx.core.Object,
    include: [com.vcollaborate.emenu.io.CouchDBODM],
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of category.
     *
     * @param _id {String} _id
     * @param title {String} title
     * @param parentcategory {com.vcollaborate.emenu.model.Category} parentcategory
     * @param order {Integer} order
     * @param icon {String?null} iconPath
     */
    construct: function(licensekey, status){
        this.base(arguments);
        
        this.set_id(com.vcollaborate.emenu.model.Application.APPID);
        
        this.set({
            licensekey: licensekey,
            status: status
        });
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        __license: null,
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        getLicenseObject: function() {
        	if(!this.__license) {
        		this.__license = com.vcollaborate.emenu.model.License.getLicenseObject(this.getLicensekey());
        	}
        	return this.__license;
        }
    },
    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
    	
    	APPID: "application",
    	
    	/**
         * The root of all categories.
         */
        app: null,
        
        /**
         * Returns the root of all categories.
         *
         * @return {com.vcollaborate.emenu.model.Category} the root of all categories.
         */
        getApplicationObject: function(){
            if (!com.vcollaborate.emenu.model.Application.app) {
            	if(com.vcollaborate.emenu.io.CouchDB.hasConnection()) {
	                var couchdb = com.vcollaborate.emenu.io.CouchDB.getInstance();
	                
	                couchdb.openDocument(com.vcollaborate.emenu.model.Application.APPID, function(resp){
	                    //onsuccess
	                    com.vcollaborate.emenu.model.Application.app = new com.vcollaborate.emenu.model.Application(resp.licensekey, resp.status);
	                    com.vcollaborate.emenu.model.Application.app.set_rev(resp._rev);
	                }, function(){
	                    //onerror
	                	var licensekey = com.vcollaborate.emenu.model.License.calculateLicenseKey(com.vcollaborate.emenu.model.License.createTestLicense());
	                    com.vcollaborate.emenu.model.Application.app = new com.vcollaborate.emenu.model.Application(licensekey, "newparent");
	                    com.vcollaborate.emenu.model.Application.app.save();
	                }, {
	                    async: false
	                });
                } else {
                	var licensekey = com.vcollaborate.emenu.model.License.calculateLicenseKey(com.vcollaborate.emenu.model.License.createTestLicense());
                    com.vcollaborate.emenu.model.Application.app = new com.vcollaborate.emenu.model.Application(licensekey, "newparent");
                }
            }
            
            return com.vcollaborate.emenu.model.Application.app;
        }
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * Email
         */
        status: {
            check: [ "new", "parent", "installed", "newparent" ],
            init: "newparent",
            event: "changeEmail",
            nullable: false,
            persist: true
        },
        
        /**
         * Key
         */
        licensekey: {
            check: "String",
            init: null,
            event: "changeLicenseKey",
            nullable: false,
            persist: true
        },
              
        /**
         * The type. Always *article*.
         */
        type: {
            check: "String",
            init: "application"
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
    }
    
});
