/**
 * This is the model of the password used by the package {@link com.vcollaborate.emenu.passwords}.
 */
qx.Class.define("com.vcollaborate.emenu.model.Password", {
    extend: com.vcollaborate.emenu.model.Article,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of article.
     * 
     * @param _id {String?null} _id
     * @param title {String?null} title
     * @param category {com.vcollaborate.emenu.model.Category?null} category
     * @param content {String?""} content
     * @param order {Integer?0} order
     */
    construct: function(_id, title, category, content, order){
        this.base(arguments, _id, title, category, content, order);
		
		/* Translate these strings */
		this.tr("username");
		this.tr("password");
		this.tr("url");
    },

    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
		/**
         * The username of this password entry.
         */
        username: {
            init: null,
            nullable: true,
            check: "String",
            event: "changeUsername"
        },
		
		password: {
            init: null,
            nullable: true,
            check: "String",
            event: "changePassword",
            persist: false,
            apply: "applyPassword"
        },
		
		passwordRepeat: {
            init: null,
            nullable: true,
            check: "String",
            event: "changePasswordRepeat",
			persist: false
        },
		
		passwordEncrypted: {
			init: null,
            nullable: true,
            check: "String",
            event: "changePasswordEncoded",
			persist: true
		}, 
		
		url: {
            init: null,
            nullable: true,
            check: "String",
            event: "changeURL",
			validate: com.vcollaborate.emenu.utils.Validate.urlOrEmpty()
        },
		
		icon: {
			init: null,
            nullable: true,
            check: "String",
            event: "changeIcon"
		},
		
		/**
         * The type. Always *password*.
         */
        type: {
            refine : true,
            init: "password"
        }
    },
    
	statics: {
		delegate: {
			getModelClass: function(properties) {
         		return com.vcollaborate.emenu.model.Password;
        	}
      	}
	},
	
	members: {
		applyPassword: function(val) {
			//TODO encode Password and save it to passwordEncrypted
			var user = this.couchdb().getCurrentUser();
			this.setPasswordEncrypted(com.vcollaborate.emenu.utils.Crypt.getInstance().encrypt(val, user.getPassword()));
			val = null;
		}
	},
	
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
    }
});
