qx.Mixin.define("com.vcollaborate.emenu.io.TableData", {
	
	include: [qx.locale.MTranslation],
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        toTable: function(filter){
			this.debug("toTable");
            
			var tableData = [];
			for(var i = 0; i < filter.length; i++) {
				var name = filter[i][0];
				var type = filter[i][1] || "text";
				if(this["get" + qx.lang.String.firstUp(name)]) {
					tableData.push([this.tr(name), this["get" + qx.lang.String.firstUp(name)](), {"type":type}]);
				}
			}
			
			return tableData;
        }
    }
});
