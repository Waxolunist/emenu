/**
 * The couchdb communicates with the couchdbserver
 * ODM means ObjectDocumentManager. This mixin should be mixed in in every model
 * to save on a couchdb server.
 */
qx.Mixin.define("com.vcollaborate.emenu.io.CouchDBODM", {

    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
        /**
         * Serializes the properties of the given qooxdoo object into a native object.
         *
         * It ignores property groups and properties with the attribute persist set to false.
         *
         * @see qx.util.Serializer#toNativeObject
         *
         * @param object {Object} Any qooxdoo object
         * @return {String} The serialized object.
         */
        qxSerializer: function(object){
            var result = {};
            
            var properties = qx.util.PropertyUtil.getAllProperties(object.constructor);

            for (var name in properties) {
                // ignore property groups
                if (properties[name].group != undefined) {
                    continue;
                }
                
                if (properties[name].persist != undefined && !properties[name].persist) {
                    continue;
                }
                
                var value = object["get" + qx.lang.String.firstUp(name)]();
                result[name] = qx.util.Serializer.toNativeObject(value, com.vcollaborate.emenu.io.CouchDBODM.qxSerializer);
            }
            
            return result;
        },
        
        toModelReviver: function (key, value) {
		    var d;
		    if (typeof value === 'string' &&
		            value.slice(0, 2) === '@(' &&
		            value.slice(-1) === ')') {
		        d = new Date(value.slice(5, -1));
		        if (d) {
		            return d;
		        }
		    }
		    return value;
		},
		
		dateFormat : new qx.util.format.DateFormat("@(dd/MM/yyyy)"),
		
		jsonToModel : function(jsonstring, delegate) {
			try {
				qx.log.Logger.debug(com.vcollaborate.emenu.io.CouchDBODM, "jsonToModel");
				var json = new qx.data.marshal.Json(delegate); 
    			return json.toModel(qx.lang.Json.parse(jsonstring, com.vcollaborate.emenu.io.CouchDBODM.toModelReviver));
			} catch(e) {
				return e;
			}
		}
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * The couchdb object to communicate.
         *
         * @see com.vcollaborate.emenu.io.CouchDB
         */
        __couchdb: null,
        
        couchdb: function() {
        	if(!this.__couchdb) {
        		this.__couchdb = com.vcollaborate.emenu.io.CouchDB.getInstance();
        	}
        	
        	return this.__couchdb;
        },
        
        /**
         * Saves this object.
         *
         * @return {void}
         */
        save: function(){
            this.debug("save: " + this.get_id());
            var oThis = this;
            
            if(qx.Class.hasInterface(qx.Class.getByName(this.classname), com.vcollaborate.emenu.io.IUser) && this.isAdmin()) {
				var options = {
	    			context: this,
	    			success: function(data, textStatus, jqXHR) {
	    				this.debug("success creating user " + this.getName());
	    				this.fireDataEvent('admincreated',data,this.getName());
	    			}, 
	    			error: function(jqXHR, textStatus, errorThrown) {
	    				this.debug(errorThrown);
	    				
	    				//TODO create Fallback if unauthorized! or detect admin party in front
	    				//alert(jqXHR.error);
	    			}
	    		};
	    		if(com.vcollaborate.emenu.io.CouchDB.hasConnection()) {
	    			this.couchdb.couch().config(options, 'admins', this.getName(), this.getPassword());
	    		}            	
            } else {
	            this.couchdb().saveDocument(this.toNativeObject(), function(resp){
	                oThis.set_rev(resp.rev);
	                oThis.set_id(resp.id);
	            });
            }
        },
        
        /**
         * Deletes this object.
         *
         * @return {void}
         */
        remove: function(){
            this.debug("delete " + this.get_id());
            
            this.couchdb().deleteDocument({
                _id: this.get_id(),
                _rev: this.get_rev()
            });
        },
        
        /**
         * Converts this object to a json object.
         *
         * @return {var} a json object
         */
        toNativeObject: function(){
            this.debug("toNativeObject" + this.get_id());
            var nativeObject = qx.util.Serializer.toNativeObject(this, com.vcollaborate.emenu.io.CouchDBODM.qxSerializer);
            
            if (this.get_id() == null) {
                delete nativeObject._id;
                delete nativeObject._rev;
            }
            else 
                if (this.get_rev() == null) {
                    delete nativeObject._rev;
                }
            
            return nativeObject;
        },
        
        toJson: function() {
        	this.debug("toJson");
        	return qx.util.Serializer.toJson(this, null, com.vcollaborate.emenu.io.CouchDBODM.dateFormat);// license.toNativeObject();
        }
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * the id of this object
         */
        _id: {
            check: "String",
            event: "changeID",
            nullable: true
        },
        
        /**
         * the revision of this object
         */
        _rev: {
            check: "String",
            event: "changeREV",
            nullable: true
        }
    }
});