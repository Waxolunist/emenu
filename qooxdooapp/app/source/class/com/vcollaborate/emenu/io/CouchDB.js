/**
 * The couchdb communicates with the couchdbserver
 */
 /* ************************************************************************
 #ignore(jQuery)
 ************************************************************************ */
qx.Class.define("com.vcollaborate.emenu.io.CouchDB", {
    extend: qx.core.Object,
    type: "singleton",
    
    /*
     *****************************************************************************
     CONSTRUCT
     *****************************************************************************
     */
    construct: function(){
        this.base(arguments);
        
        if(!this.__couchLoaded) {
        	var sl = new com.vcollaborate.emenu.utils.ScriptLoader();
        	sl.loadScriptArray([
  				"js/sha1.js",
  				"js/json2.js",
  				"js/jquery-1.6.2.min.js",
  				"js/jquery.couch.js",
  				"js/jquery.couch.app.js",
  				"js/jquery.couch.app.util.js"
        	], function(){
        		this.fireDataEvent('scriptLoaded', null);
        		this.__couchLoaded = true;
        		this.set({
           			database: "emenu_00"//document.location.href.split('/')[3]
        		});
        	}, this);
    	}
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        
    	__couchLoaded: false,
    	
		/*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Saves a document on the couchdb.
         *
         * @param data {var} a json object string to store
         * @param oncompleted {var} callback function called on success
         * @param onerror {var} callback function called on error
         * @param ajaxOptions {var} see jquery documentation for valid ajaxOptions
         * @return {void}
         */
        saveDocument: function(data, oncompleted, onerror, ajaxOptions){
            if (this.DB()) {
                this.DB().saveDoc(data, {
                    success: oncompleted,
                    error: onerror
                }, ajaxOptions);
            }
        },
        
        /**
         * Deletes a document on the couchdb.
         *
         * @param data {var} a json object string to store
         * @param oncompleted {var} callback function called on success
         * @param onerror {var} callback function called on error
         * @param ajaxOptions {var} see jquery documentation for valid ajaxOptions
         * @return {void}
         */
        deleteDocument: function(data, oncompleted, onerror, ajaxOptions){
            if (this.DB()) {
                var options = {};
                options["success"] = oncompleted;
                options["error"] = onerror;
                jQuery.extend(options, ajaxOptions);
                this.DB().removeDoc(data, options);
            }
        },
        
        /**
         * Opens a document on the couchdb with a specific id.
         *
         * @param id {var} the id of the document to open
         * @param oncompleted {var} callback function called on success
         * @param onerror {var} callback function called on error
         * @param ajaxOptions {var} see jquery documentation for valid ajaxOptions
         * @return {void}
         */
        openDocument: function(id, oncompleted, onerror, ajaxOptions){
            if (this.DB()) {
                this.DB().openDoc(id, {
                    success: oncompleted,
                    error: onerror
                }, ajaxOptions);
            }
        },
        
        /**
         * Queries a view.
         *
         * @param name {var} the name of the view
         * @param keys {var} the keys to query
         * @param oncompleted {var} callback function called on success
         * @param onerror {var} callback function called on error
         * @return {void}
         */
        queryView: function(name, keys, oncompleted, onerror){
            if (this.DB()) {
                var options = keys || {};
                options["success"] = oncompleted;
                options["error"] = onerror;
                this.DB().view(name, options);
            }
        },
        
        /**
         * Returns null if jQuery is not defined, otherwise it returns the couchdb object.
         *
         * @return {null | var} null if jQuery is not defined, otherwise it returns the couchdb object
         */
        DB: function(){
            if (this.couch()) {
            	return this.couch().db(this.getDatabase());
            }
            return null;
        },
        
        couch: function() {
        	if (typeof jQuery == 'undefined') {
                return null;
            }
            
            var couch = jQuery.couch;
            couch.urlPrefix = "http://localhost/couch";
            return couch;
        },
        
        isAdminParty: function(callback, context) {
        	if(com.vcollaborate.emenu.io.CouchDB.hasConnection()) {
	        	this.couch().session({
	        		success : function(r) {
			        	callback.call(context, r.userCtx.roles.indexOf("_admin") != -1);
	        		}
	        	});
        	} else {
        		callback.call(context, false);
        	}
        },
        
        login: function(username, password) {
        	this.debug("login");
			return (!com.vcollaborate.emenu.io.CouchDB.hasConnection() && username == password) && (!com.vcollaborate.emenu.utils.String.isBlank(username));
        }
    },
    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
    	hasConnection: function() {
    		var couchdb = com.vcollaborate.emenu.io.CouchDB.getInstance();
    		try {
    			couchdb.DB().info();
    			return true;
    		} catch(ex) {
    			return false;
    		}
    	}
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
		/**
		 * The database name.
		 */
        database: {
            check: "String",
            init: "emenu"
        },
        
        currentUser: {
        	check: "com.vcollaborate.emenu.io.IUser",
        	init: null
        }
    },
    
    events : {
 		/**
 		* fires when a script is loaded
		*/
	 	scriptLoaded: 'qx.event.type.Event'
	}
});