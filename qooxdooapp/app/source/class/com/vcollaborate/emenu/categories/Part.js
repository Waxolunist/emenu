/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/list-add.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/list-remove.png)
 #asset(com/vcollaborate/emenu/images/edit.png)
 #asset(qx/icon/${qx.icontheme}/22/places/*)
 ************************************************************************ */
/**
 * This part adds the possibility to add, edit and remove {@link com.vcollaborate.emenu.mode.Category}.
 * It provides only a leftsidepane.
 */
qx.Class.define("com.vcollaborate.emenu.categories.Part", {
    extend: com.vcollaborate.emenu.utils.AbstractPart,
    type: "singleton",

    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
	/**
     * Creates a new instance of Part.
     */
    construct: function(){
        this.base(arguments);

        if (!this.__addCategoryWindow) {
            this.__addCategoryWindow = new com.vcollaborate.emenu.categories.AddCategoryWindow();
        }

        // Create tree view
        if (!this.__treeView) {
            //TODO appearance
            this.__treeView = new qx.ui.tree.Tree();
            this.__treeView.setWidth(300);
            this.__treeView.setMinWidth(200);
            this.__treeView.setBackgroundColor("white");
        }

        // create the controller which binds the feeds to the tree
        // 1. Parameter: The model (root feed folder)
        // 2. Parameter: The view (the tree widget)
        // 3. Parameter: name of the children of the model items
        // 4. Parameter: name of the model property to show as label in the tree
        if (!this.__treeController) {
            this.__treeController = new qx.data.controller.Tree(com.vcollaborate.emenu.model.Category.getRoot(), this.__treeView, "categories", "title");
			this.__treeController.setIconPath("icon");
        }

        this.__treeView.getRoot().setOpen(true);
		this.__treeView.setRootOpenClose(true);
        this.__treeView.setHideRoot(true);
    },

    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {

        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __addCategoryWindow: null,
        __treeView: null,
        __addCategoryCmd: null,
        __editCategoryCmd: null,
        __removeCategoryCmd: null,
        __treeController: null,

        /*
         *****************************************************************************
         DERIVED
         *****************************************************************************
         */
        /**
         * Adds the window for adding categories ({@link com.vcollaborate.emenu.categories.AddCategoryWindow}) to
         * the root of the application and loads recursively the root category ({@link com.vcollaborate.emenu.model.Category}).
         *
         * @param controller {com.vcollaborate.emenu.Application} The main application class
         * @return {void}
         */
        init: function(controller){
            this.base(arguments);
            var rootCategory = com.vcollaborate.emenu.model.Category.getRoot();
            rootCategory.loadRecursive();

            controller.getRoot().add(this.__addCategoryWindow);
        },

        /**
         * Inits following three commands:
         *
         * <ul>
         * <li>{@link #getAddCategoryCmd}</li>
         * <li>{@link #getEditCategoryCmd}</li>
         * <li>{@link #getRemoveCategoryCmd}</li>
         * </ul>
         *
         * @param commands {var} the global commands object
         * @return {void}
         */
        initCommands: function(commands){
            this.base(arguments);
            commands.addCategory = new qx.ui.core.Command("Control+Shift+C");
            commands.addCategory.addListener("execute", this.__showAddCategory, this);
            this.__addCategoryCmd = commands.addCategory;

            commands.editCategory = new qx.ui.core.Command("Control+Shift+V");
            commands.editCategory.addListener("execute", this.__showEditCategory, this);
			commands.editCategory.setEnabled(false);
            this.__editCategoryCmd = commands.editCategory;

            commands.removeCategory = new qx.ui.core.Command("Control+Shift+B");
            commands.removeCategory.addListener("execute", this.__removeCategory, this);
			commands.removeCategory.setEnabled(false);
            this.__removeCategoryCmd = commands.removeCategory;
        },

        /**
         * Inits the buttons for the toolbar, one for each command.
         *
         * @see #initCommands
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @param part {qx.ui.toolbar.Part} the part to add the buttons to
         * @return {void}
         */
        initButtons: function(commands, buttons, part){
            this.base(arguments);
            buttons.addCategory = new qx.ui.toolbar.Button(this.tr("Add category"), "icon/22/actions/list-add.png");
            buttons.addCategory.setCommand(commands.addCategory);
            part.addAt(buttons.addCategory, 0);

            buttons.editCategory = new qx.ui.toolbar.Button(this.tr("Edit category"), "com/vcollaborate/emenu/images/edit.png");
            buttons.editCategory.setCommand(commands.editCategory);
            part.addAt(buttons.editCategory, 1);

            buttons.removeCategory = new qx.ui.toolbar.Button(this.tr("Remove category"), "icon/22/actions/list-remove.png");
            buttons.removeCategory.setCommand(commands.removeCategory);
            part.addAt(buttons.removeCategory, 2);
			
			part.show();
        },

        /**
         * Inits the listeners to handle the disable- and enable-actions of the toolbar buttons.
         *
         * @return {void}
         */
        initListeners: function(){
            this.base(arguments);

            this.__treeView.addListener("changeSelection", function(e){
                if (e.getData().length > 0) {
                    this.getRemoveCategoryCmd().setEnabled(true);
                    this.getAddCategoryCmd().setEnabled(true);

                    if (com.vcollaborate.emenu.categories.RenameCategoryWidget.currentEditedTreeItem == null) {
                        this.getEditCategoryCmd().setEnabled(true);
                    }

                    if (com.vcollaborate.emenu.utils.PartLoaderHelper.isPartActive("com.vcollaborate.emenu.articles")) {
                        com.vcollaborate.emenu.articles.Part.getInstance().getAddArticleCmd().setEnabled(true);
                    }
					
					if (com.vcollaborate.emenu.utils.PartLoaderHelper.isPartActive("com.vcollaborate.emenu.passwords")) {
						com.vcollaborate.emenu.passwords.Part.getInstance().getAddPasswordCmd().setEnabled(true);
					}
                }
                else {
                    this.getRemoveCategoryCmd().setEnabled(false);
                    this.getEditCategoryCmd().setEnabled(false);

                    if (com.vcollaborate.emenu.utils.PartLoaderHelper.isPartActive("com.vcollaborate.emenu.articles")) {
                        com.vcollaborate.emenu.articles.Part.getInstance().getAddArticleCmd().setEnabled(false);
                    }
					
					if (com.vcollaborate.emenu.utils.PartLoaderHelper.isPartActive("com.vcollaborate.emenu.passwords")) {
						com.vcollaborate.emenu.passwords.Part.getInstance().getAddPasswordCmd().setEnabled(false);
					}
                }
            }, this);
        },

        /**
         * Sets the tooltips for each button of this part.
         *
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @return {void}
         */
        setToolTips: function(commands, buttons){
            this.base(arguments);
            buttons.addCategory.setToolTipText(commands.addCategory.toString());
            buttons.editCategory.setToolTipText(commands.editCategory.toString());
            buttons.removeCategory.setToolTipText(commands.removeCategory.toString());
        },

        /**
         * Returns the tree {@link qx.ui.tree.Tree} representing all instances of
         * {@link com.vcollaborate.emenu.model.Category} attached to the root category.
         *
         * @return {qx.ui.tree.Tree} {@link qx.ui.tree.Tree}
         */
        getLeftSidePane: function(){
            this.base(arguments);
            return this.__treeView;
        },

        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Opens the {@link com.vcollaborate.emenu.categories.AddCategoryWindow} to add a
         * {@link com.vcollaborate.emenu.model.Category}.
         *
         * @return {void}
         */
        __showAddCategory: function(){
            this.debug("__showAddCategory");
            this.__addCategoryWindow.center();
            this.__addCategoryWindow.open();
        },

        /**
         * Attaches a {@link com.vcollaborate.emenu.categories.RenameCategoryWidget} to edit
         * the selected {@link com.vcollaborate.emenu.model.Category} to the selected treeitem.
         *
         * @return {void}
         */
        __showEditCategory: function(){
            this.debug("__showEditCategory");

            if (com.vcollaborate.emenu.categories.RenameCategoryWidget.currentEditedTreeItem == null) {
                var categorySelection = this.getSelectedCategory();
                var treeItem = this.getSelectedTreeItem();
                this.getEditCategoryCmd().setEnabled(false);
                var renameWidget = new com.vcollaborate.emenu.categories.RenameCategoryWidget(treeItem, categorySelection);

                renameWidget.addListener("finishEditing", function(e){
                    this.getEditCategoryCmd().setEnabled(true);
                }, this);
            }
        },

        /**
         * Removes the selected {@link com.vcollaborate.emenu.model.Category}.
         *
         * @return {void}
         */
        __removeCategory: function(){
            this.debug("__removeCategory");

            var categorySelection = this.getSelectedCategory();

            if (categorySelection) {
                if (com.vcollaborate.emenu.categories.RenameCategoryWidget.currentEditedTreeItem == this.getSelectedTreeItem()) {
                    com.vcollaborate.emenu.categories.RenameCategoryWidget.currentEditedTreeItem = null;
                }

                categorySelection.removeRecursive();
            }
        },

        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Returns the current selected {@link com.vcollaborate.emenu.model.Category}
         *
         * @return {com.vcollaborate.emenu.model.Category} {@link com.vcollaborate.emenu.model.Category}
         */
        getSelectedCategory: function(){
            return this.__treeController.getSelection().getItem(0);
        },

        /**
         * Returns the current selected {@link qx.ui.tree.TreeFolder}
         *
         * @return {qx.ui.tree.TreeFolder} {@link qx.ui.tree.TreeFolder}
         */
        getSelectedTreeItem: function(){
            return this.__treeView.getSelection()[0];
        },

        /**
         * Returns the {@link qx.data.controller.Tree} bound to the {@link qx.ui.tree.Tree}.
         *
         * @return {qx.data.controller.Tree} {@link qx.data.controller.Tree}
         */
        getTreeController: function(){
            return this.__treeController;
        },

        /**
         * Returns the command __addCategory__.
         * The command has the shortcut *Control+Shift+C* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getAddCategoryCmd: function(){
            return this.__addCategoryCmd;
        },

        /**
         * Returns the command __editCategory__.
         * The command has the shortcut *Control+Shift+V* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getEditCategoryCmd: function(){
            return this.__editCategoryCmd;
        },

        /**
         * Returns the command __removeCategory__.
         * The command has the shortcut *Control+Shift+B* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getRemoveCategoryCmd: function(){
            return this.__removeCategoryCmd;
        }
    },

    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
		this.debug("destruct");

        this._disposeObjects("__addCategoryWindow", "__treeView", "__addCategoryCmd", "__editCategoryCmd", "__removeCategoryCmd", "__treeController");
    }
});
