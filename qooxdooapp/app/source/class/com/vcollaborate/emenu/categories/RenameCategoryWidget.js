/**
 * Rename category window
 */
qx.Class.define("com.vcollaborate.emenu.categories.RenameCategoryWidget", {
    extend: qx.ui.container.Composite,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of RenameCategoryWidget.
     *
     * @param treeItem {qx.ui.tree.TreeFolder} treeItem
     * @param selectedCategory {com.vcollaborate.emenu.model.Category} selectedCategory
     */
    construct: function(treeItem, selectedCategory){
        this.base(arguments);
        
        this.setCurrentEditedTreeItem(treeItem);
        this.__currentSelectedCategory = selectedCategory;
        
        this.__initLayout();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __textfield: null,
        __okButton: null,
        __currentSelectedCategory: null,
		__iconChooserListenerID: null,
		__currentEditedTreeItemListenerID: null,
		__iconChooserWindow: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Inits the layout of this widget.
         *
         * @return {void}
         */
        __initLayout: function(){
            this.debug("__initLayout");
            
            this.__textfield = new qx.ui.form.TextField(this.__currentSelectedCategory.getTitle()).set({
                width: 160
            });
            this.__okButton = new qx.ui.form.Button(this.tr("OK"));
            
            this.__textfield.selectAllText();
            this.getCurrentEditedTreeItem().setLabel("");
            this.getCurrentEditedTreeItem().addWidget(this.__textfield);
            this.getCurrentEditedTreeItem().addWidget(this.__okButton);
        },
        
        /**
         * Inits the listeners of this widget.
         *
         * @return {void}
         */
        __initListeners: function(){
            this.debug("__initListeners");
            
            this.__okButton.addListener("execute", this.__save, this);
            
            this.__textfield.addListener("keypress", function(e){
                var key = e.getKeyIdentifier();
                
                if (key == "Enter") {
                    this.__save();
                }
                else 
                    if (key == "Escape") {
                        this.__cancel();
                    }
            }, this);
			
			this.__currentEditedTreeItemListenerID = this.getCurrentEditedTreeItem().addListener("keypress", function(e){
                var key = e.getKeyIdentifier();
                
                if (key == "Enter") {
                    this.__save();
                }
                else 
                    if (key == "Escape") {
                        this.__cancel();
                    }
            }, this);
			
			this.__iconChooserListenerID = this.getCurrentEditedTreeItem().getChildControl("icon").addListener("click", this.__chooseIcon, this);
        },
        
        /**
         * Saves the changed value.
         *
         * @return {void}
         */
        __save: function(){
            this.debug("__save");
            
            this.__currentSelectedCategory.setTitle(this.__textfield.getValue());
            this.__currentSelectedCategory.save();
            this.getCurrentEditedTreeItem().setLabel(this.__currentSelectedCategory.getTitle());
            this.__finishEditing();
        },
        
        /**
         * Cancels the current started actions.
         *
         * @return {void}
         */
        __cancel: function(){
            this.debug("__cancel");
            
            this.getCurrentEditedTreeItem().setLabel(this.__currentSelectedCategory.getTitle());
            this.__finishEditing();
        },
        
        /**
         * Method called after editing. Fires the event __finishEditing__.
         *
         * @return {void}
         */
        __finishEditing: function(){
            this.debug("__finishEditing");
            
            this.__textfield.destroy();
            this.__okButton.destroy();
			this.getCurrentEditedTreeItem().getChildControl("icon").removeListenerById(this.__iconChooserListenerID);
			this.getCurrentEditedTreeItem().removeListenerById(this.__currentEditedTreeItemListenerID);
            this.setCurrentEditedTreeItem(null);
            this.fireEvent("finishEditing");
        },
        
		/**
		 * Init the choose icon widgets
		 * 
		 * @param e {Event} e
		 */
		__chooseIcon: function(e) {
			this.debug("__chooseIcon");
			
			if (!this.__iconChooserWindow) {
                this.__iconChooserWindow = new com.vcollaborate.emenu.view.IconChooser();
                this.__iconChooserWindow.addListener("iconSelected", this.__iconChanged, this);
                qx.core.Init.getApplication().getRoot().add(this.__iconChooserWindow);
            }
            
            this.__iconChooserWindow.center();
            this.__iconChooserWindow.open();
		},
		
		/**
		 * Called after icon has changed.
		 * 
		 * @param e {Event} e
		 */
		__iconChanged: function(e) {
			this.debug("__iconChanged");
			
			this.__currentSelectedCategory.setIcon(e.getData());
		},
		
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Sets the currentEditedTreeItem to the given value
         * 
         * @see #currentEditedTreeItem
         *
         * @param treeItem {qx.ui.tree.TreeFolder} the current edited treefolder or null, if nothing is edited
         * @return {void}
         */
        setCurrentEditedTreeItem: function(treeItem){
			this.debug("setCurrentEditedTreeItem");
			
            com.vcollaborate.emenu.categories.RenameCategoryWidget.currentEditedTreeItem = treeItem;
        },
        
        /**
         * Returns the currentEditedTreeItem to the given value
         *
         * @return {qx.ui.tree.TreeFolder} {@link #currentEditedTreeItem}
         */
        getCurrentEditedTreeItem: function(){
			this.debug("getCurrentEditedTreeItem");
			
            return com.vcollaborate.emenu.categories.RenameCategoryWidget.currentEditedTreeItem;
        }
    },
	
    /*
     *****************************************************************************
     EVENTS
     *****************************************************************************
     */
    events: {
        /**
         * Fired when the editing is finished because of a save or a cancel action.
         */
        "finishEditing": "qx.event.type.Event"
    },
    
    /*
     *****************************************************************************
     STATICS
     *****************************************************************************
     */
    statics: {
        /**
         * Indicates if and when which {@link qx.ui.tree.TreeFolder} is currently edited.
         */
        currentEditedTreeItem: null
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
		this.debug("destruct");
		
        this._disposeObjects("__textfield", "__okButton", "__currentSelectedCategory", "__iconChooserListenerID", "__currentEditedTreeItemListenerID", "__iconChooserWindow");
    }
});