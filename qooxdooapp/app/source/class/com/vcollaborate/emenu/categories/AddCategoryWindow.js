/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/document-new.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-apply.png)
 ************************************************************************ */
/**
 * Add new category window
 */
qx.Class.define("com.vcollaborate.emenu.categories.AddCategoryWindow", {
    extend: qx.ui.window.Window,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of AddCategoryWindow.
     */
    construct: function(){
        this.base(arguments, this.tr("Add category"), "icon/22/actions/document-new.png");
        
        // set the properties of the window
        this.set({
            modal: true,
            showMinimize: false,
            showMaximize: false,
            allowMaximize: false,
			maxWidth: 600
        });
        
        this.setResizableBottom(false);
        this.setResizableTop(false);
        
        this.__addContent();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __parentCategorySelectBox: null,
        __titleTextfield: null,
        __form: null,
        __categories: null,
        __listcontroller: null,
        __iconChooserWindow: null,
        __iconChooserButton: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Adds the content of the window.
         *
         * @return {void}
         */
        __addContent: function(){
            this.debug("__addContent");
            
            this.setLayout(new qx.ui.layout.VBox(10));
            
            // create the UI ///////////////////
            // groupbox
            var groupBox = new qx.ui.groupbox.GroupBox(this.tr("Category Information"));
            groupBox.setWidth(200);
            groupBox.setLayout(new qx.ui.layout.VBox(10));
            
            this.__parentCategorySelectBox = new qx.ui.form.SelectBox();
            this.__initCategories();
            
            //bind
            this.__listcontroller = new qx.data.controller.List(this.__categories, this.__parentCategorySelectBox, "title");
            groupBox.add(this.__parentCategorySelectBox);
            
            this.__titleTextfield = new qx.ui.form.TextField().set({
                required: true,
                height: 28,
                paddingTop: 5,
                paddingLeft: 4
            });
            this.__titleTextfield.setPlaceholder(this.tr("Title"));
            
            this.__iconChooserButton = new qx.ui.form.Button("...").set({
                height: 28
            });
            this.__iconChooserButton.addListener("execute", this.__chooseIcon, this);
            
            var horizontalTitlePane = new qx.ui.container.Composite(new qx.ui.layout.Dock());
            horizontalTitlePane.add(this.__titleTextfield, {
                edge: "center"
            });
            horizontalTitlePane.add(this.__iconChooserButton, {
                edge: "west"
            });
            
            groupBox.add(horizontalTitlePane);
            
            var addButton = new qx.ui.form.Button(this.tr("Add"), "icon/22/actions/dialog-apply.png");
            
            addButton.set({
                alignX: "right",
                allowGrowX: false
            });
            
            addButton.addListener("execute", this.__addCategory, this);
            groupBox.add(addButton);
            this.add(groupBox);
            
            // create a form
            this.__form = new qx.ui.form.Form();
            
            // set the headline of the form
            this.__form.addGroupHeader(this.tr("Category Information"));
        },
        
        /**
         * Inits the categories to select.
         *
         * @return {void}
         */
        __initCategories: function(){
            this.debug("__initCategories");
            
            var categoryRoot = com.vcollaborate.emenu.model.Category.getRoot();
            this.__categories = categoryRoot.getCategories().copy();
            this.__categories.insertAt(0, categoryRoot);
            
            if (this.__listcontroller) {
                this.__listcontroller.setModel(this.__categories);
            }
            
            this.__parentCategorySelectBox.focus();
        },
        
        /**
         * Handles button clicks on 'Add' button or/and
         * pressing enter key on textfields
         *
         * @param e {qx.event.type.Event} Execute event
         * @return {void}
         */
        __addCategory: function(e){
            this.debug("__addCategory");
            
            if (this.__form.validate()) {
                var parentcategory = this.__parentCategorySelectBox.getSelection()[0].getModel();
                var parentcategoryID = null;
                
                if (parentcategory) {
                    parentcategoryID = parentcategory.get_id();
                }
                
                var title = this.__titleTextfield.getValue();
                
                if (!title) {
                    title = "";
                }
                
                var iconPath = this.__iconChooserButton.getIcon();
                
                this.__iconChooserButton.setIcon(null);
                this.__iconChooserButton.setLabel("...");
                
                var newcategory = new com.vcollaborate.emenu.model.Category(null, title, parentcategory, null, iconPath);
                newcategory.save();
                
                // clear the content of the window
                this.__titleTextfield.setValue("");
                
                this.close();
            }
        },
        
        /**
         * Inits the listeners
         *
         * @return {void}
         */
        __initListeners: function(){
            this.debug("__initListeners");
            
            this.addListener("resize", this.center, this);
            this.addListener("appear", this.__initCategories, this);
            
            this.addListener("close", this.__cleanup, this);
            
            this.addListener("keypress", function(e){
                var key = e.getKeyIdentifier();
                
                if (key == "Escape") {
                    this.close();
                }
            }, this);
            
            //when pressing enter on textfields, try to add the category
            this.addListener("keypress", function(e){
                if (e.getTarget() instanceof qx.ui.form.TextField && e.getKeyIdentifier() === "Enter") {
                    this.__addCategory();
                }
            });
        },
        
        /**
         * Starts the iconchooser.
         */
        __chooseIcon: function(){
            this.debug("__chooseIcon");
            
            if (!this.__iconChooserWindow) {
                this.__iconChooserWindow = new com.vcollaborate.emenu.view.IconChooser();
                this.__iconChooserWindow.addListener("iconSelected", this.__iconSelected, this);
                qx.core.Init.getApplication().getRoot().add(this.__iconChooserWindow);
            }
            
            this.__iconChooserWindow.center();
            this.__iconChooserWindow.open();
        },
        
        /**
         * Called after a icon is selected
         *
         * @param e {qx.event.type.Data} e
         */
        __iconSelected: function(e){
            this.debug("__iconSelected");
            this.__iconChooserButton.setIcon(e.getData());
            this.__iconChooserButton.setLabel(null);
        },
        
        /**
         * Clean up interface
         * @param e {Event} e
         */
        __cleanup: function(e){
            this.__parentCategorySelectBox.resetSelection();
            
            this.__titleTextfield.setValue(null);
            
            this.__iconChooserButton.setIcon(null);
            this.__iconChooserButton.setLabel("...");
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this.__commands = null;
        this._disposeObjects("__titleTextfield", "__parentCategorySelectBox", "__form", "__categories", "__listcontroller", "__iconChooserWindow", "__iconChooserButton");
    }
});
