/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/document-new.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-apply.png)
 ************************************************************************ */
/**
 * The create test data window
 */
qx.Class.define("com.vcollaborate.emenu.test.CreateTestDataWindow", {
    extend: qx.ui.window.Window,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of CreateTestDataWindow.
     */
    construct: function(){
        this.base(arguments, this.tr("Create Testdata"), "icon/22/actions/document-new.png");
        
        // set the properties of the window
        this.set({
            modal: true,
            showMinimize: false,
            showMaximize: false,
            allowMaximize: false
        });
        
        // Create the content with a helper
        this.__addContent();
        
        this.addListener("resize", this.center, this);
        
        this.addListener("keypress", function(e){
            var key = e.getKeyIdentifier();
            
            if (key == "Escape") {
                this.close();
            }
        }, this);
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __form: null,
        __countLabel: null,
        __parentCategoriesValue: 10,
        __childCategoriesValue: 10,
        __articlesValue: 10,
        __sliders: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Adds the content of the window.
         *
         * @return {void}
         */
        __addContent: function(){
			this.debug("__addContent");
			
            this.setLayout(new qx.ui.layout.VBox(10));
            
            // create the UI ///////////////////
            // groupbox
            var groupBox = new qx.ui.groupbox.GroupBox(this.tr("Category Information"));
            groupBox.setWidth(700);
            groupBox.setLayout(new qx.ui.layout.VBox(10));
            
            groupBox.add(this.__createHorizontalLayout());
            
            var addButton = new qx.ui.form.Button(this.tr("Add"), "icon/22/actions/dialog-apply.png");
            
            addButton.set({
                alignX: "right",
                allowGrowX: false
            });
            
            addButton.addListener("execute", this.__createTestData, this);
            groupBox.add(addButton);
            
            this.add(groupBox, {
                flex: 1,
                height: "100%"
            });
            
            // create a form
            this.__form = new qx.ui.form.Form();
            
            // set the headline of the form
            this.__form.addGroupHeader(this.tr("Category Information"));
        },
        
		/**
         * Creates the sliders.
         *
         * @return {qx.ui.container.Composite} {@link qx.ui.container.Composite}
         */
        __createHorizontalLayout: function(){
			this.debug("__createHorizontalLayout");
			
            this.__sliders = [];
            
            this.__sliders.push(this.__createSliderGroup(new qx.ui.form.Slider().set({
                maximum: 100,
                value: this.__parentCategoriesValue
            }), "__parentCategoriesValue", this.tr("Number of<br/>parentcategories")));
            
            this.__sliders.push(this.__createSliderGroup(new qx.ui.form.Slider().set({
                maximum: 100,
                value: this.__childCategoriesValue
            }), "__childCategoriesValue", this.tr("Number of childcategories<br/>per parentcategory")));
            
            this.__sliders.push(this.__createSliderGroup(new qx.ui.form.Slider().set({
                maximum: 100,
                value: this.__articlesValue
            }), "__articlesValue", this.tr("Number of articles<br/>per category")));
            
            var grid = new qx.ui.layout.Grid();
            var container = new qx.ui.container.Composite(grid);
            
            container.setPadding(20);
            container.setWidth(600);
            
            grid.setSpacing(5);
            grid.setColumnFlex(0, 1);
            grid.setColumnFlex(1, 2);
            grid.setColumnFlex(2, 2);
            grid.setColumnFlex(3, 2);
            
            grid.setColumnAlign(0, "left", "bottom");
            grid.setColumnAlign(1, "left", "bottom");
            grid.setColumnAlign(2, "center", "bottom");
            grid.setColumnAlign(3, "right", "bottom");
            
            var row = 0;
            
            for (var i = 0; i < this.__sliders.length; i++) {
                var group = this.__sliders[i];
                group.slider.setOrientation("horizontal");
                
                group.value.setWidth(100);
                group.value.setTextAlign("center");
                
                container.add(group.valueLabel, {
                    row: row,
                    column: 0,
                    rowSpan: 2
                });
                
                container.add(group.minimum, {
                    row: row,
                    column: 1
                });
                
                container.add(group.value, {
                    row: row,
                    column: 2
                });
                
                container.add(group.maximum, {
                    row: row,
                    column: 3
                });
                
                container.add(group.slider, {
                    row: row + 1,
                    column: 1,
                    colSpan: 3,
                    rowSpan: 1
                });
                
                grid.setRowHeight(row + 2, 20);
                
                row += 3;
            }
            
            this.__setCountLabel();
            
            container.add(this.__countLabel, {
                row: row + 1,
                column: 0,
                colSpan: 4,
                rowSpan: 1
            });
            
            return container;
        },
		
		/**
         * Creates the group for a slider with labels.
         *
         * @param slider {qx.ui.form.Slider} a slider
         * @param valueName {String} the name of this value, the slider should represent  
         * @param valueTitle {String} the title of this value, the slider should represent
         * @return {var} slidergroup
         */
        __createSliderGroup: function(slider, valueName, valueTitle){
			this.debug("__createSliderGroup");
			
            var group = {
                slider: slider,
                minimum: new qx.ui.basic.Label(this.tr("Min: %1", slider.getMinimum().toString())),
                maximum: new qx.ui.basic.Label(this.tr("Max: %1", slider.getMaximum().toString())),
                value: new qx.ui.basic.Label(slider.getValue().toString()),
                valueName: valueName,
                valueLabel: new qx.ui.basic.Label(valueTitle).set({
                    rich: true
                })
            };
            
            slider.addListener("changeValue", function(e){
                group.value.setValue(slider.getValue().toString());
                eval("this." + group.valueName + "=" + slider.getValue().toString());
                this.__setCountLabel();
            }, this);
            
            return group;
        },
		
		/**
         * Sets the label counting all categories and articles.
         *
         * @return {void}
         */
        __setCountLabel: function(){
			this.debug("__setCountLabel");
			
            var countParentCategories = this.__parentCategoriesValue;
            var countChildCategories = this.__childCategoriesValue * this.__parentCategoriesValue;
            var countArticles = this.__articlesValue * this.__parentCategoriesValue + this.__articlesValue * this.__childCategoriesValue * this.__parentCategoriesValue;
            var sumcounts = countParentCategories + countChildCategories + countArticles;
            
            if (!this.__countLabel) {
                this.__countLabel = new qx.ui.basic.Label().set({
                    rich: true
                });
            }
            
            this.__countLabel.setValue(this.tr("This will create %1 parentcategories, %2 childcategories and %3  articles (alltogether %4 entries).", countParentCategories, countChildCategories, countArticles, sumcounts));
        },
		
        /**
         * Creates the test data by calling {@link com.vcollaborate.emenu.test.ProgressiveTestData#addTestData}.
         *
         * @param e {Event} the event type (can be null)
         * @return {void}
         */
        __createTestData: function(e){
            this.debug("__createTestData");
            
            if (this.__form.validate()) {
                var ptd = new com.vcollaborate.emenu.test.ProgressiveTestData();
                ptd.addTestData(this.__sliders[0].slider.getValue(), this.__sliders[1].slider.getValue(), this.__sliders[2].slider.getValue());
            }
            
            this.close();
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
		this.debug("destruct");
		
        this.__sliders = null;
        this._disposeObjects("__form", "__countLabel", "__parentCategoriesValue", "__childCategoriesValue", "__articlesValue", "__sliders");
    }
});
