/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/mimetypes/executable.png)
 #asset(qx/icon/${qx.icontheme}/48/status/dialog-information.png)
 ************************************************************************ */
/**
 * This part adds the possibility to create testdata.
 */
qx.Class.define("com.vcollaborate.emenu.test.Part", {
    extend: com.vcollaborate.emenu.utils.AbstractPart,
    type: "singleton",
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of part.
     */
    construct: function(){
        this.base(arguments);
        
        if (!this.__testWindow) {
            this.__testWindow = new com.vcollaborate.emenu.test.CreateTestDataWindow();
        }
        
        if (!this.__generateRandomWindow) {
            this.__generateRandomWindow = new com.vcollaborate.emenu.view.RandomKeyGenerator();
        }
        
        if (!this.__loginDialog) {
            this.__loginDialog = new com.vcollaborate.emenu.view.LoginDialog(this.__tryLogin, this.__successFulLogin, this);
        }
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __testWindow: null,
        __generateRandomWindow: null,
        __loginDialog: null,
        
        /*
         *****************************************************************************
         DERIVED
         *****************************************************************************
         */
        /**
         * Adds the window for creating the testdata.
         *
         * @param controller {com.vcollaborate.emenu.Application} The main application class
         * @return {void}
         */
        init: function(controller){
            this.base(arguments);
            
            controller.getRoot().add(this.__testWindow);
            controller.getRoot().add(this.__generateRandomWindow);
            controller.getRoot().add(this.__loginDialog);
        },
        
        /**
         * Inits the addTestData command.
         *
         * @param commands {var} the global commands object
         * @return {void}
         */
        initCommands: function(commands){
            this.base(arguments);
            
            commands.addTestData = new qx.ui.core.Command("Control+T");
            commands.addTestData.addListener("execute", this.__showCreateTestData, this);
            
            commands.generateRandomKey = new qx.ui.core.Command("Control+R");
            commands.generateRandomKey.addListener("execute", this.__showCreateRandomKey, this);
            
            commands.testLogin = new qx.ui.core.Command("Control+L");
            commands.testLogin.addListener("execute", this.__showTestLogin, this);
            
			commands.testNotificationBar = new qx.ui.core.Command("Control+N");
            commands.testNotificationBar.addListener("execute", this.__showNotification, this);
            
            commands.testWizard = new qx.ui.core.Command("Control+W");
            commands.testWizard.addListener("execute", this.__showInstallWizard, this);
            
        },
        
        /**
         * Inits the buttons for the toolbar, one for each command.
         *
         * @see #initCommands
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @param part {qx.ui.toolbar.Part} the part to add the buttons to
         * @return {void}
         */
        initButtons: function(commands, buttons, part){
            this.base(arguments);
            
            buttons.addTestData = new qx.ui.toolbar.Button(this.tr("Create Testdata"), "icon/22/mimetypes/executable.png");
            buttons.addTestData.setCommand(commands.addTestData);
            
            buttons.generateRandomKey = new qx.ui.toolbar.Button(this.tr("Generate Randomkey"), "icon/22/mimetypes/executable.png");
            buttons.generateRandomKey.setCommand(commands.generateRandomKey);
            
            buttons.testLogin = new qx.ui.toolbar.Button(this.tr("Test Login"), "icon/22/mimetypes/executable.png");
            buttons.testLogin.setCommand(commands.testLogin);
            
            part.addAt(buttons.addTestData, 0);
			part.addAt(buttons.generateRandomKey, 1);
			part.addAt(buttons.testLogin, 2);
			
			part.show();
        },
        
        /**
         * Sets the tooltips for each button of this part.
         *
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @return {void}
         */
        setToolTips: function(commands, buttons){
            this.base(arguments);
            
            buttons.addTestData.setToolTipText(commands.addTestData.toString());
            buttons.generateRandomKey.setToolTipText(commands.generateRandomKey.toString());
            buttons.testLogin.setToolTipText(commands.testLogin.toString());
        },
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Opens the testdata window
         *
         * @return {void}
         */
        __showCreateTestData: function(){
            this.debug("__showCreateTestData");
			
            this.__testWindow.center();
            this.__testWindow.open();
        },

        /**
         * Opens the random key generator window
         *
         * @return {void}
         */        
        __showCreateRandomKey: function(){
            this.debug("__showCreateRandomKey");
			
            this.__generateRandomWindow.center();
            this.__generateRandomWindow.open();	
        },
        
        /**
         * Opens the test login box
         *
         * @return {void}
         */        
        __showTestLogin: function(){
            this.debug("__showTestLogin");
			this.__loginDialog.center();
            this.__loginDialog.open();				
        },
        
        __tryLogin: function(username, password) {
        	return username == password;
        },
        
        __successFulLogin: function(loginbox) {
			loginbox.close();
        },
        
        __showNotification: function(e) {
        	this.debug("__showNotification");
        	qx.core.Init.getApplication().showNotification("Test message!", "info");
        },
        
        __showInstallWizard: function(e) {
        	com.vcollaborate.emenu.utils.Dialogs.installWizard(this.__installWizardCallback, this);
        },
        
        __installWizardCallback: function( map ){
	    	var retVal = map || {};
	        dialog.Dialog.alert("<p>Thank you for your input:" + qx.lang.Json.stringify(retVal).replace(/\\/g,"") + "</p><p>Your account will be created.</p>");
	    }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this._disposeObjects("__testWindow", "__generateRandomWindow", "__loginDialog");
    }
});
