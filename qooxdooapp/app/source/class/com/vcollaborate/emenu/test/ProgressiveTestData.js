/* ************************************************************************
 #asset(com/vcollaborate/emenu/images/loading66.gif)
 ************************************************************************ */
/**
 * The progressive methods to create the test data.
 */
qx.Class.define("com.vcollaborate.emenu.test.ProgressiveTestData", {
    extend: qx.core.Object,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of ProgressiveTestData.
     */
    construct: function(){
        this.base(arguments);
        this.__minParentCategoryOrder = 0;
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __minParentCategoryOrder: null,
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Adds the testdata.
         *
         * @param parentcategories {Integer} Number of {@link com.vcollaborate.emenu.model.Category}
         * @param childcategories {Integer} Number of {@link com.vcollaborate.emenu.model.Category}
         * @param articles {Integer} Number of {@link com.vcollaborate.emenu.model.Article}
         * @return {void}
         */
        addTestData: function(parentcategories, childcategories, articles){
            this.debug("addTestData");
            
            // We'll use the progressive table's progress footer.  For that, we
            // need to define column widths as if we were a table.
            var columnWidths = new qx.ui.progressive.renderer.table.Widths(1);
            columnWidths.setWidth(0, "100%");
            
            // Instantiate a Progressive
            var footer = new qx.ui.progressive.headfoot.Progress(columnWidths);
            var structure = new qx.ui.progressive.structure.Default(null, footer);
            var progressive = new qx.ui.progressive.Progressive(structure);
            
            // We definitely want to see each progress as we're loading.  Ensure
            // that the widget queue gets flushed
            progressive.setFlushWidgetQueueAfterBatch(true);
            
            var addFunc = function(func){
                var ret = {
                    renderer: "func",
                    data: func
                };
                
                return ret;
            };
            
            // Instantiate a data model and populate it.
            var dataModel = new qx.ui.progressive.model.Default();
            
            // Instantiate a Function Caller
            var functionCaller = new qx.ui.progressive.renderer.FunctionCaller();
            
            // Give Progressive the renderer, and assign a name
            progressive.addRenderer("func", functionCaller);
            
            var qooxdoo = new qx.ui.basic.Image("com/vcollaborate/emenu/images/loading66.gif", "100%", "100%");
            qooxdoo.setMarginTop(-40);
            qooxdoo.setMarginLeft(0);
            progressive.add(qooxdoo);
            
            // Make the Progressive fairly small
            progressive.set({
                height: 100,
                width: 122,
                zIndex: 99999,
                opacity: 0.86,
                batchSize: 10,
                backgroundColor: "transparent"
            });
            
            qx.core.Init.getApplication().getRoot().add(progressive, {
                top: -1000,
                
                // initially off screen
                left: -1000
            });
            
            this.context = {
                oThis: this
            };
            
            progressive.addListener("renderStart", function(e){
                // Our event data is an object containing the 'state' object and
                // the number of elements to be rendered.
                var state = e.getData().state;
                var initialNum = e.getData().initial;
                
                // Center ourself
                var rootBounds = qx.core.Init.getApplication().getRoot().getBounds();
                var progressive = e.getData().state.getProgressive();
                var progressiveBounds = progressive.getBounds();
                
                var left = Math.floor((rootBounds.width - progressiveBounds.width) / 2);
                var top = Math.floor((rootBounds.height - progressiveBounds.height) / 2);
                
                progressive.setLayoutProperties({
                    left: left < 0 ? 0 : left,
                    top: top < 0 ? 0 : top
                });
                
                // Save our context in the userData field of the state object.
                state.getUserData().context = this.context;
                
                // Also save the number of elements for our progress bar usage.
                state.getUserData().initialNum = initialNum;
            }, this);
            
            progressive.addListener("renderEnd", function(e){
                // We don't need the Progressive any longer.  Arrange for it to be
                // destroyed.
                qx.event.Timer.once(function(){
                    this.getLayoutParent().remove(this);
                    this.dispose();
                }, this, 0);
            });
            
            this.context.minParentCategoryOrder = this.__minParentCategoryOrder;
            
            this.context.i = 0;
            this.context.j = 0;
            this.context.k = 0;
            this.context.l = 0;
            this.context.parentcategory = null;
            this.context.childcategory = null;
            
            this.context.examplecontent = "<p><span style='font-family: Verdana;'><font size='4'>﻿But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter consequences that are extremely painful. Nor again is there anyone who loves or pursues or desires to obtain pain of itself, because it is pain, but because occasionally circumstances occur in which toil and pain can procure him some great pleasure. To take a trivial example, which of us ever undertakes laborious physical exercise, except to obtain some advantage from it? But who has any right to find fault with a man who chooses to enjoy a pleasure that has no annoying consequences, or one who avoids a pain that produces no resultant pleasure?</font></span></p>";
            
            this.context.randomIDPart = this.guid();
            
            for (var i = this.context.minParentCategoryOrder; i < (parentcategories + this.context.minParentCategoryOrder); i++) {
                //create Parent categories
                dataModel.addElement(addFunc(function(userData){
                    var context = userData.context;
                    var rootCategory = com.vcollaborate.emenu.model.Category.getRoot();
                    var parentcategory = new com.vcollaborate.emenu.model.Category("C" + context.randomIDPart + "-" + context.i, "Test Parent Category " + context.i, rootCategory, context.i);
                    parentcategory.save();
                    context.parentcategory = parentcategory;
                }));
                
                //create Child categories for each parentcategory
                for (var j = 0; j < childcategories; j++) {
                    dataModel.addElement(addFunc(function(userData){
                    
                        var context = userData.context;
                        var parentcategory = context.parentcategory;
                        var childcategory = new com.vcollaborate.emenu.model.Category("C" + context.randomIDPart + "-" + context.i + "-" + context.j, "Test Child Category " + context.i + "-" + context.j, parentcategory, context.j);
                        childcategory.save();
                        context.childcategory = childcategory;
                    }));
                    
                    //create Articles for each Child category
                    for (var k = 0; k < articles; k++) {
                        dataModel.addElement(addFunc(function(userData){
                            var context = userData.context;
                            var childcategory = context.childcategory;
                            var article = new com.vcollaborate.emenu.model.Article("A" + context.randomIDPart + "-" + context.i + "-" + context.j + "-" + context.k, "Test Article " + context.i + "-" + context.j + "-" + context.k, childcategory, context.examplecontent, context.k);
                            article.save();
                        }));
                        
                        //increment article count
                        dataModel.addElement(addFunc(function(userData){
                            ++userData.context.k;
                        }));
                    }
                    
                    //increment child category count
                    dataModel.addElement(addFunc(function(userData){
                        userData.context.k = 0;
                        ++userData.context.j;
                    }));
                }
                
                //create Articles for each Parent category
                for (var l = 0; l < articles; l++) {
                    dataModel.addElement(addFunc(function(userData){
                        var context = userData.context;
                        var parentcategory = context.parentcategory;
                        var article = new com.vcollaborate.emenu.model.Article("A" + context.randomIDPart + "-" + context.i + "-" + context.l, "Test Article " + context.i + "-" + context.l, parentcategory, context.examplecontent, context.l);
                        article.save();
                    }));
                    
                    //increment article count
                    dataModel.addElement(addFunc(function(userData){
                        ++userData.context.l;
                    }));
                }
                
                //reset Article Parent Category count
                dataModel.addElement(addFunc(function(userData){
                    userData.context.l = 0;
                }));
                
                //increment parent category count
                dataModel.addElement(addFunc(function(userData){
                    userData.context.j = 0;
                    ++userData.context.i;
                }));
            }
            
            // Tell Progressive about its data model
            progressive.setDataModel(dataModel);
            
            // Begin execution
            progressive.render();
        },
        
        /**
         * Creates a four letter random string.
         *
         * @return {String} a four letter random string
         */
        S4: function(){
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        },
        
        /**
         * Creates a guid.
         *
         * @return {String} a guid
         */
        guid: function(){
            return (this.S4() + this.S4() + "-" + this.S4() + "-" + this.S4() + "-" + this.S4() + "-" + this.S4() + this.S4() + this.S4());
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this._disposeObjects("__minParentCategoryOrder");
    }
});
