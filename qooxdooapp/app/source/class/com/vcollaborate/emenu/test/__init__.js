/**
 * This package contains the com.vcollaborate.emenu.test's UI parts and logic
 * for batch adding objects of type {@link com.vcollaborate.emenu.model.Category}
 * and {@link com.vcollaborate.emenu.model.Article}.
 */