/**
 * This widgets displays an {@link com.vcollaborate.emenu.model.Password}.
 */
qx.Class.define("com.vcollaborate.emenu.passwords.PasswordView", {
    extend: qx.ui.container.Composite,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of PasswordView.
     */
    construct: function(){
        this.base(arguments);
        
        this.__initLayout();
        this.__initBindings();
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * The current {@link com.vcollaborate.emenu.model.Password}.
         */
        password: {
            apply: "__applyPassword",
            nullable: true,
            check: "Object",
            event: "changePassword"
        }
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __htmlArea: null,
        __table: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Applies the new {@link com.vcollaborate.emenu.model.Password} to this view.
         * If value is old, the content of the htmlArea is set to an empty string.
         *
         * @param value {com.vcollaborate.emenu.model.Password} the new {@link com.vcollaborate.emenu.model.Password}
         * @param old {com.vcollaborate.emenu.model.Password} the old {@link com.vcollaborate.emenu.model.Password}
         * @return {void}
         */
        __applyPassword: function(value, old){
            this.debug("__applyPassword");
            
            this.__htmlArea.getContentElement().scrollToY(0);
            
            this.updateView();
        },
        
        /**
         * Initialises the layout of this view.
         * The layout consists of a horizontal {@link qx.ui.splitpane.Pane}. On the
         * left side the __htmlArea is shown, on the left side an empty widget at
         * the moment. Later some additonal properties can be shown there.
         *
         * @return {void}
         */
        __initLayout: function(){
            this.debug("__initLayout");
            
            var dock = new qx.ui.layout.Dock();
            
            this.setLayout(dock);
            
            var horizontalSplitPane = new qx.ui.splitpane.Pane("horizontal");
            
            this.add(horizontalSplitPane, {
                edge: "center"
            });
            
            this.__htmlArea = new qx.ui.embed.Html().set({
                backgroundColor: "white"
            });
            this.__htmlArea.setOverflow("auto", "auto");
            this.__htmlArea.setSelectable(true);
            
            var htmlContainer = new qx.ui.core.scroll.ScrollPane();
            htmlContainer.add(this.__htmlArea);
            
            this.__table = this.__createTable();
            
            horizontalSplitPane.add(this.__table.set({
                decorator: null,
                backgroundColor: "white",
                minWidth: 200,
                width: 600
            }), 7);
            
            horizontalSplitPane.add(htmlContainer.set({
                decorator: "input-html",
                textColor: "text-active",
                minWidth: 200
            }), 3);
        },
        
        __initBindings: function(){
            this.debug("__initBindings");
            
            this.bind("password.content", this.__htmlArea, "html", {
                onUpdate: function(source, target, data){
                    if (data) {
                        target.setHtml(data);
                        
                        // flush all elements (needed to access the content of the dom element)
                        qx.html.Element.flush();
                        
                        // get the dom element containing the html of the password
                        var element = target.getContentElement().getDomElement();
                        
                        // get all links
                        var links = element.getElementsByTagName("a");
                        
                        // set the targets of all links to _blank
                        for (var i = 0; i < links.length; i++) {
                            links[i].target = "_blank";
                        }
                    }
                    else {
                        target.setHtml("");
                    }
                }
            });
            
            
        },
        
        __createTable: function(){
            var tableModel = new qx.ui.table.model.Simple();
            tableModel.setColumns([this.tr('Property'), this.tr('Value')]);
            var tableOptions = {
                tableColumnModel: function(obj){
                    return new qx.ui.table.columnmodel.Resize(obj);
                }
            };
            
            var table = new qx.ui.table.Table(tableModel, tableOptions);
            // layout
            table.setColumnVisibilityButtonVisible(false);
            table.setKeepFirstVisibleRowComplete(true);
            table.setStatusBarVisible(false);
			table.setShowCellFocusIndicator(false);
            // selection mode
            table.getSelectionModel().setSelectionMode(qx.ui.table.selection.Model.SINGLE_SELECTION);
            
            var propertyCellRendererFactoryFunc = function(cellInfo){
                var metaData = cellInfo.table.getTableModel().getRowData(cellInfo.row)[2];
                var renderer;
                
                for (var cmd in metaData) {
                
                    if (cmd == "type") {
                        switch (metaData['type']) {
							case "password":
                                return new qx.ui.table.cellrenderer.Password;
							case "text":
								return new qx.ui.table.cellrenderer.Default;
							case "url":
								return new com.vcollaborate.emenu.view.UrlCellRenderer;
							default:								        
								return new qx.ui.table.cellrenderer.Default;            
						}
                        break;
					}
                }
                return new qx.ui.table.cellrenderer.Default();
            }
            
            var propertyCellRendererFactory = new qx.ui.table.cellrenderer.Dynamic(propertyCellRendererFactoryFunc);
            
            table.getTableColumnModel().setDataCellRenderer(1, propertyCellRendererFactory);
			
            return table;
        },
        
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        updateView: function(shadow){
            shadow = shadow === undefined ? true : shadow;
            if (this.getPassword()) {
                if (shadow) {
                    this.__table.getTableModel().setData(this.getPassword().toTable([["username", "password"], ["password", "password"], ["url", "url"]]));
                }
                else {
                    this.__table.getTableModel().setData(this.getPassword().toTable([["username"], ["password"], ["url", "url"]]));
                }
            }
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this._disposeObjects("__htmlArea");
    }
});
