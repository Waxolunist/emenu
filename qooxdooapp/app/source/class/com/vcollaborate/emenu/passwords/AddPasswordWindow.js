/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/document-new.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/dialog-apply.png)
 ************************************************************************ */
/**
 * This widgets displays an {@link com.vcollaborate.emenu.model.Password} for editing and adding.
 */
qx.Class.define("com.vcollaborate.emenu.passwords.AddPasswordWindow", {
    extend: qx.ui.window.Window,
    
    /*
     *****************************************************************************
     CONSTRUCTOR
     *****************************************************************************
     */
    /**
     * Creates a new instance of AddPasswordWindow.
     */
    construct: function(){
        this.base(arguments, this.tr("Add password"), "icon/22/actions/document-new.png");
        
        // set the properties of the window
        this.set({
            modal: true,
            showMinimize: false,
            showMaximize: true,
            allowMaximize: true,
            //overflow: "auto",
            height: 300
        });
        
        // Create the content with a helper
        this.__addContent();
        this.__addBindings();
        this.__initListeners();
    },
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __categoryLabel: null,
        __form: null,
        __htmlArea: null,
        __addButton: null,
		__controller: null,
        
        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Adds the content of the window.
         *
         * @return {void}
         */
        __addContent: function(){
            this.debug("__addContent");
            
            this.setLayout(new qx.ui.layout.VBox(10));
            
            var groupBox = new qx.ui.groupbox.GroupBox(this.tr("Password Information")).set({
				minWidth: 200,
				height: 300
				//overflow: "auto"
			});
            groupBox.setLayout(new qx.ui.layout.Dock());
            
            this.__addButton = new qx.ui.form.Button(this.tr("Add"), "icon/22/actions/dialog-apply.png").set({
                alignX: "right",
                allowGrowX: false
            });
			this.__htmlArea = new com.vcollaborate.emenu.view.HtmlAreaBindingWidget(this.tr("Description"));						
			
			this.__form = new qx.ui.form.Form();
			
			//Icon
			this.__form.add(new com.vcollaborate.emenu.view.IconChooserButton().set({
				placeholder: "...",
				allowGrowX: true,
				alignX: "left",
				height: 28,
				toolTipText: this.tr("Select an icon")
            }),"...",null,"icon", this, {linebreak: false});
			//Title
			this.__form.add(new qx.ui.form.TextField().set({
                required: true,
				placeholder: this.tr("Title"),
				allowGrowX: true,
				alignX: "left",
				height: 28,
				paddingTop: 5,
                paddingLeft: 4
            }),this.tr("Title"),null,"title", this, {linebreak: true, colSpan: 2});
			//Username
			this.__form.add(new qx.ui.form.TextField().set({
				placeholder: this.tr("Username"),
				allowGrowX: true
            }),this.tr("Username"),null,"username", this, {colSpan: 3});
			//Password
			var passwordField = new qx.ui.form.PasswordField().set({
				placeholder: this.tr("Password"),
				allowGrowX: true
            });
			passwordField.addListener("appear", com.vcollaborate.emenu.view.PasswordFieldHelper.resetType, passwordField);
			var passwordRepeatField = new qx.ui.form.PasswordField().set({
				placeholder: this.tr("Password repeat"),
				allowGrowX: true
            });
			passwordRepeatField.addListener("appear", com.vcollaborate.emenu.view.PasswordFieldHelper.resetType, passwordRepeatField);
			passwordRepeatField.addListener("appear", function(e){
				this.setValue(passwordField.getValue());
				this.setValid(true);
			}, passwordRepeatField);
			
			this.__form.add(passwordField,
							this.tr("Password"),
							com.vcollaborate.emenu.utils.Validate.equal("value", qx.locale.Manager.tr("Password and Password Replay are not the same.")),
							"password", 
							passwordRepeatField, 
							{colSpan: 2, linebreak: false});
			var bindingButton = new com.vcollaborate.emenu.view.BindingButton("*").set({
				placeholder: "...",
				allowGrowX: true,
				alignX: "left",
				toolTipText: this.tr("Show Password")
            });
			bindingButton.addListener("execute", com.vcollaborate.emenu.view.PasswordFieldHelper.toggleType, passwordField);
			this.__form.add(bindingButton,"*",null,null, this, {linebreak: true});
			//Password repeat
			bindingButton.addListener("execute", com.vcollaborate.emenu.view.PasswordFieldHelper.toggleType, passwordRepeatField);
			this.__form.add(passwordRepeatField,
							this.tr("Password repeat"),
							com.vcollaborate.emenu.utils.Validate.equal("value", qx.locale.Manager.tr("Password and Password Replay are not the same.")),
							"passwordRepeat", 
							passwordField, 
							{colSpan: 3});
			//URL
			this.__form.add(new qx.ui.form.TextField().set({
				placeholder: this.tr("URL"),
				allowGrowX: true,
				required: false
            }),this.tr("URL"), com.vcollaborate.emenu.utils.Validate.urlOrEmpty(), "url", this, {colSpan: 3});

			this.__form.add(this.__htmlArea,this.tr("Description"),null,"content", this, {colSpan: 3});
			
			this.__form.addButton(this.__addButton);
			
			var single = new com.vcollaborate.emenu.passwords.FormRenderer(this.__form);
			single.getLayout().setRowFlex(5,3);
			single.getLayout().setColumnFlex(0,0);
			single.getLayout().setColumnFlex(1,3);
			groupBox.add(single, {edge: "center"});

			this.__categoryLabel = new qx.ui.basic.Atom("");
			this.add(this.__categoryLabel);
			
            this.add(groupBox, {
                flex: 1,
                height: "100%"
            });
        },
     
        /**
         * Inits the listeners for key strokes.
         */
        __initListeners: function(){
            this.debug("__initListeners");
            
            this.addListener("resize", this.center, this);
            
            this.addListener("keypress", function(e){
                var key = e.getKeyIdentifier();
                
                if (key == "Escape") {
                    this.close();
                } 
				/*
				else if (e.getTarget() instanceof qx.ui.form.TextField && e.getKeyIdentifier() === "Enter") {
                    this.__addPassword();
                }
                */
            }, this);
			
			this.addListener("appear", function(e){
				var part = com.vcollaborate.emenu.passwords.Part.getInstance();
				part.getCopyPasswordCmd().setEnabled(false);
				part.getCopyUsernameCmd().setEnabled(false);
			},this);
			
			this.addListener("close", function(e){
				var part = com.vcollaborate.emenu.passwords.Part.getInstance();
				part.getCopyPasswordCmd().setEnabled(true);
				part.getCopyUsernameCmd().setEnabled(true);
			},this);
			
			this.__addButton.addListener("execute", this.__addPassword, this);
        },
		
        /**
         * Handles button clicks on 'Add' button or/and
         * pressing enter key on textfields
         *
         * @param e {qx.event.type.Event} Execute event
         * @return {void}
         */
        __addPassword: function(e){
            this.debug("__addPassword");
            
            if (this.__form.validate()) {               	
				this.__controller.updateModel();
				var pw = this.__controller.getModel();
				pw.setCategory(this.getCategory());
				pw.save();
				this.close();
				this.setPassword(null);
				this.__htmlArea.emptyContent();
				this.__form.reset()
				this.fireEvent("finishEditing");
            }
        },
        
        /**
         * Applies the new {@link com.vcollaborate.emenu.model.Password} to this view.
         * If value is null, the content of the htmlArea is set to an empty string.
         *
         * @param value {com.vcollaborate.emenu.model.Password} the new {@link com.vcollaborate.emenu.model.Password}
         * @param old {com.vcollaborate.emenu.model.Password} the old {@link com.vcollaborate.emenu.model.Password}
         * @return {void}
         */
        __applyPassword: function(value, old){
			this.debug("__applyPassword");
			
			var title = this.getChildControl("title");
			
            if (value && value.getCategory()) {
				title.setValue(this.tr("Edit password"));
				this.__addButton.setLabel(this.tr("Save"));
			} else {
				title.setValue(this.tr("Add password"));
				this.__addButton.setLabel(this.tr("Add"));
				this.__htmlArea.emptyContent();
            }
			
			this.__controller = new qx.data.controller.Form(value, this.__form, true);
        },
		
        /**
         * Binds the {@link #password} and {@link #category} to the widgets.
         */
        __addBindings: function(){
			this.debug("__addBindings");
        	
			var oThis = this;
			this.bind("category.title", this.__categoryLabel, "label");
			this.bind("category.icon", this.__categoryLabel, "icon");
        }
    },
    
    /*
     *****************************************************************************
     PROPERTIES
     *****************************************************************************
     */
    properties: {
        /**
         * The passwords this window presents.
         */
        password: {
            nullable: true,
            check: "com.vcollaborate.emenu.model.Password",
            init: new com.vcollaborate.emenu.model.Password,
            event: "changePassword",
            apply: "__applyPassword"
        }, 
		
		category: {
			nullable: true,
            check: "com.vcollaborate.emenu.model.Category",
			init: null,
            event: "changeCategory"
		}
    },
    
	/*
     *****************************************************************************
     EVENTS
     *****************************************************************************
     */
    events: {
        /**
         * Fired when the editing is finished because of a save or a cancel action.
         */
        "finishEditing": "qx.event.type.Event"
    },
	
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        this.__commands = null;
        this._disposeObjects("__form", "__htmlArea", "__categoryLabel", "__addButton", "__controller");
    }
});
