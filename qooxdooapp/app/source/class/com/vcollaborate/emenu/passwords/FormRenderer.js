/**
 * Rendere using the placeholder property of {@link qx.ui.form.AbstractField}
 * to visualize the name.
 */
qx.Class.define("com.vcollaborate.emenu.passwords.FormRenderer", {
    extend: qx.ui.form.renderer.Single,
    implement: qx.ui.form.renderer.IFormRenderer,
    
    members: {
        // overridden
        addItems: function(items, names, title, options){
            // add the header
            if (title != null) {
                this._add(this._createHeader(title), {
                    row: this._row,
                    column: 0,
                    colSpan: 2
                });
                this._row++;
            }
            
            var column = 0;
            // add the items
            for (var i = 0; i < items.length; i++) {
                var linebreak = options[i] ? (options[i].linebreak === undefined ? true : options[i].linebreak) : true;
                var colSpan = options[i] ? (options[i].colSpan === undefined ? 1 : options[i].colSpan) : 1;
                
                if (items[i].setPlaceholder === undefined) {
                    throw new Error("Only widgets with placeholders supported.");
                }
                items[i].setPlaceholder(names[i]);
                this._add(items[i], {
                    row: this._row,
                    column: column,
                    colSpan: colSpan
                });
                
                if (linebreak) {
                    this._row++;
                    column = 0;
                }
                else {
                    column += colSpan;
                }
            }
        },
        
        addButton: function(button){
            if (this._buttonRow == null) {
				var maxCol = this.getLayout().getColumnCount();
				
                // create button row
                this._buttonRow = new qx.ui.container.Composite();
                this._buttonRow.setMarginTop(5);
                var hbox = new qx.ui.layout.HBox();
                hbox.setAlignX("right");
                hbox.setSpacing(5);
                this._buttonRow.setLayout(hbox);
                // add the button row
                this._add(this._buttonRow, {
                    row: this._row,
                    column: 0,
                    colSpan: maxCol
                });
                // increase the row
                this._row++;
            }
            
            // add the button
            this._buttonRow.add(button);
        }
    
    }
});
