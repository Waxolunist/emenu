/* ************************************************************************
 #asset(qx/icon/${qx.icontheme}/22/actions/list-add.png)
 #asset(qx/icon/${qx.icontheme}/22/actions/list-remove.png)
 #asset(com/vcollaborate/emenu/images/edit.png)
 ************************************************************************ */
/**
 * This part adds the possibility to add, edit and remove {@link com.vcollaborate.emenu.mode.Password} to
 * {@link com.vcollaborate.emenu.mode.Category}.
 * It provides only a rightsidepane.
 *
 * @see com.vcollaborate.emenu.categories.Part
 */
qx.Class.define("com.vcollaborate.emenu.passwords.Part", {
    extend: com.vcollaborate.emenu.utils.AbstractPart,
    type: "singleton",

	/*
    *****************************************************************************
    CONSTRUCTOR
    *****************************************************************************
    */
	/**
     * Creates a new instance of Part.
     */
    construct: function(){
        this.base(arguments);

        if (!this.__addPasswordWindow) {
            this.__addPasswordWindow = new com.vcollaborate.emenu.passwords.AddPasswordWindow();
			this.__addPasswordWindow.addListener("finishEditing", function(){this.__passwordView.updateView();}, this);
        }

        if (!this.__listView) {
            this.__listView = new com.vcollaborate.emenu.passwords.List();
            this.__listView.setDecorator("main");
        }

        if (!this.__passwordView) {
            this.__passwordView = new com.vcollaborate.emenu.passwords.PasswordView();
            this.__passwordView.setDecorator("main");
            this.__passwordView.setHeight(150);
        }

        if (!this.__listController) {
            // create the controller which binds ths list
            // 1. Parameter: The model (null because is bound in the next line)
            // 2. Parameter: The view (the list widget)
            // 3. Parameter: name of the model property to show as label in the list
            this.__listController = new qx.data.controller.List(null, this.__listView.getList(), "title");
			this.__listController.setIconPath("icon");

            // bind the password
            // bind the first selection of the list to the password view
            this.__listController.bind("selection[0]", this.__passwordView, "password");
        }
    },

	/*
    *****************************************************************************
    MEMBERS
    *****************************************************************************
    */
    members: {

        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __addPasswordWindow: null,
        __listView: null,
        __passwordView: null,
        __listController: null,
        __addPasswordCmd: null,
        __editPasswordCmd: null,
        __removePasswordCmd: null,
		__copyPasswordCmd: null,
		__copyUsernameCmd: null,
		__openUrlCmd: null,

        /*
         *****************************************************************************
         DERIVED
         *****************************************************************************
         */
        /**
         * Adds the window for adding and editing passwords ({@link com.vcollaborate.emenu.passwords.})
         * to the root of the application.
         *
         * @param controller {com.vcollaborate.emenu.Application} The main application class
         * @return {void}
         */
        init: function(controller){
            this.base(arguments);
            controller.getRoot().add(this.__addPasswordWindow);
        },

        /**
         * Inits following three commands:
         *
         * <ul>
         * <li>{@link #getAddPasswordCmd}</li>
         * <li>{@link #getEditPasswordCmd}</li>
         * <li>{@link #getRemovePasswordCmd}</li>
         * </ul>
         *
         * @param commands {var} the global commands object
         * @return {void}
         */
        initCommands: function(commands){
            this.base(arguments);
            commands.addPassword = new qx.ui.core.Command("Control+Shift+A");
            commands.addPassword.addListener("execute", this.__showAddPassword, this);
			commands.addPassword.setEnabled(false);
            this.__addPasswordCmd = commands.addPassword;

            commands.editPassword = new qx.ui.core.Command("Control+Shift+S");
            commands.editPassword.addListener("execute", this.__showEditPassword, this);
			commands.editPassword.setEnabled(false);
            this.__editPasswordCmd = commands.editPassword;

            commands.removePassword = new qx.ui.core.Command("Control+Shift+D");
            commands.removePassword.addListener("execute", this.__removePassword, this);
			commands.removePassword.setEnabled(false);
            this.__removePasswordCmd = commands.removePassword;
			
			commands.copyPassword = new qx.ui.core.Command("Control+C");
            commands.copyPassword.addListener("execute", this.__copyPassword, this);
			this.__copyPasswordCmd = commands.copyPassword;
			
			commands.copyUsername = new qx.ui.core.Command("Control+B");
            commands.copyUsername.addListener("execute", this.__copyUsername, this);
			this.__copyUsernameCmd = commands.copyUsername;
			
			commands.openUrl = new qx.ui.core.Command("Control+U");
			commands.openUrl.addListener("execute", this.__openUrl, this);
			this.__openUrlCmd = commands.openUrl;
        },

        /**
         * Inits the buttons for the toolbar, one for each command.
         *
         * @see #initCommands
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @param part {qx.ui.toolbar.Part} the part to add the buttons to
         * @return {void}
         */
        initButtons: function(commands, buttons, part){
            this.base(arguments);
            buttons.addPassword = new qx.ui.toolbar.Button(this.tr("Add password"), "icon/22/actions/list-add.png");
            buttons.addPassword.setCommand(commands.addPassword);
            part.addAt(buttons.addPassword, 0);

            buttons.editPassword = new qx.ui.toolbar.Button(this.tr("Edit password"), "com/vcollaborate/emenu/images/edit.png");
            buttons.editPassword.setCommand(commands.editPassword);
            part.addAt(buttons.editPassword, 1);

            buttons.removePassword = new qx.ui.toolbar.Button(this.tr("Remove password"), "icon/22/actions/list-remove.png");
            buttons.removePassword.setCommand(commands.removePassword);
            part.addAt(buttons.removePassword, 2);
			
			part.show();
        },

        /**
         * Inits the listeners to handle the disable- and enable-actions of the toolbar buttons.
         *
         * @return {void}
         */
        initListeners: function(){
            this.base(arguments);
            this.__listView.getList().addListener("changeSelection", function(e){
                if (e.getData().length > 0) {
                    this.getRemovePasswordCmd().setEnabled(true);
                    this.getEditPasswordCmd().setEnabled(true);
                }
                else {
                    this.getRemovePasswordCmd().setEnabled(false);
                    this.getEditPasswordCmd().setEnabled(false);
                }
            }, this);
        },

        /**
         * Sets the tooltips for each button of this part.
         *
         * @param commands {var} the global commands object
         * @param buttons {var} the global buttons object
         * @return {void}
         */
        setToolTips: function(commands, buttons){
            this.base(arguments);
            buttons.addPassword.setToolTipText(commands.addPassword.toString());
            buttons.editPassword.setToolTipText(commands.editPassword.toString());
            buttons.removePassword.setToolTipText(commands.removePassword.toString());
        },

        /**
         * Returns a vertical {@link qx.ui.splitpane.Pane} with a {@link com.vcollaborate.emenu.passwords.List}
         * at the head and a {@link com.vcollaborate.emenu.passwords.PasswordView} at the bottom.
         *
         * @return {qx.ui.splitpane.Pane} {@link qx.ui.splitpane.Pane}
         */
        getRightSidePane: function(){
            this.base(arguments);
            var verticalSplitPane = new qx.ui.splitpane.Pane("vertical");
            verticalSplitPane.setDecorator(null);

            verticalSplitPane.add(this.__listView, 1);
            verticalSplitPane.add(this.__passwordView, 0);

            return verticalSplitPane;
        },

		postInit: function() {
			//this.__listView.setHeight(this.__listView.getLayoutParent().getHeight()*0.8);
		},

        /*
         *****************************************************************************
         PRIVATE
         *****************************************************************************
         */
        /**
         * Opens the {@link com.vcollaborate.emenu.password.} to add an
         * {@link com.vcollaborate.emenu.model.Password}.
         *
         * @return {void}
         */
        __showAddPassword: function(){
            this.debug("__showAddPassword");
			this.__addPasswordWindow.setPassword(new com.vcollaborate.emenu.model.Password);
            this.__addPasswordWindow.center();
            this.__addPasswordWindow.open();
        },

        /**
         * Opens a {@link com.vcollaborate.emenu.password.} to edit
         * the selected {@link com.vcollaborate.emenu.model.Password}.
         *
         * @return {void}
         */
        __showEditPassword: function(){
            this.debug("__showEditPassword");
            var passwordSelection = this.getSelectedPassword();

            if (passwordSelection) {
				this.__addPasswordWindow.setPassword(passwordSelection);
                this.__addPasswordWindow.center();
                this.__addPasswordWindow.open();
            }
        },

        /**
         * Removes the selected {@link com.vcollaborate.emenu.model.Password}.
         *
         * @return {void}
         */
        __removePassword: function(){
            this.debug("__removePassword");
            var passwordSelection = this.getSelectedPassword();

            if (passwordSelection) {
                passwordSelection.getCategory().getArticles().remove(passwordSelection);
                passwordSelection.remove();
            }
        },
		
		__copyPassword: function(e) {
			this.debug("__copyPassword");
			var pw = this.getSelectedPassword();
			if (pw) {
				com.vcollaborate.emenu.utils.Clipboard.copyToSystemClipboard(pw.getPassword());
			}
		},

		__copyUsername: function(e) {
			this.debug("__copyUsername");
			var pw = this.getSelectedPassword();
			if (pw) {
				com.vcollaborate.emenu.utils.Clipboard.copyToSystemClipboard(pw.getUsername());
			}
		},

		__openUrl: function(e) {
			this.debug("__openUrl");
			var pw = this.getSelectedPassword();
			if (pw) {
				window.open(pw.getUrl(), "_blank");
			}
		},
		
        /*
         *****************************************************************************
         PUBLIC
         *****************************************************************************
         */
        /**
         * Returns the {@link qx.data.controller.List} bound to {@link com.vcollaborate.emenu.passwords.List}.
         *
         * @return {qx.data.controller.List} {@link qx.data.controller.List}
         */
        getListController: function(){
            return this.__listController;
        },
		
		/**
         * Returns the {@link com.vcollaborate.emenu.passwords.AddPasswordWindow}.
         *
         * @return {com.vcollaborate.emenu.passwords.AddPasswordWindow} {@link com.vcollaborate.emenu.passwords.AddPasswordWindow}
         */
        getAddPasswordWindow: function(){
            return this.__addPasswordWindow;
        },

        /**
         * Returns the current selected {@link com.vcollaborate.emenu.model.Password}
         *
         * @return {com.vcollaborate.emenu.model.Password} {@link com.vcollaborate.emenu.model.Password}
         */
        getSelectedPassword: function(){
            return this.__listController.getSelection().getItem(0);
        },

        /**
         * Returns the command __addPassword__.
         * The command has the shortcut *Control+Shift+A* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getAddPasswordCmd: function(){
            return this.__addPasswordCmd;
        },

        /**
         * Returns the command __editPassword__.
         * The command has the shortcut *Control+Shift+S* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getEditPasswordCmd: function(){
            return this.__editPasswordCmd;
        },

        /**
         * Returns the command __removePassword__.
         * The command has the shortcut *Control+Shift+D* set.
         *
         * @return {qx.ui.core.Command} {@link qx.ui.core.Command}
         */
        getRemovePasswordCmd: function(){
            return this.__removePasswordCmd;
        },
		
		getCopyPasswordCmd: function() {
			return this.__copyPasswordCmd;
		},
		
		getCopyUsernameCmd: function() {
			return this.__copyUsernameCmd;
		},
		
		getOpenUrlCmd: function() {
			return this.__openUrlCmd;
		}
    },

	/*
    *****************************************************************************
    DESTRUCTOR
    *****************************************************************************
    */
	destruct: function(){
		this.debug("destruct");
		
        this._disposeObjects("__addPasswordWindow", "__listView", "__passwordView", "__listController", "__addPasswordCmd", "__editPasswordCmd", "__removePasswordCmd");
    }
});
