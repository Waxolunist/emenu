/* ************************************************************************
 #asset(com/vcollaborate/emenu/images/loading22.gif)
 #asset(com/vcollaborate/emenu/images/loader.gif)
 #asset(com/vcollaborate/emenu/css/*)
 #asset(js/sjcl.min.js)
 #asset(js/sha1.js)
 #asset(js/json2.js)
 #asset(js/jquery-1.6.2.min.js)
 #asset(js/jquery.couch.js)
 #asset(js/jquery.couch.app.js)
 #asset(js/jquery.couch.app.util.js)
 ************************************************************************ */
/**
 * This is the main application. It consists of following
 */
qx.Class.define("com.vcollaborate.emenu.Application", {
    extend: qx.application.Standalone,
    
    /*
     *****************************************************************************
     MEMBERS
     *****************************************************************************
     */
    members: {
    
        /*
         *****************************************************************************
         PRIVATE FIELDS
         *****************************************************************************
         */
        __commands: null,
        __partsArray: null,
        __loginDialog: null,
        __registerDialog: null,
        __notificationBar: null,
        
        __showInstallWizard: true,
        __showLogin: true,
        /*
         ---------------------------------------------------------------------------
         APPLICATION METHODS
         ---------------------------------------------------------------------------
         */
        /**
         * Application initialization which happens when
         * all library files are loaded and ready
         *
         * @return {void}
         */
        main: function(){
            this.base(arguments);
            
            this.debug("main");
            
            //qx.Class.patch(qx.Class.getByName("qx.core.Property"), qx.Mixin.getByName("com.vcollaborate.emenu.io.MProperty"));
            
            this._initChain(
            	this._initExternals, 
            	this._initParts,
            	this._initInstallation,
            	this._buildUpGui
            );
            
            // Add log appenders
            if (qx.core.Environment.get('qx.debug')) {
                qx.log.appender.Native;
                qx.log.appender.Console;
            }
                        
            this.getRoot().set({
        		blockerColor: '#6A6A6A',
				blockerOpacity: 0.4
			});
        },
        
        /**
         * Called when the application’s main method was executed to handle “final” tasks like rendering or retrieving data.
         *
         * @return {void}
         */
        finalize: function(){
            this.debug("finalize");
            
            this.base(arguments);
        },
        
        /*
         *****************************************************************************
         PROTECTED
         *****************************************************************************
         */
        
        /*
         *****************************************************************************
         INIT FUNCTIONS - parameterless
         *****************************************************************************
         */
        
        _initChain: function() {
        	for(var i = 0, l = arguments.length; i < l; i++) {
				if(arguments[i] !== undefined) {
					arguments[i].apply(this, arguments);
					break;
				}
        	}
        },
        
        _initChainCallNext: function() {
        	//qx.lang.Array.removeAt(arguments, 0);
        	for(var i = 0, l = arguments.length; i < l; i++) {
				if(arguments[i] !== undefined) {
					delete arguments[i];
					break;
				}
			} 
        	
        	this._initChain.apply(this, arguments);
        },
        
        _initExternals: function() {
        	//init crypto library
        	var varargs = arguments;
            var cr = com.vcollaborate.emenu.utils.Crypt.getInstance();
            cr.addListenerOnce("scriptLoaded", function(e) {
	            //init couchdb
            	var db = com.vcollaborate.emenu.io.CouchDB.getInstance();
            	db.addListenerOnce("scriptLoaded", function(e) {
            		this._initChainCallNext.apply(this, varargs);
            	}, this);
            }, this);
        },
        
        _initParts: function() {            
            qx.io.PartLoader.getInstance().addListener("partLoaded", function(e){
                this.debug("part loaded: " + e.getData().getName());
            }, this);
            
            // Load current locale part
            var currentLanguage = qx.locale.Manager.getInstance().getLanguage();
            var knownParts = qx.Part.getInstance().getParts();
            
            // if the locale is available as part
            if (knownParts[currentLanguage]) {
                // load this part
                qx.io.PartLoader.require([currentLanguage], function(){
                    // forcing identical locale
                    qx.locale.Manager.getInstance().setLocale(currentLanguage);
                    
                    // build the GUI after the initial locals has been loaded
                    this._initChainCallNext.apply(this, arguments);
                }, this);
            }
            else {
                // if we cant find the default locale, print a warning and load the gui
                this.warn("Cannot load locale part for current language " + currentLanguage + ", falling back to English.");
                this._initChainCallNext.apply(this, arguments);
            }
        },
        
        _initInstallation: function() {
        	this.debug("_initInstallation");
        	
        	this.__notificationBar = com.vcollaborate.emenu.view.NotificationBar.getInstance();
            this.getRoot().add(this.__notificationBar, {right:"0%", top:"0%", bottom: "0%", left: "76%"});
            
        	var appObj = com.vcollaborate.emenu.model.Application.getApplicationObject();
			
			if(appObj.getStatus() == "newparent" && this.__showInstallWizard) {
				com.vcollaborate.emenu.io.CouchDB.getInstance().isAdminParty(function(adminparty){
					this.debug("adminparty: " + adminparty);
					com.vcollaborate.emenu.utils.Dialogs.installWizard(this._installWizardCallback, this, adminparty);
				}, this);
				
			} else {
				 this._initChainCallNext.apply(this, arguments);
			}
			
        },
        
        _installWizardCallback: function( map ){
	    	var retVal = map || {};
	    	var appObj = com.vcollaborate.emenu.model.Application.getApplicationObject();
	    	var licenseObj = com.vcollaborate.emenu.model.License.getLicenseObject(retVal.license || null, retVal.email);
	    	try {
	    		if((licenseObj.getEmail() !== retVal.email) || (retVal.license !== 'test')) {
	    			throw new com.vcollaborate.emenu.model.LicenseError();
	    		}
	    		
	    		appObj.setLicensekey(com.vcollaborate.emenu.model.License.calculateLicenseKey(licenseObj));
	    		this.debug(appObj.getLicensekey());
	    		appObj.setStatus("parent");
	    		appObj.save();
	    		
	    		this.__notificationBar.showMessage("The application will be installed!", "info");
	    		
	    		//TODO create Adminaccount
	    		var admin = new com.vcollaborate.emenu.model.User(null, 'admin', retVal.password, retVal.email, '', true);
	    		admin.save();
	    		
		        //dialog.Dialog.alert("<p>Thank you for your input:" + qx.lang.Json.stringify(retVal).replace(/\\/g,"") + "</p><p>Your account will be created.</p>");
		        this._buildUpGui();
	    	} catch (ex) {
	    		if(ex instanceof com.vcollaborate.emenu.model.LicenseError) {
	    			this.__notificationBar.showMessage("This is not a valid licensekey! Please reload and try again!", "info", -1);
	    		} else {
	    			throw ex;
	    		}
	    	}
	    },
        
	    // Init after login - not before
        _initScreenLock: function() {
        	this.debug("_initScreenLock");
        	var labels = {};
        	labels.title = this.tr("<b>Screen is locked!</b>");
        	labels.name = this.tr("Name");
        	labels.password = this.tr("Password");
        	labels.login = this.tr("Unlock Screen");
        	com.vcollaborate.emenu.utils.ScreenLock.getInstance().initScreenLock(30, 
        		new com.vcollaborate.emenu.view.LoginDialog(
	            	com.vcollaborate.emenu.io.CouchDB.getInstance().login, function(loginbox){
	            		loginbox.close();
	            	}, 
	            	null, this, labels
	        	),
	        	'#C3C3C3', 1.0
	        );
        },
	    
        /**
         * Main routine which builds the whole GUI.
         *
         * @return {void}
         */
        _buildUpGui: function(){
            this.debug("buildUpGui");
            
            if(this.__showLogin) {
	            if (!this.__loginDialog) {
	            	this.__loginDialog = new com.vcollaborate.emenu.view.LoginDialog(
	            		com.vcollaborate.emenu.io.CouchDB.getInstance().login, this._successfulLogin, this._openRegisterUserDialog, this
	        		);
	        	}
	        	
	        	this.__loginDialog.center();
	            this.__loginDialog.open();
	            
	            qx.event.Timer.once(function(){
					if(!com.vcollaborate.emenu.io.CouchDB.hasConnection()) {
						this.__notificationBar.showMessage("No connection to database!", "info");
					}
	            }, this, 1000);   
	            
	            var listenerId = this.getRoot().addListener("resize", function(e){
	            	this.__loginDialog.center();
	            }, this);
	            this.__loginDialog.addListener("close", function(e){
	            	this.getRoot().removeListenerById(listenerId);
	            }, this);
            } else {
            	this._initApplication();
            }
        },
		
		_successfulLogin: function(loginbox) {
			this.debug("_successfulLogin");
			loginbox.close();
			this._initApplication();
		},
		
		_initApplication: function() {
			this.debug("_initApplication");
			this.__partsArray = com.vcollaborate.emenu.utils.PartLoaderHelper.getActiveParts();
			//load all parts
			this.__partsArray.forEach(function(item){
                com.vcollaborate.emenu.utils.PartLoaderHelper.require([item], function(){
                    var part = qx.Class.getByName(item + ".Part");
                    part.getInstance().init(this);
                }, this);
            }, this);
			
			this._initializeCommands();
            this._createLayout();
            this._setUpBinding();
            this._initScreenLock();			
		},
		
		_openRegisterUserDialog: function() {
			this.debug("_openRegisterUserDialog");
			
			if (!this.__registerDialog) {
            	this.__registerDialog = new com.vcollaborate.emenu.view.RegisterUserDialog(this);
            	this.__registerDialog.addListener("registered", this._registerUser, this);
        	}
        	
        	this.__registerDialog.center();
            this.__registerDialog.open();
		},
		
		_registerUser: function(e) {
			this.debug("_registerUser");
			this.__notificationBar.showMessage(this.tr("User succcessfully registered!"), "info");
			//console.log(e.getData());
		},
			
        /**
         * Set up the bindings and controller.
         *
         * @return {void}
         */
        _setUpBinding: function(){
            this.debug("_setUpBinding");
            
            com.vcollaborate.emenu.utils.PartLoaderHelper.require(["com.vcollaborate.emenu.categories", "com.vcollaborate.emenu.articles"], function(){
                // bind the first selection of the tree as the model of the list
                com.vcollaborate.emenu.categories.Part.getInstance().getTreeController().bind("selection[0].articles", com.vcollaborate.emenu.articles.Part.getInstance().getListController(), "model");
            }, this);
            
            com.vcollaborate.emenu.utils.PartLoaderHelper.require(["com.vcollaborate.emenu.categories", "com.vcollaborate.emenu.passwords"], function(){
                // bind the first selection of the tree as the model of the list
                com.vcollaborate.emenu.categories.Part.getInstance().getTreeController().bind("selection[0].articles", com.vcollaborate.emenu.passwords.Part.getInstance().getListController(), "model");
                com.vcollaborate.emenu.categories.Part.getInstance().getTreeController().bind("selection[0]", com.vcollaborate.emenu.passwords.Part.getInstance().getAddPasswordWindow(), "category");
            }, this);
        },
        
        /**
         * Creates the core layout.
         *
         * @return {void}
         */
        _createLayout: function(){
            this.debug("_createLayout");
            
            // Create main layout
            var dockLayout = new qx.ui.layout.Dock();
            dockLayout.setSeparatorY("separator-vertical");
            var dockLayoutComposite = new qx.ui.container.Composite(dockLayout);
            this.getRoot().add(dockLayoutComposite, {
                edge: 0
            });
            
            dockLayoutComposite.add(new com.vcollaborate.emenu.view.Header(), {
                edge: "north"
            });
            dockLayoutComposite.add(new com.vcollaborate.emenu.view.ToolBar(this.__commands), {
                edge: "north"
            });
            
            // Create horizontal splitpane
            var horizontalSplitPane = new qx.ui.splitpane.Pane();
            dockLayoutComposite.add(horizontalSplitPane);
            
            this.__partsArray.forEach(function(item){
                com.vcollaborate.emenu.utils.PartLoaderHelper.require([item], function(){
                    var part = qx.Class.getByName(item + ".Part");
                    var leftsidepane = part.getInstance().getLeftSidePane();
                    var rightsidepane = part.getInstance().getRightSidePane();
                    
                    if (leftsidepane) {
                        horizontalSplitPane.add(leftsidepane, 0);
                    }
                    
                    if (rightsidepane) {
                        horizontalSplitPane.add(rightsidepane, 1);
                    }
                    
                    part.getInstance().postInit();
                }, this);
            }, this);
        },
        
        /**
         * Initialize commands (shortcuts, ...)
         *
         * @return {void}
         */
        _initializeCommands: function(){
            this.debug("_initializeCommands");
            this.__commands = {};
            
            this.__commands.about = new qx.ui.core.Command("F1");
            this.__commands.about.addListener("execute", this._showAbout, this);
            
            this.__commands.removeEntry = new qx.ui.core.Command("Delete");
            this.__commands.removeEntry.addListener("execute", this._removeEntry, this);
            
            this.__commands.renameEntry = new qx.ui.core.Command("F2");
            this.__commands.renameEntry.addListener("execute", this._renameEntry, this);
            
            this.__partsArray.forEach(function(item){
                com.vcollaborate.emenu.utils.PartLoaderHelper.require([item], function(){
                    var part = qx.Class.getByName(item + ".Part");
                    part.getInstance().initCommands(this.__commands);
                    part.getInstance().initListeners();
                }, this);
            }, this);
        },
        
        /**
         * Removes the currently selected entry.
         *
         * @return {void}
         */
        _removeEntry: function(){
            this.debug("_removeEntry");
            
            if (this.__commands.removeArticle && this.__commands.removeArticle.isEnabled()) {
                this.__commands.removeArticle.execute(this);
            }
            else if (this.__commands.removePassword && this.__commands.removePassword.isEnabled()) {
                this.__commands.removePassword.execute(this);
            } 
			else if (this.__commands.removeCategory && this.__commands.removeCategory.isEnabled()) {
                this.__commands.removeCategory.execute(this);
            }
        },
        
        
        /**
         * Renames the current selection. At the moment this is only implemented for {@link com.vcollaborate.emenu.categories}.
         *
         * @return {void}
         */
        _renameEntry: function(){
            this.debug("_renameEntry");
            
            if (this.__commands.editCategory && this.__commands.editCategory.isEnabled()) {
                this.__commands.editCategory.execute(this);
            }
        },
        
        /**
         * Shows the about popup for the application.
         *
         * @return {void}
         */
        _showAbout: function(){
            this.debug("_showAbout");
            
            alert(this.tr("EMenu Administration"));
        },
        
        lockScreen: function() {
        	
        },
        
        showNotification: function(message, icon){
        	this.debug("showNotification");
        	
        	this.__notificationBar.showMessage(message, icon);
        },
        
        getShortcuts: function() {
        	return this.__commands;
        }
    },
    
    /*
     *****************************************************************************
     DESTRUCTOR
     *****************************************************************************
     */
    destruct: function(){
        this.debug("destruct");
        
        //A reference of every part needs to be written here. Just for build
        com.vcollaborate.emenu.utils.PartLoaderHelper.require("com.vcollaborate.emenu.articles", function(){
            com.vcollaborate.emenu.articles.Part
        }, this);
        com.vcollaborate.emenu.utils.PartLoaderHelper.require("com.vcollaborate.emenu.categories", function(){
            com.vcollaborate.emenu.categories.Part
        }, this);
        com.vcollaborate.emenu.utils.PartLoaderHelper.require("com.vcollaborate.emenu.pref", function(){
            com.vcollaborate.emenu.pref.Part
        }, this);
        com.vcollaborate.emenu.utils.PartLoaderHelper.require("com.vcollaborate.emenu.test", function(){
            com.vcollaborate.emenu.test.Part
        }, this);
        com.vcollaborate.emenu.utils.PartLoaderHelper.require("com.vcollaborate.emenu.time", function(){
            com.vcollaborate.emenu.time.Part
        }, this);
        com.vcollaborate.emenu.utils.PartLoaderHelper.require("com.vcollaborate.emenu.passwords", function(){
            com.vcollaborate.emenu.passwords.Part
        }, this);
        
        this.__commands = null;
        this._disposeObjects("__commands", "__partsArray", "__loginDialog", "__registerDialog", "__notificationBar");
    }
});
